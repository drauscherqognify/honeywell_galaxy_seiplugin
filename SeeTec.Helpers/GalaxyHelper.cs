﻿using SeeTec.Helpers.Logging;
using System;

namespace SeeTec.HoneywellGalaxy.SEIPlugin.Helper
{
    /// <summary>
    /// Class for static helper methods.
    /// </summary>
    public static class GalaxyHelper
    {
        public static int ConvertToThreeDigitIntegerZoneAdress(string adress)
        {
            if (!IsValidZoneAdress(adress))
            {
                throw new ArgumentException($"Not a valid zone adress: {adress}");
            }

            if (adress.Length == 3)
            {
                if (int.TryParse(adress, out int addrInt))
                {
                    return addrInt;
                }
            }

            if (adress.Length == 4)
            {
                if (int.TryParse(ConvertZoneAdress(adress), out int addrInt))
                {
                    return addrInt;
                }
            }

            throw new ArgumentException($"Not a valid zone adress: {adress}");
        }

        /// <summary>
        /// Converts between 3 digit and 4 digit Galaxy Zone adresses (and vice versa).
        /// </summary>
        /// <param name="adress"></param>
        /// <returns></returns>
        public static string ConvertZoneAdress(string adress)
        {
            if (string.IsNullOrWhiteSpace(adress))
            {
                throw new ArgumentException($"Invalid zone adress: {adress}");
            }

            string zoneAdress = adress.Trim();

            if (zoneAdress.Length == 3)
            {
                if (int.TryParse(zoneAdress, out int zoneInt))
                {
                    zoneInt--;
                    return $"{((1 + (zoneInt >> 7)) * 1000) + (((zoneInt >> 3) & 0x0f) * 10) + ((zoneInt & 0x07) + 1)}";
                }
                else
                {
                    throw new ArgumentException($"Zone adress is not an integer number: {zoneAdress}");
                }
            }
            else if (zoneAdress.Length == 4)
            {
                try
                {
                    int line = int.Parse($"{zoneAdress[0]}");
                    int rio = int.Parse($"{zoneAdress[1]}") * 10 + int.Parse($"{zoneAdress[2]}");
                    int zone = int.Parse($"{zoneAdress[3]}");

                    return $"{((line - 1) * 128) + (rio * 8) + (zone)}".PadLeft(3, '0');
                }
                catch (Exception)
                {
                    throw new ArgumentException($"Invalid zone adress: {adress}");
                }
            }
            else
            {
                throw new ArgumentException($"Invalid zone adress: {adress}");
            }
        }

        /// <summary>
        /// Checks whether the given string is a valid zone adress (3 or 4 digit).
        /// </summary>
        /// <param name="zoneAdress">The string to check.</param>
        /// <returns>true if a valid zone adress, false else.</returns>
        public static bool IsValidZoneAdress(string zoneAdress)
        {
            if (string.IsNullOrWhiteSpace(zoneAdress))
            {
                return false;
            }

            if (zoneAdress.Length == 3)
            {
                if (int.TryParse(zoneAdress, out int addrInt))
                {
                    if (addrInt >= 1 && addrInt <= 512)
                    {
                        return true;
                    }
                }
                return false;
            }
            else if (zoneAdress.Length == 4)
            {
                try
                {
                    int line = int.Parse($"{zoneAdress[0]}");
                    int rio = int.Parse($"{zoneAdress[1]}") * 10 + int.Parse($"{zoneAdress[2]}");
                    int zone = int.Parse($"{zoneAdress[3]}");

                    if (line >= 0 && line <= 4)
                    {
                        if (rio >= 0 && rio < 16)
                        {
                            if (zone > 0 && zone <= 8)
                            {
                                return true;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    Logger.Error($"Cannot parse zone adress: {zoneAdress}");
                }
            }

            return false;
        }

        /// <summary>
        /// Overload of IsValidZoneAdress for int.
        /// </summary>
        /// <param name="zoneId">Zone adress.</param>
        /// <returns>True if valid, false else.</returns>
        public static bool IsValidZoneAdress(int zoneId)
        {
            return IsValidZoneAdress($"{zoneId}");
        }

        /// <summary>
        /// Extension method to get a bit from a byte.
        /// </summary>
        /// <param name="theByte">The byte to get the bit from.</param>
        /// <param name="bitIndex">Index of the bit (0-7)</param>
        /// <returns></returns>
        public static bool GetBit(this byte theByte, int bitIndex)
        {
            if (bitIndex < 0 || bitIndex > 7)
            {
                throw new ArgumentException($"Index out of range: {bitIndex}");
            }
            return (theByte & (1 << bitIndex)) != 0;
        }

        /// <summary>
        /// Extension method to set a bit on a byte.
        /// </summary>
        /// <param name="theByte">The byte to set the bit on.</param>
        /// <param name="bitIndex">Index of the bit (0-7)</param>
        /// <param name="value">The bit value to set.</param>
        /// <returns>The modified byte.</returns>
        public static byte SetBit(this byte theByte, int bitIndex, bool value)
        {
            byte mask = (byte)(1 << bitIndex);

            theByte = (byte)(value ? (theByte | mask) : (theByte & ~mask));
            return theByte;
        }
    }
}

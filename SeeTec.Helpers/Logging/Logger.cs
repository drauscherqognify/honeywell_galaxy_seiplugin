﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using log4net;

namespace SeeTec.Helpers.Logging
{
    /// <summary>
    /// Wraps Log4Net into a static logger implementing its own log level.
    /// Note: Since this Logger is specialized to "hijack" the Cayuga Log4Net logger, it always
    /// logs on Log4Net log level "Info", and applies its own log level!
    /// </summary>
    public class Logger
    {
        /// <summary>
        /// Log levels.
        /// </summary>
        public enum LogLevel
        {
            TRC = 0,
            DBG = 1,
            INF = 2,
            WRN = 3,
            ERR = 4,
        }

        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Log level set for this logger. 
        /// </summary>
#if DEBUG
        private static LogLevel _desiredLogLevel = LogLevel.TRC;    // Default log level during development is TRACE
#else
        private static LogLevel _desiredLogLevel = LogLevel.INF;    // Default log level in production is INFO
#endif

        /// <summary>
        /// Prefix for the message, if any.
        /// </summary>
        private static string _messagePrefix = "";

        /// <summary>
        /// Sets the log file name for the Logger, automatically switching from console logging to file logging.
        /// </summary>
        /// <param name="logLevel">Desired log level for the logger.</param>
        /// <param name="logFileName">File name for the log file, including full path.</param>
        public static void SetLogLevel(LogLevel logLevel)
        {
            _desiredLogLevel = logLevel;
            Trace($"Set log level to {logLevel}");
        }

        /// <summary>
        /// Sets the prefix for every log message. May  be blank.
        /// Best practise for SEI Plugins: "<pluginName>"
        /// </summary>
        /// <param name="messagePrefix"></param>
        public static void SetMessagePrefix(string messagePrefix)
        {
            _messagePrefix = messagePrefix;
        }

        /// <summary>
        /// Internal method to do the actual logging.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="optionalLogLevel"></param>
        private static void Log(string message, LogLevel optionalLogLevel, string callerFilePath = null)
        {
            if (optionalLogLevel >= _desiredLogLevel)
            {
                var callerTypeName = Path.GetFileNameWithoutExtension(callerFilePath);
                _log.Info($"{_messagePrefix} [{optionalLogLevel}] ({callerTypeName}) {message}");
            }
        }

        /// <summary>
        /// Overload of SetLogLevel to allow strings.
        /// </summary>
        /// <param name="logLevel">Log level to set.</param>
        public static void SetLogLevel(string logLevel)
        {
            SetLogLevel(String2LogLevel(logLevel));
        }

        /// <summary>
        /// Convenience method for logging to error log level.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="param">Optional parameters for the message, if formatting tags are given.</param>
        public static void Error(string message, [CallerFilePath] string callerFilePath = null)
        {
            Log(message, LogLevel.ERR, callerFilePath);
        }


        /// <summary>
        /// Convenience method for logging to debug log level.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="param">Optional parameters for the message, if formatting tags are given.</param>
        public static void Debug(string message, [CallerFilePath] string callerFilePath = null)
        {
            Log(message, LogLevel.DBG, callerFilePath);
        }

        /// <summary>
        /// Convenience method for logging to info log level.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="param">Optional parameters for the message, if formatting tags are given.</param>
        public static void Info(string message, [CallerFilePath] string callerFilePath = null)
        {
            Log(message, LogLevel.INF, callerFilePath);
        }

        /// <summary>
        /// Convenience method for logging to warning log level.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="param">Optional parameters for the message, if formatting tags are given.</param>
        public static void Warn(string message, [CallerFilePath] string callerFilePath = null)
        {
            Log(message, LogLevel.WRN, callerFilePath);
        }

        /// <summary>
        /// Convenience method for logging to trace log level.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="param">Optional parameters for the message, if formatting tags are given.</param>
        public static void Trace(string message, [CallerFilePath] string callerFilePath = null)
        {
            Log(message, LogLevel.TRC, callerFilePath);
        }

        /// <summary>
        /// Helper method for convenient conversion between string and LogLevel enum. 
        /// </summary>
        /// <param name="logLevelString">Log level as string.</param>
        /// <returns>Corresponding log level as enum value, or INF if string is not recognized.</returns>
        public static LogLevel String2LogLevel(string logLevelString)
        {
            switch (logLevelString?.ToUpper())
            {
                case "TRC":
                case "TRACE":
                    return LogLevel.TRC;
                case "DBG":
                case "DEBUG":
                    return LogLevel.DBG;
                case "WRN":
                case "WARN":
                case "WARNING":
                    return LogLevel.WRN;
                case "ERR":
                case "ERROR":
                    return LogLevel.ERR;
                case "INF":
                case "INFO":
                case "INFORMATION":
                default:
                    return LogLevel.INF;
            }
        }
    }
}

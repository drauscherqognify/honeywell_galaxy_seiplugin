﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;
using SeeTec.Helpers.Logging;

namespace SeeTec.HoneywellGalaxy.Configuration.ICT
{
    /// <summary>
    /// Static helper class to Honeywell Galaxy event definitions from a CSV file.
    /// </summary>
    public static class EventDefinitionCSVReader
    {
        /// <summary>
        /// Reads the Honeywell Galaxy event definition from the given file.
        /// </summary>
        /// <param name="path">File to read from, including full path.</param>
        /// <returns>List of <see cref="SIAEventDefinition"/> contained in the CSV file.</returns>
        public static List<SIAEventDefinition> ReadEventDefinition(string path)
        {
            List<SIAEventDefinition> result = new List<SIAEventDefinition>();

            using (TextFieldParser csvParser = new TextFieldParser(path))
            {
                csvParser.CommentTokens = new string[] { "#" };
                csvParser.SetDelimiters(new string[] { ";" });
                csvParser.HasFieldsEnclosedInQuotes = true;

                // Skip the row with the column names
                csvParser.ReadLine();

                Logger.Info("Reading event definitions...");

                while (!csvParser.EndOfData)
                {
                    // Read current line fields, pointer moves to the next line.
                    string[] fields = csvParser.ReadFields();

                    if (fields.Length != 9)
                    {
                        Logger.Error($"Invalid event definition: {fields}");
                    }

                    string eventCode = fields[0].Trim();
                    string eventDescription = fields[1].Trim();
                    string logEvent = fields[2].Trim();
                    string logEventDescription = fields[3].Trim();
                    string eventType = fields[4].Trim();
                    string trigger = fields[5].Trim();
                    string sia01 = fields[6].Trim();
                    string sia24 = fields[7].Trim();
                    string category = fields[8].Trim();

                    Logger.Trace($"{eventCode}\t{eventDescription}\t{logEvent}\t{logEventDescription}\t{eventType}\t{trigger}\t{sia01}\t{sia24}\t{category}");


                    var newEventDefinition = new SIAEventDefinition(eventCode, eventDescription, logEvent, logEventDescription, eventType, trigger, sia01, sia24, category);

                    result.Add(newEventDefinition);
                }
            }
            Logger.Info($"Done, read {result.Count} event definitions.");
            return result;
        }
    }
}

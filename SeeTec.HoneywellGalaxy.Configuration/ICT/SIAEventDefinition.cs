﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeTec.HoneywellGalaxy.Configuration.ICT
{
    /// <summary>
    /// Represents a SIA event definition for Honeywell Galaxy.
    /// </summary>
    public class SIAEventDefinition
    {
        /// <summary>
        /// The event code of the event. This is the event code contained in the SIA message.
        /// </summary>
        public string EventCode { get; private set; }

        /// <summary>
        /// Human readable description for the event code.
        /// </summary>
        public string EventDescription { get; private set; }

        /// <summary>
        /// Log event (sub-event) of the event code.
        /// </summary>
        public string LogEvent { get; private set; }

        /// <summary>
        /// Human readable description of the log event.
        /// </summary>
        public string LogEventDescription { get; private set; }

        /// <summary>
        /// Item type that may issue the event.
        /// </summary>
        public string ItemType { get; private set; }

        /// <summary>
        /// Trigger (??).
        /// </summary>
        public string Trigger { get; private set; }

        /// <summary>
        /// SIA level 0,1 format of the SIA message corresponding to the event.
        /// </summary>
        public string SIA01 { get; private set; }

        /// <summary>
        /// SIA level 2,3,4 format of the SIA message corresponding to the event.
        /// </summary>
        public string SIA24 { get; private set; }

        /// <summary>
        /// Category of the event, used for grouping.
        /// </summary>
        public string Category { get; private set; }

        /// <summary>
        /// Creates a new event definition.
        /// </summary>
        /// <param name="eventCode">SIA event code.</param>
        /// <param name="eventDescription">Human readable description of the event code.</param>
        /// <param name="logEvent">SIA log event.</param>
        /// <param name="logEventDescription">Human readable description of the log event.</param>
        /// <param name="eventType">Item type which may issue this event.</param>
        /// <param name="trigger">Trigger (???).</param>
        /// <param name="sia01">SIA level 0,1 format of the SIA message corresponding to the event.</param>
        /// <param name="sia24">SIA level 2,3,4 format of the SIA message corresponding to the event.</param>
        /// <param name="category">Category of the event, used for grouping.</param>
        public SIAEventDefinition(string eventCode, string eventDescription, string logEvent, string logEventDescription, string eventType, string trigger, string sia01, string sia24, string category)
        {
            EventCode = eventCode;
            EventDescription = eventDescription;
            LogEvent = logEvent;
            LogEventDescription = logEventDescription;
            ItemType = eventType;
            Trigger = trigger; 
            SIA01 = sia01;
            SIA24 = sia24;
            Category = category;
        }
    }
}

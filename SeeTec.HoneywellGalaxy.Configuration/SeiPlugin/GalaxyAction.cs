﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeTec.HoneywellGalaxy.Configuration.SeiPlugin
{
    /// <summary>
    /// Represents a Galaxy Action definition. 
    /// </summary>
    public class GalaxyAction
    {
        /// <summary>
        /// Cayuga ID of the action. 
        /// </summary>
        public int CayugaId { get; private set; }

        /// <summary>
        /// Third party action ID.
        /// </summary>
        public string ActionID { get; private set; }

        /// <summary>
        /// Third party item type ID this action is applicable to.
        /// </summary>
        public string ItemTypeID { get; private set; }

        /// <summary>
        /// Description (display name) of the action.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// SIA Command representing the action.
        /// </summary>
        public string Command { get; private set; }

        /// <summary>
        /// SIA Value (sub-command) of the action.
        /// </summary>
        public int Value { get; private set; }

        /// <summary>
        /// Creates a new action.
        /// </summary>
        /// <param name="cayugaId"></param>
        /// <param name="actionId"></param>
        /// <param name="description"></param>
        /// <param name="itemTypeId"></param>
        /// <param name="command"></param>
        /// <param name="value"></param>
        public GalaxyAction(int cayugaId, string actionId, string description, string itemTypeId, string command, int value)
        {
            CayugaId = cayugaId;
            ActionID = actionId;
            ItemTypeID = itemTypeId;
            Description = description;
            Command = command;
            Value = value;
        }
    }
}

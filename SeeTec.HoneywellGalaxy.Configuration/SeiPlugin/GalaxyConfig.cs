﻿using SeeTec.EventInterface;
using SeeTec.Helpers.Logging;
using SeeTec.SEIPluginConfiguration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SeeTec.HoneywellGalaxy.Configuration.SeiPlugin
{
    /// <summary>
    /// Manages the SEI Plugin configuration.
    /// </summary>
    public class GalaxyConfig
    {
        public const int UNKNOWN_STATE_TYPE_ID = int.MaxValue;
        public const string UNKNOWN_STATE_TYPE_NAME = "Unknown";

        public const string ACTION_GROUP_UNSET = "Unset";
        public const string ACTION_GROUP_SET = "Set";
        public const string ACTION_GROUP_PARTSET = "Part set";
        public const string ACTION_GROUP_SYSTEMRESET = "System reset";
        public const string ACTION_GROUP_ABORTSET = "Abort set";
        public const string ACTION_GROUP_FORCESET = "Force set";

        public const string ACTION_ZONE_UNOMIT = "Un-omit";
        public const string ACTION_ZONE_OMIT = "Omit";
        public const string ACTION_ZONE_SOAKTESTOFF = "Soak Test Off";
        public const string ACTION_ZONE_SOAKTESTON = "Soak Test On";
        public const string ACTION_ZONE_PARTSETOFF = "Part Set Off";
        public const string ACTION_ZONE_PARTSETON = "Part Set On";
        public const string ACTION_ZONE_FORCEOFF = "Force Off";
        public const string ACTION_ZONE_FORCEON = "Force On";
        public const string ACTION_ZONE_FORCEONOFF = "Force On -> Off";
        public const string ACTION_ZONE_FORCETAMPER = "Force Tamper";

        public const string ACTION_OUTPUT_ON = "On";
        public const string ACTION_OUTPUT_OFF = "Off";

        public const string ZONE_STATE_CLOSED = "XZS0-40-0";
        public const string ZONE_STATE_LOWHIGH = "XZS0-40-1";
        public const string ZONE_STATE_OPEN = "XZS1-20-1";
        public const string ZONE_STATE_OCSC = "XZS1-30-1";
        public const string ZONE_STATE_MASKED = "XZS1-60-1";
        public const string ZONE_STATE_FAULT = "XZS1-70-1";
        public const string ZONE_STATE_NOALARM = "XZS-10-0";
        public const string ZONE_STATE_ALARM = "XZS-10-1";
        public const string ZONE_STATE_NOTOMIT = "XZS-50-0";
        public const string ZONE_STATE_OMIT = "XZS-50-1";

        /// <summary>
        /// Instance of the Plugin configuration.
        /// </summary>
        private readonly SEIPluginConfig _config;

        private List<GalaxyAction> _actionTypes;

        public GalaxyConfig(SEIPluginConfig config)
        {
            _config = config;

            CreateActionTypeConfig();
        }

        private void CreateActionTypeConfig()
        {
            // Create hard coded action types
            _actionTypes = new List<GalaxyAction>()
            {
                // Group
                NewAction(ItemTypeNames.ITEM_TYPE_TID_GROUP, ACTION_GROUP_UNSET, "CSA", 0),
                NewAction(ItemTypeNames.ITEM_TYPE_TID_GROUP, ACTION_GROUP_SET, "CSA", 1),
                NewAction(ItemTypeNames.ITEM_TYPE_TID_GROUP, ACTION_GROUP_PARTSET, "CSA", 2),
                NewAction(ItemTypeNames.ITEM_TYPE_TID_GROUP, ACTION_GROUP_SYSTEMRESET, "CSA", 3),
                NewAction(ItemTypeNames.ITEM_TYPE_TID_GROUP, ACTION_GROUP_ABORTSET, "CSA", 4),
                NewAction(ItemTypeNames.ITEM_TYPE_TID_GROUP, ACTION_GROUP_FORCESET, "CSA", 5),

                // Zone
                NewAction(ItemTypeNames.ITEM_TYPE_TID_ZONE, ACTION_ZONE_UNOMIT, "CSB", 0),
                NewAction(ItemTypeNames.ITEM_TYPE_TID_ZONE, ACTION_ZONE_OMIT, "CSB", 1),
                NewAction(ItemTypeNames.ITEM_TYPE_TID_ZONE, ACTION_ZONE_SOAKTESTOFF, "XZS", 0),
                NewAction(ItemTypeNames.ITEM_TYPE_TID_ZONE, ACTION_ZONE_SOAKTESTON, "XZS", 1),
                NewAction(ItemTypeNames.ITEM_TYPE_TID_ZONE, ACTION_ZONE_PARTSETOFF, "XZS", 2),
                NewAction(ItemTypeNames.ITEM_TYPE_TID_ZONE, ACTION_ZONE_PARTSETON, "XZS", 3),
                NewAction(ItemTypeNames.ITEM_TYPE_TID_ZONE, ACTION_ZONE_FORCEOFF, "XZS", 4),
                NewAction(ItemTypeNames.ITEM_TYPE_TID_ZONE, ACTION_ZONE_FORCEON, "XZS", 5),
                NewAction(ItemTypeNames.ITEM_TYPE_TID_ZONE, ACTION_ZONE_FORCEONOFF, "XZS", 6),
                NewAction(ItemTypeNames.ITEM_TYPE_TID_ZONE, ACTION_ZONE_FORCETAMPER, "XZS", 7),

                // Output
                NewAction(ItemTypeNames.ITEM_TYPE_TID_OUTPUT, ACTION_OUTPUT_ON, "COR", 1),
                NewAction(ItemTypeNames.ITEM_TYPE_TID_OUTPUT, ACTION_OUTPUT_OFF, "COR", 0)
            };
        }

        public List<GalaxyAction> GetActionsForItemType(string itemTypeTID)
        {
            return _actionTypes.FindAll(a => a.ItemTypeID.Equals(itemTypeTID));
        }

        private static GalaxyAction NewAction(string itemTypeTID, string action, string command, int value)
        {
            return new GalaxyAction($"{itemTypeTID}-{action}".GetHashCode(), $"{itemTypeTID}-{action}", action, itemTypeTID, command, value);
        }

        /// <summary>
        /// Gets all possible SEI Type Infos for the configured state types. 
        /// </summary>
        /// <returns>List of SEI Type Infos for the configured state types. </returns>
        public List<SeiTypeInfo> GetStateTypeInfos()
        {
            return GetStateTypeInfos(_config.ItemTypes);
        }

        /// <summary>
        /// Recursively gets the SEI Type Infos of State Types for the given list of Item Types. 
        /// </summary>
        /// <param name="itemTypeList">List of Item Types.</param>
        /// <returns>State Type infos associated with the Item Types.</returns>
        private List<SeiTypeInfo> GetStateTypeInfos(IList<SeiItemType> itemTypeList)
        {
            List<SeiTypeInfo> result = new List<SeiTypeInfo>();

            foreach (var itemType in itemTypeList)
            {
                foreach (var stateType in itemType.StateTypes)
                {
                    if (int.TryParse(stateType.Id, out int stateId))
                    {
                        SeiTypeInfo stateTypeInfo = new SeiTypeInfo(stateId, stateType.Name);
                        Logger.Trace($"{itemType.Name} state: [{stateId}] {stateType.Name}");
                        result.Add(stateTypeInfo);
                    }
                    else
                    {
                        Logger.Warn($"Unable to parse ID '{stateType.Id}' of state type '{stateType.Name}' to int. State Type skipped.");
                    }
                }

                // recurse through item sub-types
                result.AddRange(GetStateTypeInfos(itemType.SubItems));
            }

            // Add unknown state type
            result.Add(new SeiTypeInfo(UNKNOWN_STATE_TYPE_ID, UNKNOWN_STATE_TYPE_NAME));
            return result;
        }

        /// <summary>
        /// Gets the Cayuga ID of an item type third party ID.
        /// </summary>
        /// <param name="thirdPartyId">The item type third party ID.</param>
        /// <returns></returns>
        public int GetItemTypeId(string thirdPartyId)
        {
            var seiItemType = _config.ItemTypes.FirstOrDefault(it => it.ThirdPartyId.Equals(thirdPartyId));

            if (null == seiItemType)
            {
                throw new ArgumentException($"Cannot find item type with third party ID '{thirdPartyId}'");
            }

            string strId = seiItemType.Id;

            if (int.TryParse(strId, out int result))
            {
                return result;
            }

            throw new ArgumentException($"Cannot parse ID '{strId}' of item type '{thirdPartyId}' to integer value.");
        }

        /// <summary>
        /// Gets the third party id of an item type for the given Cayuga item type id. 
        /// </summary>
        /// <param name="cayugaId">The Cayuga ID of the item type.</param>
        /// <returns>The third party item type.</returns>
        public string GetItemTypeThirdPartyId(int cayugaId)
        {
            var seiItemType = _config.ItemTypes.FirstOrDefault(it => it.Id.Equals($"{cayugaId}"));

            if (null == seiItemType)
            {
                throw new ArgumentException($"Cannot find item type with Cayuga ID '{cayugaId}'");
            }

            return seiItemType.ThirdPartyId;
        }

        /// <summary>
        /// Gets a list of all active item types.
        /// </summary>
        /// <returns>List of all active item types.</returns>
        public IEnumerable<SeiItemType> GetActiveItemTypes()
        {
            //List<SeiItemType> itemTypes = Array.Find<SeiItemType>(_config.ItemTypes, it => it.Active == true);
            return _config.ItemTypes.Where(it => it.Active == true);
        }


        /// <summary>
        /// Gets all possible SEI Type Infos for the configured Event types. 
        /// </summary>
        /// <returns>List of SEI Type Infos for the configured Event types. </returns>
        public List<SeiTypeInfo> GetEventTypeInfos()
        {
            List<SeiTypeInfo> result = new List<SeiTypeInfo>();

            List<SeiTypeInfo> nativeEventTypeInfos = GetEventTypeInfos(_config.NativeEventTypes, true);
            List<SeiTypeInfo> customEventTypeInfos = GetEventTypeInfos(_config.CustomEventTypes, false);

            result.AddRange(nativeEventTypeInfos);
            result.AddRange(customEventTypeInfos);

            return result;
        }

        /// <summary>
        /// Helper method to (optionally recursively) get all possible event type infos for the given list
        /// of event types.
        /// </summary>
        /// <param name="eventTypeList">List of event types.</param>
        /// <param name="considerChildren">Flag whether the method should recurse through child event types of the given event types.</param>
        /// <returns>List of SEI Type Infos.</returns>
        private List<SeiTypeInfo> GetEventTypeInfos(IList<SeiEventType> eventTypeList, bool considerChildren)
        {
            List<SeiTypeInfo> result = new List<SeiTypeInfo>();

            foreach (var et in eventTypeList)
            {
                if (int.TryParse(et.Id, out int neId))
                {
                    SeiTypeInfo neTypeInfo = new SeiTypeInfo(neId, et.Name);
                    result.Add(neTypeInfo);
                    Logger.Trace($"Event Type: [{neId}] {et.Name}");

                    if (true == considerChildren)
                    {
                        result.AddRange(GetEventTypeInfos(et.Children, considerChildren));
                    }
                }
                else
                {
                    Logger.Error($"Unable to parse ID '{et.Id}' of event type '{et.Name}' to int. Event Type skipped.");
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all possible SEI Type Infos for the configured Item types. 
        /// </summary>
        /// <returns>List of SEI Type Infos for the configured Item types. </returns>
        public List<SeiItemTypeInfo> GetItemTypeInfos()
        {
            List<SeiItemTypeInfo> result = new List<SeiItemTypeInfo>();

            foreach (var itDef in _config.ItemTypes)
            {
                if (!itDef.Active)
                {
                    Logger.Debug($"Inactive Item Type: [{itDef.Id}] {itDef.Name}");
                    continue;
                }

                if (true == int.TryParse(itDef.Id, out int newId))
                {
                    List<int> stateTypeIds = GetStateTypeIds(itDef);
                    List<int> eventTypeIds = GetEventTypeIds(itDef);
                    List<int> actionTypeIds = GetActionTypeIds(itDef.ThirdPartyId);

                    SeiItemTypeInfo newItemTypeInfo = new SeiItemTypeInfo(newId, itDef.Name, stateTypeIds, eventTypeIds, actionTypeIds);

                    Logger.Info($"Active Item Type: [{newId}] {itDef.Name} ({stateTypeIds.Count - 1}/{itDef.StateTypes.Count} states active, {eventTypeIds.Count} events assigned, {actionTypeIds.Count} actions)");
                    result.Add(newItemTypeInfo);
                }
                else
                {
                    Logger.Error($"Unable to parse ID '{itDef.Id}' of item type '{itDef.Name}' to int. Item Type skipped.");
                    continue;
                }
            }
            return result;
        }

        /// <summary>
        /// Gets a list of Action Type IDs for the given Item Type third party ID.
        /// </summary>
        /// <param name="itemTypeTID">The Item Type third party ID.</param>
        /// <returns>List of all action type ids.</returns>
        private List<int> GetActionTypeIds(string itemTypeTID)
        {
            List<int> result = new List<int>();

            var itemActions = _actionTypes.FindAll(a => a.ItemTypeID.Equals(itemTypeTID));
            
            foreach (var a in itemActions)
            {
                Logger.Trace($"Item type '{itemTypeTID}' has action '{a.Description}'");
                result.Add(a.CayugaId);
            }

            return result;
        }

        /// <summary>
        /// Gets all possible SEI Type Infos for the Action types. 
        /// </summary>
        /// <returns>List of SEI Type Infos for the Action types. </returns>
        public IEnumerable<SeiTypeInfo> GetActionTypeInfos()
        {
            List<SeiTypeInfo> result = new List<SeiTypeInfo>();

            foreach (var a in _actionTypes)
            {
                SeiTypeInfo ti = new SeiTypeInfo(a.CayugaId, a.Description);
                result.Add(ti);
            }

            return result;
        }

        /// <summary>
        /// Gets Event Type IDs for the given itemType.
        /// </summary>
        /// <param name="itemType">The item type to consider.</param>
        /// <returns>List event type IDs.</returns>
        private List<int> GetEventTypeIds(SeiItemType itemType)
        {
            List<int> result = new List<int>();

            foreach (var et in itemType.AssignedEventTypes)
            {
                if (int.TryParse(et.Id, out int etId))
                {
                    result.Add(etId);
                }
                else
                {
                    Logger.Error($"Unable to parse ID '{et.Id}' of event type '{et.Name}' to int. Event Type skipped.");
                }
            }
            return result;
        }

        /// <summary>
        /// Gets State Type IDs for the given itemType.
        /// </summary>
        /// <param name="itemType">The item type to consider.</param>
        /// <returns>List of state type IDs.</returns>
        private List<int> GetStateTypeIds(SeiItemType itemType)
        {
            List<int> result = new List<int>();

            foreach (var st in itemType.StateTypes)
            {
                if (false == st.Active)
                {
                    Logger.Trace($"{itemType.Name} State Type inactive: {st.Name}");
                    continue;
                }

                if (int.TryParse(st.Id, out int stId))
                {
                    Logger.Trace($"{itemType.Name} State Type active: {st.Name}");
                    result.Add(stId);
                }
                else
                {
                    Logger.Warn($"[{itemType}] Unable to parse ID '{st.Id}' of state type '{st.Name}' to int. State Type skipped.");
                }
            }

            // Always add unknown state type
            result.Add(UNKNOWN_STATE_TYPE_ID);

            return result;
        }

        public GalaxyAction FindActionById(int actionTypeId)
        {
            try
            {
                GalaxyAction action = _actionTypes.Single(a => a.CayugaId == actionTypeId);
                return action;
            }
            catch (InvalidOperationException)
            {
                Logger.Error($"Action for ID {actionTypeId} not found or ambiguous.");
                return null;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeTec.HoneywellGalaxy.Configuration.SeiPlugin
{
    public class ItemTypeNames
    {
        /// <summary>
        /// Item Type third party ID for the "GROUP" item type.
        /// </summary>
        public const string ITEM_TYPE_TID_GROUP = "GROUP";

        /// <summary>
        /// Item Type third party ID for the "ZONE" item type.
        /// </summary>
        public const string ITEM_TYPE_TID_ZONE = "ZONE";

        /// <summary>
        /// Item Type third party ID for the "USER" item type.
        /// </summary>
        public const string ITEM_TYPE_TID_USER = "USER";

        /// <summary>
        /// Item Type third party ID for the "MOD" item type.
        /// </summary>
        public const string ITEM_TYPE_TID_MOD = "MOD";

        /// <summary>
        /// Item Type third party ID for the "TEST" item type.
        /// </summary>
        public const string ITEM_TYPE_TID_TEST = "TEST";

        /// <summary>
        /// Item Type third party ID for the "EVENT" item type.
        /// </summary>
        public const string ITEM_TYPE_TID_EVENT = "EVENT";

        /// <summary>
        /// Item Type third party ID for the "OUTPUT" item type.
        /// </summary>
        public const string ITEM_TYPE_TID_OUTPUT = "OUTPUT";

        /// <summary>
        /// Item Type display name for the "GROUP" item type.
        /// </summary>
        public const string ITEM_TYPE_DISPLAY_NAME_GROUP = "Group";

        /// <summary>
        /// Item Type display name for the "ZONE" item type.
        /// </summary>
        public const string ITEM_TYPE_DISPLAY_NAME_ZONE = "Zone";

        /// <summary>
        /// Item Type display name for the "USER" item type.
        /// </summary>
        public const string ITEM_TYPE_DISPLAY_NAME_USER = "User";

        /// <summary>
        /// Item Type display name for the "MOD" item type.
        /// </summary>
        public const string ITEM_TYPE_DISPLAY_NAME_MOD = "Module";

        /// <summary>
        /// Item Type display name for the "TEST" item type.
        /// </summary>
        public const string ITEM_TYPE_DISPLAY_NAME_TEST = "Test";

        /// <summary>
        /// Item Type display name for the "EVENT" item type.
        /// </summary>
        public const string ITEM_TYPE_DISPLAY_NAME_EVENT = "Panel";

        /// <summary>
        /// Item Type display name for the "EVENT" item type.
        /// </summary>
        public const string ITEM_TYPE_DISPLAY_NAME_OUTPUT = "Output";
    }
}

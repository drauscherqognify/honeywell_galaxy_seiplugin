﻿using SeeTec.EventInterface;
using SeeTec.Helpers.Logging;
using SeeTec.HoneywellGalaxy.Configuration.SeiPlugin;
using SeeTec.HoneywellGalaxy.SEIPlugin.Helper;
using SeeTec.HoneywellGalaxy.SIAConnector;
using SeeTec.HoneywellGalaxy.SIAConnector.SIA;
using SeeTec.SEIPluginConfiguration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace SeeTec.HoneywellGalaxy.ConsoleTest
{
    /// <summary>
    /// Console testing application for development...
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Gets the version of this plugin. 
        /// </summary>
        public static string Version => System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

        private static void Main(string[] args)
        {
            Logger.Info("============= STARTUP ============");
            Logger.SetLogLevel(Logger.LogLevel.TRC);

            // TestPluginParameters();
            // TestPluginConfiguration();
            TestSocketListener();
            //TestSendCommand();
            // TestSIACommandSocketLoop("CSA01", 1000);
            // TestSIACommandSocket();
            //TestZoneAdressing();
            //            TestStateHandler();
            //TestOutputCommandBytes();
            // TestSIAMessage();
            ///TestAction();

            Console.WriteLine("Press enter to exit...");
            Console.ReadLine();
            Logger.Info("============== EXIT ==============");
        }

        private static void TestAction()
        {
            GalaxyAction action = new GalaxyAction(-123, GalaxyConfig.ACTION_OUTPUT_ON, "Output on", ItemTypeNames.ITEM_TYPE_TID_OUTPUT, "COR", 1);
            SIACommandSocket cmdSock = new SIACommandSocket("172.16.16.241", "543210");
            cmdSock.ExecuteAction(action, "001");
        }

        private static void TestOutputCommandBytes()
        {
            
            for (int num = 1; num <= 256; num++)
            {
                string itemTID = $"{num}".PadLeft(3, '0');

                // we need 32 null bytes
                byte[] bytes = new byte[32];

                // try to get group id as int
                if (int.TryParse(itemTID, out int intId))
                {
                    // per byte 4 bits mask, 4 bits op
                    if (intId > 128)
                    {
                        // Roll over
                        intId = intId - 128;
                    }

                    int byteIndex = ((intId - 1) / 4);

                    if (byteIndex > (bytes.Length) - 1)
                    {
                        // byte index outside array, error
                    }



                int opBitIndex = ((intId - 1) % 4);
                int maskBitIndex = ((intId - 1) % 4) + 4;

                Logger.Info($"{itemTID} -> byteIndex {byteIndex}, maskBit {maskBitIndex}, opBit {opBitIndex}");
                }
            }
        }

        private static void TestZoneAdressing()
        {
            for (int num = 1; num <= 512; num++)
            {
                string s3 = $"{num}".PadLeft(3, '0');
                string s4 = GalaxyHelper.ConvertZoneAdress(s3);
                string s3b = GalaxyHelper.ConvertZoneAdress(s4);

                int adress = int.Parse(s3);
                int byteIndex = ((adress - 1) / 8);
                int zoneIndexWithinByte = ((adress - 1) % 8);

                Console.WriteLine($"{s3} ({GalaxyHelper.IsValidZoneAdress(s3)}) -> {s4} ({GalaxyHelper.IsValidZoneAdress(s4)}) -> {s3b} ({GalaxyHelper.IsValidZoneAdress(s3b)}) ==> byte index {byteIndex}, zone index {zoneIndexWithinByte}");
            }

            Console.WriteLine("==============");
            string s1 = "1874";
            string s2 = GalaxyHelper.ConvertZoneAdress(s1);
            string sb = GalaxyHelper.ConvertZoneAdress(s2);
            Console.WriteLine($"{s1} -> {s2} -> {sb} === ({GalaxyHelper.IsValidZoneAdress(s1)})");

        }



        private static void TestSIAMessage()
        {
            SIAMessage m = new SIAMessage("CSA91*20000000000000000000000000000000", true);

        }

        private static void TestStateHandler()
        {
            Logger.SetLogLevel(Logger.LogLevel.TRC);

            string filename = @"C:\dev\SeeTec\Honeywell Galaxy\ICT\SeeTec.HoneywellGalaxy.SEIPlugin.xml";
            SEIPluginConfig loadedConfig = null;

            Logger.Info($"Loading configuration from {filename}");
            loadedConfig = SEIPluginConfigurationSerializer.ReadPluginConfiguration(filename);

            GalaxyConfig configManager = new GalaxyConfig(loadedConfig);

            SIACommandSocket cmdSock = new SIACommandSocket("172.16.16.241", "543210");
        }

        private static void TestSIACommandSocketLoop(string cmd, int numTries)
        {
            Logger.SetLogLevel(Logger.LogLevel.TRC);
            SIACommandSocket cmdSock = new SIACommandSocket("172.16.16.241", "543210");

            int errorCount = 0;
            Stopwatch sw = new Stopwatch();
            long totalMs = 0;

            for (int i = 0; i <= numTries; i++)
            {
                sw.Start();
                Logger.Info($"===== Try #{i + 1}");
                SIAMessage reply = null;

                if (cmd.Length > 1)
                {
                    reply = cmdSock.SendMessage(new SIAMessage(cmd[0], cmd.Substring(1)).RawMessage);
                }
                else if (cmd.Length > 0)
                {
                    reply = cmdSock.SendMessage(new SIAMessage(cmd[0], "").RawMessage);
                }
                else
                {
                    continue;
                }

                if (null == reply)
                {
                    Logger.Error("Command not successful.");

                    errorCount++;
                }
                Logger.Info($"Reply: {reply}");
                sw.Stop();
                var elapsed = sw.ElapsedMilliseconds;
                sw.Reset();
                totalMs += elapsed;
                Logger.Trace($"Took: {elapsed}ms");
            }
            Logger.Info($"===== FINISHED =====");
            Logger.Info($"Total:\t{numTries - errorCount} tries in {totalMs} ms.");
            Logger.Info($"Success:\t{numTries - errorCount}");
            Logger.Info($"Fails:\t{errorCount}");
            Logger.Info($"Average time per try: {totalMs / numTries} ms.");
            Logger.Info($"===================");
        }

        private static void TestSIACommandSocket()
        {
            SIACommandSocket cmdSock = new SIACommandSocket("172.16.16.241", "543210");

            // Get Panel version
            // var version = cmdSock.GetPanelVersion();
            //
            //  Logger.Info($"Panel version: {version}");


            while (true)
            {
                Console.Write("CMD> ");
                string cmd = Console.ReadLine();
                SIAMessage reply = null;

                if (cmd.Length > 1)
                {
                    reply = cmdSock.SendMessage(new SIAMessage(cmd[0], cmd.Substring(1)).RawMessage);
                }
                else if (cmd.Length > 0)
                {
                    reply = cmdSock.SendMessage(new SIAMessage(cmd[0], "").RawMessage);
                }
                else
                {
                    continue;
                }

                Logger.Info($"Reply: {reply}");
            }
        }

        private static void TestPluginParameters()
        {
            var optionalParams = SEIPlugin.Parameters.PluginParameters.AvailableOptionalParameters;
            var requiredParams = SEIPlugin.Parameters.PluginParameters.AvailableOptionalParameters;
        }

        private static void TestPluginConfiguration()
        {
            Logger.SetLogLevel(Logger.LogLevel.DBG);

            string filename = @"C:\dev\SeeTec\Honeywell Galaxy\ICT\SeeTec.HoneywellGalaxy.SEIPlugin.xml";
            SEIPluginConfig loadedConfig = null;

            Logger.Info($"Loading configuration from {filename}");
            loadedConfig = SEIPluginConfigurationSerializer.ReadPluginConfiguration(filename);

            GalaxyConfig configManager = new GalaxyConfig(loadedConfig);

            var allEventIds = configManager.GetEventTypeInfos();

            var allStateIds = configManager.GetStateTypeInfos();

            List<SeiItemTypeInfo> itemTypes = configManager.GetItemTypeInfos();

            var activeItemTypes = configManager.GetActiveItemTypes();

            Logger.Info($"{ItemTypeNames.ITEM_TYPE_TID_GROUP}: {configManager.GetItemTypeId(ItemTypeNames.ITEM_TYPE_TID_GROUP)}");
        }

        private static void TestSocketListener()
        {
            SIAListenerSocket listener = new SIAListenerSocket(GotEvent, 10002, new List<string>());
            listener.StartListening();
        }

        private static void GotEvent(SIAEvent evtArgs)
        {
            Logger.Debug($"Event occured: {evtArgs.ToString()}");
        }

        public static void PrintCodepageInfo()
        {
            // Print the header.
            Console.Write("Info.CodePage      ");
            Console.Write("Info.Name                    ");
            Console.Write("Info.DisplayName");
            Console.WriteLine();

            // Display the EncodingInfo names for every encoding, and compare with the equivalent Encoding names.
            foreach (EncodingInfo ei in Encoding.GetEncodings())
            {
                Encoding e = ei.GetEncoding();

                Console.Write("{0,-15}", ei.CodePage);
                if (ei.CodePage == e.CodePage)
                    Console.Write("    ");
                else
                    Console.Write("*** ");

                Console.Write("{0,-25}", ei.Name);
                if (ei.CodePage == e.CodePage)
                    Console.Write("    ");
                else
                    Console.Write("*** ");

                Console.Write("{0,-25}", ei.DisplayName);
                if (ei.CodePage == e.CodePage)
                    Console.Write("    ");
                else
                    Console.Write("*** ");

                Console.WriteLine();
            }
        }

        private static void TestSendCommand()
        {
            IPEndPoint ep = new IPEndPoint(IPAddress.Any, 10005);

            // Data buffer for incoming data.  
            byte[] bytes = new Byte[1024];
            string data = null;

            // Bind the socket to the local endpoint and   
            // listen for incoming connections.  
            try
            {
                //sock.Bind(ep);
                //sock.Listen(10);

                while (true)
                {
                    Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    data = null;
                    Console.Write("CMD> ");
                    string cmd = Console.ReadLine();
                    sock.Connect("172.16.16.241", 10005);

                    SIAMessage msg = new SIAMessage('?', "543210");

                    sock.Send(msg.RawMessage);

                    Console.WriteLine("Busy wait for data...");
                    while (sock.Available < 1)
                    {
                        if (!sock.Connected)
                        {
                            Console.WriteLine("Socket disconnected!");
                            break;
                        }
                    }

                    int bytesRec = sock.Receive(bytes);
                    data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                    Logger.Debug($"Reply: {data}");

                    if (data.Contains("@AL4B15"))
                    {
                        Console.WriteLine("Login success.");
                        data = null;
                        if (cmd.Length > 0)
                        {
                            char blockCode = cmd[0];
                            string blockData = "";
                            if (cmd.Length > 1)
                            {
                                blockData = cmd.Substring(1, cmd.Length - 1);
                            }

                            msg = new SIAMessage(blockCode, blockData);
                            sock.Send(msg.RawMessage);

                            if (sock.Connected)
                            {
                                Console.WriteLine("Busy wait for data...");
                                while (sock.Available < 1)
                                {
                                    if (!sock.Connected)
                                    {
                                        Console.WriteLine("Socket disconnected!");
                                        break;
                                    }
                                }
                                bytesRec = sock.Receive(bytes);
                                data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                                Console.WriteLine($"Reply: {data}");
                            }
                            else
                            {
                                Console.WriteLine("Socket disconnected");
                            }
                        }
                    }
                    sock.Disconnect(true);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
    }
}

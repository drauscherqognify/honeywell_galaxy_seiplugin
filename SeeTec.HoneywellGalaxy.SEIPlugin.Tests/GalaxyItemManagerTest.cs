﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeeTec.HoneywellGalaxy.SEIPlugin.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeTec.HoneywellGalaxy.SEIPlugin.Tests
{
    [TestClass]
    public partial class GalaxyItemManagerTest
    {
        [TestMethod]
        public void TestExtractZoneStateBytes()
        {
            List<byte> test34Bytes = new List<byte>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35 };
            byte[] expected34false = new byte[] { 1, 2, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34 };
            byte[] expected34true = new byte[] { 1, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34 };

            byte[] test32Bytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32 };
            byte[] expected32 = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32 };


            var result = GalaxyItemManager.ExtractZoneBytes(test32Bytes, false, out byte vz32trueByte);
            Assert.AreEqual(32, result.Count());
            Assert.AreEqual(0, vz32trueByte);
            Assert.IsTrue(expected32.SequenceEqual(result));

            result = GalaxyItemManager.ExtractZoneBytes(test32Bytes, true, out byte vz32falseByte);
            Assert.AreEqual(32, result.Count());
            Assert.AreEqual(0, vz32falseByte);
            Assert.IsTrue(expected32.SequenceEqual(result));

            result = GalaxyItemManager.ExtractZoneBytes(test34Bytes, false, out byte vz34falseByte);
            Assert.AreEqual(32, result.Count());
            Assert.AreEqual(0, vz34falseByte);
            Assert.IsTrue(expected34false.SequenceEqual(result));

            result = GalaxyItemManager.ExtractZoneBytes(test34Bytes, true, out byte vz34trueByte);
            Assert.AreEqual(32, result.Count());
            Assert.AreEqual(2, vz34trueByte);
            Assert.IsTrue(expected34true.SequenceEqual(result));
        }
    }
}

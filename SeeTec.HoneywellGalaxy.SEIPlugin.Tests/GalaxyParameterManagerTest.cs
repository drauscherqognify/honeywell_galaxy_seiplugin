// <copyright file="GalaxyParameterManagerTest.cs">Copyright © SeeTec 2019</copyright>
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeeTec.EventInterface;
using SeeTec.HoneywellGalaxy.SEIPlugin.Parameters;
using System.Collections.Generic;

namespace SeeTec.HoneywellGalaxy.SEIPlugin.Tests
{
    /// <summary>This class contains parameterized unit tests for ParameterManager</summary>
    [TestClass]
    public partial class GalaxyParameterManagerTest
    {
        [TestMethod]
        public void TestPluginParameterAttribute()
        {
            var requiredParameters = PluginParameters.AvailableRequiredParameters;

            Assert.AreEqual(2, requiredParameters.Count);

            var pinParam = requiredParameters.Find(x => x.Name == "PIN");
            Assert.AreNotEqual(null, pinParam);
            Assert.AreEqual("PIN", pinParam.Name);
            Assert.IsTrue(pinParam.IsPassword);

            var hostParam = requiredParameters.Find(x => x.Name == "Host");
            Assert.AreNotEqual(null, hostParam);
            Assert.AreEqual("Host", hostParam.Name);
            Assert.IsFalse(hostParam.IsPassword);

            var optionalParameters = PluginParameters.AvailableOptionalParameters;
            var zoneIDsParam = optionalParameters.Find(x => x.Name == "Zone IDs");
            Assert.IsNotNull(zoneIDsParam);
            Assert.IsFalse(hostParam.IsPassword);
        }

        [TestMethod]
        public void TestParseIntListWithRangeParameter()
        {
            var strList = " 1001   ";
            var result = PluginParameters.ParseIntListWithRangeParameter(strList);
            Assert.AreEqual(typeof(List<int>), result.GetType());
            var castResult = (List<int>)result;
            Assert.AreEqual(1, castResult.Count);
            Assert.IsTrue(castResult.Contains(1001));

            strList = " 1001   - 1008   ";
            result = PluginParameters.ParseIntListWithRangeParameter(strList);
            Assert.AreEqual(typeof(List<int>), result.GetType());
            castResult = (List<int>)result;
            Assert.AreEqual(8, castResult.Count);
            Assert.IsTrue(castResult.Contains(1001));
            Assert.IsTrue(castResult.Contains(1002));
            Assert.IsTrue(castResult.Contains(1003));
            Assert.IsTrue(castResult.Contains(1004));
            Assert.IsTrue(castResult.Contains(1005));
            Assert.IsTrue(castResult.Contains(1006));
            Assert.IsTrue(castResult.Contains(1007));
            Assert.IsTrue(castResult.Contains(1008));

            strList = "  231 , 1001   - 1008  , 1012, 1005 ";
            result = PluginParameters.ParseIntListWithRangeParameter(strList);
            Assert.AreEqual(typeof(List<int>), result.GetType());
            castResult = (List<int>)result;
            Assert.AreEqual(10, castResult.Count);
            Assert.IsTrue(castResult.Contains(1001));
            Assert.IsTrue(castResult.Contains(1002));
            Assert.IsTrue(castResult.Contains(1003));
            Assert.IsTrue(castResult.Contains(1004));
            Assert.IsTrue(castResult.Contains(1005));
            Assert.IsTrue(castResult.Contains(1006));
            Assert.IsTrue(castResult.Contains(1007));
            Assert.IsTrue(castResult.Contains(1008));
            Assert.IsTrue(castResult.Contains(231));
            Assert.IsTrue(castResult.Contains(1012));
        }

        [TestMethod]
        public void TestParseStringListParameter()
        {
            var strList = "  231 , 1001   - 1008  , 1012, 1005 ";
            var result = PluginParameters.ParseStringListParameter(strList);
            Assert.AreEqual(typeof(List<string>), result.GetType());
            var castResult = (List<string>)result;
            Assert.AreEqual(4, castResult.Count);
            Assert.IsTrue(castResult.Contains("1001   - 1008"));
            Assert.IsTrue(castResult.Contains("1005"));
            Assert.IsTrue(castResult.Contains("231"));
            Assert.IsTrue(castResult.Contains("1012"));
        }

        [TestMethod]
        public void TestParseParameters()
        {
            List<SeiParameter> requiredParameters = new List<SeiParameter>();
            List<SeiParameter> optionalParameters = new List<SeiParameter>();

            requiredParameters.Add(new SeiParameter("PIN", "12345", true));
            requiredParameters.Add(new SeiParameter("Host", "192.168.1.1"));

            optionalParameters.Add(new SeiParameter("Zone IDs", "1001, 1005-1011, 1009,-1002,1020"));
            optionalParameters.Add(new SeiParameter("State refresh interval (secs)", "123"));
            optionalParameters.Add(new SeiParameter("Allowed Hosts", "192.168.1.2,192.168.1.3,192.168.1.6"));

            PluginParameters pm = new PluginParameters(requiredParameters, optionalParameters);

            Assert.AreEqual("12345", pm.Pin);
            Assert.AreEqual("192.168.1.1", pm.Host);
            Assert.AreEqual(10002, pm.EventPort);
            Assert.AreEqual(6, pm.ZoneIdList.Count);
            Assert.AreEqual(3, pm.AllowedHostList.Count);
            Assert.AreEqual(123, pm.StateRefreshIntervalSecs);
        }

        [TestMethod]
        public void TestInvalidListParameters()
        {
            List<SeiParameter> requiredParameters = new List<SeiParameter>();
            List<SeiParameter> optionalParameters = new List<SeiParameter>();

            requiredParameters.Add(new SeiParameter("PIN", "12345", true));
            requiredParameters.Add(new SeiParameter("Host", "192.168.1.1"));

            optionalParameters.Add(new SeiParameter("Zone IDs", "1005-1020"));
            optionalParameters.Add(new SeiParameter("Group IDs", "2,30-35,45"));
            optionalParameters.Add(new SeiParameter("Output IDs", "2,30-35, 255-258, 2314"));

            PluginParameters pm = new PluginParameters(requiredParameters, optionalParameters);

            Assert.IsFalse(pm.AreParametersValid);

            Assert.AreEqual(12, pm.ZoneIdList.Count);
            Assert.IsTrue(pm.ZoneIdList.Contains(1005));
            Assert.IsTrue(pm.ZoneIdList.Contains(1006));
            Assert.IsTrue(pm.ZoneIdList.Contains(1007));
            Assert.IsTrue(pm.ZoneIdList.Contains(1008));
            Assert.IsTrue(pm.ZoneIdList.Contains(1011));
            Assert.IsTrue(pm.ZoneIdList.Contains(1012));
            Assert.IsTrue(pm.ZoneIdList.Contains(1013));
            Assert.IsTrue(pm.ZoneIdList.Contains(1014));
            Assert.IsTrue(pm.ZoneIdList.Contains(1015));
            Assert.IsTrue(pm.ZoneIdList.Contains(1016));
            Assert.IsTrue(pm.ZoneIdList.Contains(1017));
            Assert.IsTrue(pm.ZoneIdList.Contains(1018));
            Assert.IsFalse(pm.ZoneIdList.Contains(1009));
            Assert.IsFalse(pm.ZoneIdList.Contains(1010));
            Assert.IsFalse(pm.ZoneIdList.Contains(1019));
            Assert.IsFalse(pm.ZoneIdList.Contains(1020));

            Assert.AreEqual(4, pm.GroupIdList.Count);
            Assert.IsTrue(pm.GroupIdList.Contains(2));
            Assert.IsTrue(pm.GroupIdList.Contains(30));
            Assert.IsTrue(pm.GroupIdList.Contains(31));
            Assert.IsTrue(pm.GroupIdList.Contains(32));
            Assert.IsFalse(pm.GroupIdList.Contains(33));
            Assert.IsFalse(pm.GroupIdList.Contains(34));
            Assert.IsFalse(pm.GroupIdList.Contains(35));
            Assert.IsFalse(pm.GroupIdList.Contains(45));

            Assert.AreEqual(9, pm.OutputIdList.Count);
            Assert.IsTrue(pm.OutputIdList.Contains(2));
            Assert.IsTrue(pm.OutputIdList.Contains(30));
            Assert.IsTrue(pm.OutputIdList.Contains(31));
            Assert.IsTrue(pm.OutputIdList.Contains(32));
            Assert.IsTrue(pm.OutputIdList.Contains(33));
            Assert.IsTrue(pm.OutputIdList.Contains(34));
            Assert.IsTrue(pm.OutputIdList.Contains(35));
            Assert.IsTrue(pm.OutputIdList.Contains(255));
            Assert.IsTrue(pm.OutputIdList.Contains(256));
            Assert.IsFalse(pm.OutputIdList.Contains(257));
            Assert.IsFalse(pm.OutputIdList.Contains(258));
            Assert.IsFalse(pm.OutputIdList.Contains(2314));
        }
    }
}

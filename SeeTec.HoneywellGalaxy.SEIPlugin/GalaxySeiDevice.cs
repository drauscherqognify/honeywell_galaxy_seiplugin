﻿using SeeTec.EventInterface;
using SeeTec.Helpers.Logging;
using SeeTec.HoneywellGalaxy.Configuration.SeiPlugin;
using SeeTec.HoneywellGalaxy.SEIPlugin.Items;
using SeeTec.HoneywellGalaxy.SEIPlugin.Parameters;
using SeeTec.HoneywellGalaxy.SIAConnector;
using SeeTec.HoneywellGalaxy.SIAConnector.SIA;
using SeeTec.SEIPluginConfiguration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeeTec.HoneywellGalaxy.SEIPlugin
{
    /// <summary>
    /// Honewell Galaxy SEI device.
    /// </summary>
    public class GalaxySeiDevice : SeiDevice
    {
        /// <summary>
        /// Reference to the configuration manager.
        /// </summary>
        private readonly GalaxyConfig _configManager;

        /// <summary>
        /// Reference to the parameter manager.
        /// </summary>
        private PluginParameters _parameterManager;

        /// <summary>
        /// Reference to the item manager.
        /// </summary>
        private GalaxyItemManager _itemManager;

        /// <summary>
        /// Reference to the socket used to send commands to the panel on.
        /// </summary>
        private SIACommandSocket _actionCommandSocket;

        /// <summary>
        /// Reference to the event listener.
        /// </summary>
        private SIAListenerSocket _eventListener;

        /// <summary>
        /// Version of the panel this device is connected to.
        /// </summary>
        private SIAPanelVersion _panelVersion;

        /// <summary>
        /// Creates a new instance of the Galaxy SEI Device.
        /// </summary>
        /// <param name="pluginConfigManager">The plugin configuration manager for this plugin.</param>
        public GalaxySeiDevice(GalaxyConfig pluginConfigManager)
        {
            _configManager = pluginConfigManager;
        }

        /// <summary>
        /// Initializes the Device.
        /// </summary>
        /// <param name="requiredParameters">List of required parameters.</param>
        /// <param name="optionalParameters">List of optional parameters.</param>
        public override void Init(IEnumerable<SeiParameter> requiredParameters, IEnumerable<SeiParameter> optionalParameters)
        {
            Logger.Info("Initializing Device...");
            
            _parameterManager = new PluginParameters(requiredParameters.ToList(), optionalParameters.ToList());

            if (!_parameterManager.AreParametersValid)
            {
                Logger.Warn("Some plugin parameters are invalid, please check your plugin configuration in Cayuga client. Invalid values have been ignored.");
            }

            Logger.SetLogLevel(_parameterManager.LogLevel);

            Logger.Trace("Initializing Command Socket...");
            _actionCommandSocket = new SIACommandSocket(_parameterManager.Host, _parameterManager.Pin);

            Logger.Trace("Initializing Item Manager...");
            _itemManager = new GalaxyItemManager(_configManager, _parameterManager, _actionCommandSocket, RaiseStateChanged);

            Logger.Trace("Initializing Event Listener...");
            _eventListener = new SIAListenerSocket(RaiseEventOccured, _parameterManager.EventPort, _parameterManager.AllowedHostList);

            _panelVersion = _actionCommandSocket.GetPanelVersion();
            Logger.Info($"Connection to panel successful. Panel version: {_panelVersion}");

            _itemManager.InitItemStates();

            Task.Factory.StartNew(() => { _eventListener.StartListening(); });

            _itemManager.StartPeriodicStateRefresh(_parameterManager.StateRefreshIntervalSecs * 1000);

            Logger.Info("Device initialization done.");
        }

        /// <summary>
        /// Callback when an event occured.
        /// </summary>
        /// <param name="evt">The SIA event which occured.</param>
        private void RaiseEventOccured(SIAEvent evt)
        {
            Logger.Debug($"Event occured: {evt.ToString()}");

            string itemTypeThirdPartyId = null;
            string relevantId = null;

            switch (evt.EventType)
            {
                case SIAEventType.UNKNOWN:
                    Logger.Warn($"Unknown event type for event: {evt.ToString()}");
                    Logger.Warn("Event ignored.");
                    return;
                case SIAEventType.EVENT:
                    itemTypeThirdPartyId = ItemTypeNames.ITEM_TYPE_TID_EVENT;
                    relevantId = itemTypeThirdPartyId;
                    break;
                case SIAEventType.MODULE:
                    itemTypeThirdPartyId = ItemTypeNames.ITEM_TYPE_TID_MOD;
                    relevantId = evt.PeripheryId;
                    break;
                case SIAEventType.USER:
                    itemTypeThirdPartyId = ItemTypeNames.ITEM_TYPE_TID_USER;
                    relevantId = evt.UserId;
                    break;
                case SIAEventType.ZONE:
                    itemTypeThirdPartyId = ItemTypeNames.ITEM_TYPE_TID_ZONE;
                    relevantId = evt.ZoneId;
                    break;
                default:
                    Logger.Warn($"Unsupported event type for event: {evt.ToString()}");
                    Logger.Warn("Event ignored.");
                    return;
            }
            HandleEvent(itemTypeThirdPartyId, relevantId, evt);

            // Separate handling if there's a group ID
            if (evt.HasGroupId)
            {
                Logger.Debug("Handling Group Event");
                HandleEvent(ItemTypeNames.ITEM_TYPE_TID_GROUP, evt.Group, evt);
            }

            Logger.Debug("Event handling complete.");
        }

        /// <summary>
        /// Handles an event received from the Socket Listener.
        /// </summary>
        /// <param name="itemTypeThirdPartyId">The third party id of the item type.</param>
        /// <param name="itemThirtPartyId">The third party id of the item.</param>
        /// <param name="evt">The received event.</param>
        private void HandleEvent(string itemTypeThirdPartyId, string itemThirtPartyId, SIAEvent evt)
        {
            if (itemTypeThirdPartyId == null)
            {
                Logger.Error($"Unsupported SIA Event Type: {evt.EventType}");
                return;
            }

            var itemType = _configManager.GetActiveItemTypes().FirstOrDefault(x => x.ThirdPartyId.Equals(itemTypeThirdPartyId));

            if (itemType == null || !itemType.Active)
            {
                Logger.Debug($"Item type '{itemTypeThirdPartyId}' is not active, event ignored.");
                return;
            }

            GalaxySeiItem item;
            if (_parameterManager.GetSeiItemIdList(itemTypeThirdPartyId).Count > 0)
            {
                item = _itemManager.FindByThirdPartyId(itemThirtPartyId);
            }
            else
            {
                Logger.Debug($"No specific items configured, using master item.");
                item = _itemManager.FindByThirdPartyId(itemTypeThirdPartyId);
            }

            if (null == item)
            {
                Logger.Warn($"Event skipped, {itemTypeThirdPartyId} {itemThirtPartyId} item not exposed.");
                return;
            }

            List<SeiEventType> eventList = GalaxyItemManager.GetActiveEventTypes(evt, itemType);
            RaiseEvents(item.Id, eventList);
        }

        /// <summary>
        /// Raises the given events for the given Item.
        /// </summary>
        /// <param name="itemId">The Cayuga item ID to raise the events for.</param>
        /// <param name="eventList"></param>
        private void RaiseEvents(int itemId, List<SeiEventType> eventList)
        {
            foreach (var et in eventList)
            {
                if (int.TryParse(et.Id, out int eventId))
                {
                    Logger.Debug($"Found active event: {et.Name}");

                    // Fire all events
                    EventOccuredEventArgs evtArgs = new EventOccuredEventArgs()
                    {
                        ItemId = itemId,
                        Description = et.Name,
                        Time = DateTime.Now,
                        EventId = eventId
                    };

                    GalaxySeiItem item = _itemManager.FindByCayugaId(evtArgs.ItemId);
                    Logger.Info($"Event occured on item '{item.Name}': {evtArgs.Description}.");

                    RaiseEventOccured(evtArgs);
                }
                else
                {
                    throw new SeiException($"Unable to parse ID of event: {et.Id}");
                }
            }
        }

        /// <summary>
        /// Gets the available items which can have children.
        /// </summary>
        /// <returns>Available items.</returns>
        public override IEnumerable<SeiItem> GetItems()
        {
            Logger.Trace("GetItems() called.");
            foreach (var it in _itemManager.SeiItems)
            {
                Logger.Trace($"Exposing item: {it.Name}, id {it.Id}, thirdPartyId {it.ThirdPartyId}, TypeId {it.TypeId}");
            }
            return _itemManager.SeiItems;
        }

        /// <summary>
        /// Executes the action specified by the given id. The plugin decides whether the action is executable at the current state or not. If not an exeception is thrown.
        /// </summary>
        /// <param name="itemId">The id of the item whose action is to be executed.</param>
        /// <param name="actionTypeId">The id of the action that should get executed.</param>
        public override void ExecuteAction(int itemId, int actionTypeId)
        {
            GalaxySeiItem it = _itemManager.FindByCayugaId(itemId);

            if (it == null)
            {
                throw new SeiException($"Unknown Item ID: {itemId}");
            }

            Logger.Debug($"Action execution for item ({it.Id}, {it.ThirdPartyId}) [{it.Name}] ({_configManager.GetItemTypeThirdPartyId(it.TypeId)})");

            GalaxyAction action = _configManager.FindActionById(actionTypeId);

            Logger.Info($"Executing action '{action.Description}' for item '{it.Name}'");

            lock (_actionCommandSocket)
            {

                bool result = _actionCommandSocket.ExecuteAction(action, it.ThirdPartyId);

                if (result)
                {
                    Logger.Info("Action successful.");
                }
                else
                {
                    Logger.Error("Action failed.");
                }
            }
        }

        /// <summary>
        /// The id of the item whose state should be returned.
        /// </summary>
        /// <param name="itemId">id of the item.</param>
        /// <returns>State of the item.</returns>
        public override int GetItemState(int itemId)
        {
            GalaxySeiItem it = _itemManager.FindByCayugaId(itemId);
            Logger.Debug($"State requested, item is ({it.Id}, {it.ThirdPartyId}) [{it.Name}] ({_configManager.GetItemTypeThirdPartyId(it.TypeId)})");

            if (it == null)
            {
                throw new SeiException($"Unknown Item ID: {itemId}");
            }

            return it.CurrentState;
        }

        /// <summary>
        /// Disposes the object.
        /// </summary>
        public override void Dispose()
        {
            Logger.Trace("SeiDevice.Dispose() called.");
            base.Dispose();
            _itemManager.Dispose();
            _eventListener.Dispose();
        }
    }
}

﻿using SeeTec.EventInterface;
using SeeTec.Helpers.Logging;
using SeeTec.HoneywellGalaxy.Configuration.SeiPlugin;
using SeeTec.HoneywellGalaxy.SEIPlugin.Parameters;
using SeeTec.SEIPluginConfiguration;
using System;
using System.Collections.Generic;
using System.IO;

namespace SeeTec.HoneywellGalaxy.SEIPlugin
{
    /// <summary>
    /// Honeywell galaxy SEI Plugin.
    /// </summary>
    public class GalaxySeiPlugin : SeiPlugin
    {
#if DEBUG
        /// <summary>
        /// Amount of seconds to wait on plugin startup for the debugger to attach
        /// to the process. 
        /// </summary>
        private const int DEBUG_STARTUP_WAIT_SECS = 0;
#endif
        /// <summary>
        /// Expected path to the plugin working directory.
        /// </summary>
        private const string RELATIVE_PLUGIN_DIRECTORY = @"\EventPlugins\SeeTec.HoneywellGalaxy.SEIPlugin\";

        /// <summary>
        /// Expected name of the configuration file.
        /// </summary>
        private const string CONFIG_FILE_NAME = @"SeeTec.HoneywellGalaxy.SEIPlugin.xml";

        /// <summary>
        /// Instance of the configuration manager for this plugin.
        /// </summary>
        private readonly GalaxyConfig _configManager;

        /// <summary>
        /// Gets the manufacturer of the devices this plugin can connect to.
        /// </summary>
        public override string Manufacturer => "Honeywell Galaxy";

        /// <summary>
        /// Gets the type of the devices this plugin can connect to. 
        /// </summary>
        public override string PluginType => "Galaxy Dimension";

        /// <summary>
        /// Gets the version of this plugin. 
        /// </summary>
        public override string Version => System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

        public GalaxySeiPlugin()
        {
            Logger.SetLogLevel(Logger.LogLevel.INF);
            Logger.SetMessagePrefix("<HoneywellGalaxy.SEIPlugin>");

            Logger.Info($"Honeywell Galaxy SEI Plugin v{Version} starting up.");

#if DEBUG
            // In DEBUG build, wait to give Debugger a chance to attach
            Logger.Warn("================================================================");
            Logger.Warn("============= DEBUG BUILD - NOT FOR PRODUCTION USE =============");
            Logger.Warn("================================================================");
            Logger.SetLogLevel(Logger.LogLevel.DBG);
            if (DEBUG_STARTUP_WAIT_SECS > 0)
            {
                Logger.Debug($"Waiting {DEBUG_STARTUP_WAIT_SECS} sec for Debugger to attach...");
                System.Threading.Thread.Sleep(DEBUG_STARTUP_WAIT_SECS * 1000);
                Logger.Debug($"Continue execution.");
            }
#if TRACE
            Logger.SetLogLevel(Logger.LogLevel.TRC);
#endif
#endif


            string configFilePath = Directory.GetCurrentDirectory() + RELATIVE_PLUGIN_DIRECTORY + CONFIG_FILE_NAME;
            
            try
            {
                Logger.Info($"Loading configuration file: {configFilePath}");
                var config = SEIPluginConfigurationSerializer.ReadPluginConfiguration(configFilePath);
                Logger.Info($"Configuration loaded successfully. Initializing configuration.");
                _configManager = new GalaxyConfig(config);
                Logger.Info($"Configuration complete.");
            }
            catch (Exception e)
            {
                Logger.Error($"Error loading the plugin configuration: {e.Message}");
                Logger.Error($"Plugin initialization aborted.");
                throw new SeiException($"Error loading the plugin configuration: {e.Message}");
            }

            Logger.Info($"Plugin initialization complete.");
        }

        /// <summary>
        /// Creates a new instance of the Device class.
        /// </summary>
        /// <returns>New instance of the device.</returns>
        public override SeiDevice CreateDevice()
        {
            return new GalaxySeiDevice(_configManager);
        }

        /// <summary>
        /// Gets a list containing the information about the available action types. 
        /// </summary>
        /// <returns>List containing the information about the available action types</returns>
        public override IEnumerable<SeiTypeInfo> GetActionTypeInfos()
        {
            Logger.Trace("Action Type Infos requested.");
            var typeInfos = (List<SeiTypeInfo>)_configManager.GetActionTypeInfos();



            Logger.Debug($"Announcing {typeInfos.Count} Action Type Infos.");
            return typeInfos;
        }

        /// <summary>
        /// Gets a list containing the information about the available event types. 
        /// </summary>
        /// <returns>List containing the information about the available event types. </returns>
        public override IEnumerable<SeiTypeInfo> GetEventTypeInfos()
        {
            Logger.Trace("Event Type Info requested.");
            var typeInfos = (List<SeiTypeInfo>)_configManager.GetEventTypeInfos();
            Logger.Debug($"Announcing {typeInfos.Count} Event Type Infos.");
            return typeInfos;
        }

        /// <summary>
        /// Gets information about the available item types. 
        /// </summary>
        /// <returns>List of the available item types. </returns>
        public override IEnumerable<SeiItemTypeInfo> GetItemTypeInfos()
        {
            Logger.Trace("Item Type Info requested.");
            var typeInfos = (List<SeiItemTypeInfo>)_configManager.GetItemTypeInfos();
            Logger.Debug($"Announcing {typeInfos.Count} Item Type Infos.");
            return typeInfos;
        }

        /// <summary>
        /// Gets a list containing the information about the available state types.
        /// </summary>
        /// <returns>List containing the information about the available state types.</returns>
        public override IEnumerable<SeiTypeInfo> GetStateTypeInfos()
        {
            Logger.Trace("State Type Info requested.");
            var typeInfos = (List<SeiTypeInfo>)_configManager.GetStateTypeInfos();
            Logger.Debug($"Announcing {typeInfos.Count} State Type Infos.");
            return typeInfos;
        }

        /// <summary>
        /// Gets a list containing the information about the available optional parameters. 
        /// </summary>
        /// <returns>List containing the information about the available optional parameters.</returns>
        public override IEnumerable<SeiParameter> GetOptionalParameters()
        {
            return PluginParameters.AvailableOptionalParameters;
        }

        /// <summary>
        /// Gets a list containing the information about the available required parameters. 
        /// </summary>
        /// <returns>List containing the information about the available required parameters.</returns>
        public override IEnumerable<SeiParameter> GetRequiredParameters()
        {
            return PluginParameters.AvailableRequiredParameters;
        }
    }
}

﻿using SeeTec.EventInterface;
using SeeTec.Helpers.Logging;
using SeeTec.HoneywellGalaxy.Configuration.ICT;
using SeeTec.HoneywellGalaxy.Configuration.SeiPlugin;
using SeeTec.HoneywellGalaxy.SEIPlugin.Helper;
using SeeTec.HoneywellGalaxy.SEIPlugin.Parameters;
using SeeTec.HoneywellGalaxy.SIAConnector;
using SeeTec.HoneywellGalaxy.SIAConnector.SIA;
using SeeTec.SEIPluginConfiguration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Timers;

namespace SeeTec.HoneywellGalaxy.SEIPlugin.Items
{
    /// <summary>
    /// Manages SEI Items for a Galaxy SEI Device.
    /// </summary>
    internal class GalaxyItemManager : IDisposable
    {
        /// <summary>
        /// Mapping of Cayuga IDs to SEI Items for convenient lookup.
        /// </summary>
        private Dictionary<int, GalaxySeiItem> _itemDictionary = new Dictionary<int, GalaxySeiItem>();

        /// <summary>
        /// Mapping of third party IDs to Cayuga IDs for convenient lookup.
        /// </summary>
        private Dictionary<string, int> _thirdPartyIdDictionary = new Dictionary<string, int>();
        private GalaxyConfig _configManager;
        private Timer _stateRefreshTimer;
        private readonly SIACommandSocket _commandSocket;

        /// <summary>
        /// SEI Items managed by this item manager.
        /// </summary>
        public IEnumerable<GalaxySeiItem> SeiItems => _itemDictionary.Values;

        private Action<StateChangedEventArgs> _stateChangedCallback { get; set; }

        private PluginParameters _parameterManager;

        /// <summary>
        /// Creates a new instance of the item manager.
        /// </summary>
        /// <param name="configManager">The configuration manager to use.</param>
        /// <param name="parameterManager">The parameter manager to use.</param>
        public GalaxyItemManager(GalaxyConfig configManager, PluginParameters parameterManager, SIACommandSocket commandSocket, Action<StateChangedEventArgs> stateChangedCallback)
        {
            _configManager = configManager;
            _commandSocket = commandSocket;
            _stateChangedCallback = stateChangedCallback;
            _parameterManager = parameterManager;
            CreateAllSeiItems(_configManager, _parameterManager);
        }

        /// <summary>
        /// Adds a new Item to the manager.
        /// </summary>
        /// <param name="item">The item to add.</param>
        private void AddItem(GalaxySeiItem item)
        {
            _itemDictionary.Add(item.Id, item);
            _thirdPartyIdDictionary.Add(item.ThirdPartyId, item.Id);
        }

        /// <summary>
        /// Finds an item by its third party ID.
        /// </summary>
        /// <param name="thirdPartyId">third party ID of the item.</param>
        /// <returns>The item.</returns>
        public GalaxySeiItem FindByThirdPartyId(string thirdPartyId)
        {
            try
            {
                if (!_thirdPartyIdDictionary.ContainsKey(thirdPartyId))
                {
                    Logger.Debug($"Unknown (inactive) third party item id: {thirdPartyId}.");
                    return null;
                }

                int id = _thirdPartyIdDictionary[thirdPartyId];

                if (!_itemDictionary.ContainsKey(id))
                {
                    Logger.Debug($"Unknown (inactive) item id: {id}.");
                    return null;
                }

                return _itemDictionary[id];
            }
            catch
            {
                throw new ArgumentException($"Invalid third party ID for Item: {thirdPartyId}");
            }
        }

        /// <summary>
        /// Finds an item by its Cayuga ID.
        /// </summary>
        /// <param name="itemId">The Cayuga ID.</param>
        /// <returns>The item.</returns>
        internal GalaxySeiItem FindByCayugaId(int itemId)
        {
            try
            {
                return _itemDictionary[itemId];
            }
            catch
            {
                throw new ArgumentException($"Invalid Cayuga ID for Item: {itemId}");
            }
        }

        /// <summary>
        /// Creates SEI Items for a given item type.
        /// </summary>
        /// <param name="configManager">Configuration manager to get item type configuration from.</param>
        /// <param name="parameterManager">Parameter manager to get item type parameters from</param>
        /// <param name="item">The item type to get the items for.</param>
        private void CreateSeiItems(GalaxyConfig configManager, PluginParameters parameterManager, SeiItemType item)
        {
            StringBuilder sb = new StringBuilder();

            string typeThirdPartyId = item.ThirdPartyId;
            string displayName = item.Name;

            List<int> idList = parameterManager.GetSeiItemIdList(typeThirdPartyId);

            if (idList.Count > 0)
            {
                foreach (var id in idList)
                {
                    if (int.TryParse(item.Id, out int itemId))
                    {
                        int padding = GetPadding(typeThirdPartyId);
                        string itemName = $"{displayName} {$"{id}".PadLeft(padding, '0')}";
                        GalaxySeiItem seiItem = new GalaxySeiItem($"{typeThirdPartyId}-{id}".GetHashCode(), itemName, configManager.GetItemTypeId(typeThirdPartyId), $"{id}".PadLeft(padding, '0'), _configManager.GetActionsForItemType(typeThirdPartyId));
                        AddItem(seiItem);
                        sb.Append($"'{itemName}' ");
                    }
                    else
                    {
                        Logger.Warn($"Cannot parse item ID '{item.Id}' of item {item.Name} - item skipped.");
                    }
                }
                Logger.Info($"Exposing {displayName}s: {sb.ToString()}");
            }
            else
            {
                switch (typeThirdPartyId)
                {
                    case ItemTypeNames.ITEM_TYPE_TID_OUTPUT:
                        sb.Append($"No {displayName} IDs specified in parameters, item type '{displayName}' hidden.");
                        break;
                    case ItemTypeNames.ITEM_TYPE_TID_GROUP:
                    case ItemTypeNames.ITEM_TYPE_TID_MOD:
                    case ItemTypeNames.ITEM_TYPE_TID_USER:
                    case ItemTypeNames.ITEM_TYPE_TID_EVENT:
                    case ItemTypeNames.ITEM_TYPE_TID_ZONE:
                        GalaxySeiItem singleItem = new GalaxySeiItem(typeThirdPartyId.GetHashCode(), $"{displayName} (master)", configManager.GetItemTypeId(typeThirdPartyId), typeThirdPartyId, _configManager.GetActionsForItemType(typeThirdPartyId));
                        AddItem(singleItem);
                        sb.Append($"'{singleItem.Name}' ");
                        Logger.Info($"No {displayName} IDs specified in parameters, exposing generic {displayName}(s): {sb.ToString()}");
                        break;
                    default:
                        Logger.Warn($"Unknown item type '{typeThirdPartyId}', no SeiItem created.");
                        break;
                }
            }
        }

        /// <summary>
        /// Gets the padding width for a third party id.
        /// </summary>
        /// <param name="typeThirdPartyId">The third party id.</param>
        /// <returns>The padding width.</returns>
        private static int GetPadding(string typeThirdPartyId)
        {
            int padding = 0;
            switch (typeThirdPartyId)
            {
                case ItemTypeNames.ITEM_TYPE_TID_OUTPUT:
                    padding = 3;
                    break;
                case ItemTypeNames.ITEM_TYPE_TID_ZONE:
                    padding = 4;
                    break;
                case ItemTypeNames.ITEM_TYPE_TID_GROUP:
                    padding = 2;
                    break;
                default:
                case ItemTypeNames.ITEM_TYPE_TID_MOD:
                case ItemTypeNames.ITEM_TYPE_TID_USER:
                case ItemTypeNames.ITEM_TYPE_TID_EVENT:
                    padding = 0;
                    break;
            }

            return padding;
        }

        /// <summary>
        /// Creates all SEI Items for the given configuration and parameters.
        /// </summary>
        /// <param name="configManager">The configuration manager.</param>
        /// <param name="parameterManager">The parameter manager.</param>
        private void CreateAllSeiItems(GalaxyConfig configManager, PluginParameters parameterManager)
        {
            IEnumerable<SeiItemType> activeItemTypes = configManager.GetActiveItemTypes();

            CheckParametersActiveItemTypes(parameterManager, activeItemTypes);

            foreach (var activeItem in activeItemTypes)
            {
                CreateSeiItems(configManager, parameterManager, activeItem);
            }
        }

        /// <summary>
        /// Checks the parameter manager versus active item types and logs warnings if Item IDs are defined
        /// in optional parameters, but the item type is not active.
        /// </summary>
        /// <param name="parameterManager">The parameters to check.</param>
        /// <param name="activeItemTypes">The active item types to check.</param>
        private static void CheckParametersActiveItemTypes(PluginParameters parameterManager, IEnumerable<SeiItemType> activeItemTypes)
        {
            // Check parameters versus active Items and log warnings
            if (parameterManager.HasGroupIdList)
            {
                if (activeItemTypes.FirstOrDefault(ai => ai.ThirdPartyId.Equals(ItemTypeNames.ITEM_TYPE_TID_GROUP)) == null)
                {
                    Logger.Warn("Group IDs specified in optional parameters, but Item Type not active in configuration. The Items will not be visible.");
                }
            }

            if (parameterManager.HasModuleIdList)
            {
                if (activeItemTypes.FirstOrDefault(ai => ai.ThirdPartyId.Equals(ItemTypeNames.ITEM_TYPE_TID_MOD)) == null)
                {
                    Logger.Warn("Module IDs specified in optional parameters, but Item Type not active in configuration. The Items will not be visible.");
                }
            }

            if (parameterManager.HasOutputIdList)
            {
                if (activeItemTypes.FirstOrDefault(ai => ai.ThirdPartyId.Equals(ItemTypeNames.ITEM_TYPE_TID_OUTPUT)) == null)
                {
                    Logger.Warn("Output IDs specified in optional parameters, but Item Type not active in configuration. The Items will not be visible.");
                }
            }

            if (parameterManager.HasUserIdList)
            {
                if (activeItemTypes.FirstOrDefault(ai => ai.ThirdPartyId.Equals(ItemTypeNames.ITEM_TYPE_TID_USER)) == null)
                {
                    Logger.Warn("User IDs specified in optional parameters, but Item Type not active in configuration. The Items will not be visible.");
                }
            }

            if (parameterManager.HasZoneIdList)
            {
                if (activeItemTypes.FirstOrDefault(ai => ai.ThirdPartyId.Equals(ItemTypeNames.ITEM_TYPE_TID_ZONE)) == null)
                {
                    Logger.Warn("Zone IDs specified in optional parameters, but Item Type not active in configuration. The Items will not be visible.");
                }
            }
        }

        /// <summary>
        /// Returns the highest priority state from the given list of states, according to plugin configuration.
        /// </summary>
        /// <param name="stateTIDList">The state list.</param>
        /// <param name="itemTypeTID">The item type third party id to check agains.</param>
        /// <returns>The highest priority state cayuga id.</returns>
        private int GetHighestPriorityStateId(List<string> stateTIDList, string itemTypeTID)
        {
            SeiItemType itemType = _configManager.GetActiveItemTypes().FirstOrDefault(x => x.ThirdPartyId.Equals(itemTypeTID));

            if (null == itemType)
            {
                Logger.Warn($"Item type inactive: {itemTypeTID}");
                return GalaxyConfig.UNKNOWN_STATE_TYPE_ID;
            }

            var unknownStateType = new SeiStateType()
            {
                Active = true,
                Id = $"{GalaxyConfig.UNKNOWN_STATE_TYPE_ID}",
                Name = GalaxyConfig.UNKNOWN_STATE_TYPE_NAME,
                Priority = int.MaxValue,
                ThirdPartyId = "UNKNOWN"
            };

            List<SeiStateType> stateTypeList = new List<SeiStateType>() { unknownStateType };

            foreach (var r in stateTIDList)
            {
                var stateType = itemType.StateTypes.FirstOrDefault(s => s.ThirdPartyId.Equals(r));

                if (stateType == null)
                {
                    Logger.Warn($"Unknown state type '{r}'");
                }
                else
                {
                    Logger.Trace($"State Type for State ID '{r}': ({stateType.Id}, {stateType.ThirdPartyId}) {stateType.Name} - Priority {stateType.Priority}");
                    stateTypeList.Add(stateType);
                }
            }

            stateTypeList = stateTypeList.OrderBy(x => x.Priority).ToList();

            if (int.TryParse(stateTypeList.First().Id, out int stateId))
            {
                return stateId;
            }
            else
            {
                throw new SeiException($"Cannot parse ID '{stateTypeList.First().Id}' of state type to integer.");
            }
        }

        /// <summary>
        /// Gets a list of all active event types for the given SIA event and item type hierarchical event type assignment.
        /// </summary>
        /// <param name="evt">The SIA Event to derive event types from.</param>
        /// <param name="itemType">The item type.</param>
        /// <returns>List of event types.</returns>
        internal static List<SeiEventType> GetActiveEventTypes(SIAEvent evt, SeiItemType itemType)
        {
            List<SeiEventType> eventList = new List<SeiEventType>();

            string logEventString = $"{evt.AsciiBlock.LogEvent}{evt.AsciiBlock.EventState.Trim()}";


            // Handle custom events
            IEnumerable<SeiEventType> customEventList = itemType.AssignedEventTypes.Where(et => et.Custom == true);
            Logger.Trace($"Looking for custom events.");
            foreach (var ce in customEventList)
            {
                var neParentsList = ce.Children.Where(ne => ne.ThirdPartyId.Equals(evt.EventCode));
                var neDetailedList = ce.Children.Where(et => et.ThirdPartyId.Contains(logEventString) && et.Properties["parentEventCode"] == evt.EventCode);

                if (neParentsList.Count() > 0 || neDetailedList.Count() > 0)
                {
                    Logger.Trace($"Found custom event: {ce.Name}");
                    eventList.Add(ce);
                }
            }

            // Find active parent event, if any
            Logger.Trace($"Looking for category event, event code '{evt.EventCode}'");
            var parentEvent = itemType.AssignedEventTypes.FirstOrDefault(et => et.Custom == false && et.ThirdPartyId.Equals(evt.EventCode));
            if (null != parentEvent)
            {
                Logger.Trace($"Found parent event: {parentEvent.Name}");
                eventList.Add(parentEvent);
            }
            else
            {
                Logger.Debug($"Event occured, but event type not active.");
            }

            // Find active detailed event, if any
            Logger.Trace($"Looking for log event '{logEventString}', event code '{evt.EventCode}'");
            var logEvent = itemType.AssignedEventTypes.FirstOrDefault(et => et.ThirdPartyId.Contains(logEventString) && et.Properties["parentEventCode"] == evt.EventCode);
            if (null != logEvent)
            {
                Logger.Trace($"Found log event: {logEvent.Name}");
                eventList.Add(logEvent);
            }
            else
            {
                Logger.Debug($"Event occured, but event type not active.");
            }

            return eventList;
        }

        /// <summary>
        /// Starts the periodic state refresh in a new thread.
        /// </summary>
        /// <param name="refreshIntervallMs">Refresh intervall in ms.</param>
        public void StartPeriodicStateRefresh(int refreshIntervallMs)
        {
            _stateRefreshTimer = new Timer(refreshIntervallMs);

            _stateRefreshTimer.Elapsed += StatesRefreshElapsed;
            _stateRefreshTimer.AutoReset = false;
            _stateRefreshTimer.Start();
        }

        /// <summary>
        /// Worker method for periodic state refresh thread.
        /// </summary>
        /// <param name="refreshIntervallMs">Refresh intervall in ms</param>
        private void StatesRefreshElapsed(Object source, ElapsedEventArgs e)
        {
            _stateRefreshTimer.Stop();

            Stopwatch sw = new Stopwatch();
            sw.Start();

            lock (_commandSocket)
            {
                RefreshItemStates(true);
            }

            sw.Stop();
            Logger.Debug($"Item states refreshed, took {sw.ElapsedMilliseconds} ms.");

            _stateRefreshTimer.Start();
        }


        /// <summary>
        /// Refreshes the states of all supported item types.
        /// </summary>
        /// <param name="raiseEvent">Flag whether state change occured event shall be raised when state changes are detected.</param>
        private void RefreshItemStates(bool raiseEvent)
        {
            Logger.Trace("Refreshing item states...");

            RefreshZoneStates(raiseEvent);
            RefreshOutputStates(raiseEvent);
            RefreshGroupStates(raiseEvent);
        }

        /// <summary>
        /// Refreshes output states.
        /// </summary>
        /// <param name="raiseEvent">Flag whether state change occured event shall be raised when state changes are detected.</param>
        private void RefreshOutputStates(bool raiseEvent)
        {
            int outputTypeId = _configManager.GetItemTypeId(ItemTypeNames.ITEM_TYPE_TID_OUTPUT);
            IEnumerable<GalaxySeiItem> outputList = _itemDictionary.Values.Where(i => i.TypeId.Equals(outputTypeId));

            if (outputList.Count() < 1)
            {
                Logger.Debug("No active outputs, state refresh skipped.");
                return;
            }

            if (outputList.Count() == 1 && outputList.First().ThirdPartyId.Equals(ItemTypeNames.ITEM_TYPE_TID_OUTPUT))
            {
                Logger.Debug("State query for master group, state unknown.");
                outputList.First().CurrentState = GalaxyConfig.UNKNOWN_STATE_TYPE_ID;
                return;
            }

            SIAMessage request = new SIAMessage("COR1000");
            var reply = _commandSocket.SendMessage(request.RawMessage);

            IEnumerable<byte> outputState = GetStateBytes(reply, 32);

            foreach (var output in outputList)
            {
                List<string> result = GetOutputState(outputState, output.ThirdPartyId);

                int stateId = GetHighestPriorityStateId(result, ItemTypeNames.ITEM_TYPE_TID_OUTPUT);

                if (stateId != output.CurrentState && raiseEvent)
                {
                    if (_stateChangedCallback != null)
                    {
                        Logger.Info($"State of item '{output.Name}' changed.");

                        StateChangedEventArgs evtArgs = new StateChangedEventArgs()
                        {
                            ItemId = output.Id,
                            NewStateId = stateId,
                            Time = DateTime.Now,
                            Description = "State changed"
                        };

                        _stateChangedCallback(evtArgs);
                    }
                }
                output.CurrentState = stateId;
            }
        }

        /// <summary>
        /// Refreshes zone states.
        /// </summary>
        /// <param name="raiseEvent">Flag whether state change occured event shall be raised when state changes are detected.</param>
        private void RefreshZoneStates(bool raiseEvent)
        {
            int zoneTypeId = _configManager.GetItemTypeId(ItemTypeNames.ITEM_TYPE_TID_ZONE);
            IEnumerable<GalaxySeiItem> zoneList = _itemDictionary.Values.Where(i => i.TypeId.Equals(zoneTypeId));

            if (zoneList.Count() < 1)
            {
                Logger.Debug("No active zones, state refresh skipped.");
                return;
            }

            if (zoneList.Count() == 1 && zoneList.First().ThirdPartyId.Equals(ItemTypeNames.ITEM_TYPE_TID_ZONE))
            {
                Logger.Debug("State query for master zone, state unknown.");
                zoneList.First().CurrentState = GalaxyConfig.UNKNOWN_STATE_TYPE_ID;
                return;
            }

            List<SIAMessage> requestList = new List<SIAMessage>()
            {
                new SIAMessage("XZS1"),
                new SIAMessage("XZS2"),
                new SIAMessage("XZS101"),
                new SIAMessage("XZS102"),
                new SIAMessage("XZS201"),
                new SIAMessage("XZS202"),
                new SIAMessage("XZS301"),
                new SIAMessage("XZS302"),
                new SIAMessage("XZS401"),
                new SIAMessage("XZS402"),
                new SIAMessage("XZS501"),
                new SIAMessage("XZS502"),
                new SIAMessage("XZS601"),
                new SIAMessage("XZS602"),
                new SIAMessage("XZS701"),
                new SIAMessage("XZS702")
            };

            List<SIAMessage> replyList = _commandSocket.SendAllMessages(requestList);

            if (replyList.Count != requestList.Count)
            {
                throw new SeiException("Sending zone state request messages failed.");
            }

            bool virtualRIO = _parameterManager.VirtualRIO;

            IEnumerable<byte> xzs1 = ExtractZoneBytes(GetStateBytes(replyList[0]), virtualRIO, out byte xzs1VirtualZones);
            IEnumerable<byte> xzs2 = GetStateBytes(replyList[1], 32);
            IEnumerable<byte> xzs = xzs1.Concat(xzs2);

            IEnumerable<byte> xzs101 = ExtractZoneBytes(GetStateBytes(replyList[2]), virtualRIO, out byte xzs10VirtualZones);
            IEnumerable<byte> xzs102 = GetStateBytes(replyList[3], 32);
            IEnumerable<byte> xzs10 = xzs101.Concat(xzs102);

            IEnumerable<byte> xzs201 = ExtractZoneBytes(GetStateBytes(replyList[4]), virtualRIO, out byte xzs20VirtualZones);
            IEnumerable<byte> xzs202 = GetStateBytes(replyList[5], 32);
            IEnumerable<byte> xzs20 = xzs201.Concat(xzs202);

            IEnumerable<byte> xzs301 = ExtractZoneBytes(GetStateBytes(replyList[6]), virtualRIO, out byte xzs30VirtualZones);
            IEnumerable<byte> xzs302 = GetStateBytes(replyList[7], 32);
            IEnumerable<byte> xzs30 = xzs301.Concat(xzs302);

            IEnumerable<byte> xzs401 = ExtractZoneBytes(GetStateBytes(replyList[8]), virtualRIO, out byte xzs40VirtualZones);
            IEnumerable<byte> xzs402 = GetStateBytes(replyList[9], 32);
            IEnumerable<byte> xzs40 = xzs401.Concat(xzs402);

            IEnumerable<byte> xzs501 = ExtractZoneBytes(GetStateBytes(replyList[10]), virtualRIO, out byte xzs50VirtualZones);
            IEnumerable<byte> xzs502 = GetStateBytes(replyList[11], 32);
            IEnumerable<byte> xzs50 = xzs501.Concat(xzs502);

            IEnumerable<byte> xzs601 = ExtractZoneBytes(GetStateBytes(replyList[12]), virtualRIO, out byte xzs60VirtualZones);
            IEnumerable<byte> xzs602 = GetStateBytes(replyList[13], 32);
            IEnumerable<byte> xzs60 = xzs601.Concat(xzs602);

            IEnumerable<byte> xzs701 = ExtractZoneBytes(GetStateBytes(replyList[14]), virtualRIO, out byte xzs70VirtualZones);
            IEnumerable<byte> xzs702 = GetStateBytes(replyList[15], 32);
            IEnumerable<byte> xzs70 = xzs701.Concat(xzs702);

            foreach (var zone in zoneList)
            {
                List<string> result = new List<string>();

                var adress = GalaxyHelper.ConvertToThreeDigitIntegerZoneAdress(zone.ThirdPartyId);

                bool bit1;
                bool bit10;
                bool bit20;
                bool bit30;
                bool bit40;
                bool bit50;
                bool bit60;
                bool bit70;

                if (adress >= -119 && adress <= -112)
                {
                    Logger.Trace($"Handling virtual zone: {zone.Name}");

                    // Virtual zone adresses 0011 through 0018 - handle 
                    if (_parameterManager.VirtualRIO)
                    {
                        int bitIndex = ((adress + 127) % 8);

                        bit1 = xzs1VirtualZones.GetBit(bitIndex);
                        bit10 = xzs10VirtualZones.GetBit(bitIndex); // Alarm
                        bit20 = xzs20VirtualZones.GetBit(bitIndex); // Open
                        bit30 = xzs30VirtualZones.GetBit(bitIndex); // O/C or S/C
                        bit40 = xzs40VirtualZones.GetBit(bitIndex); // Low or High
                        bit50 = xzs50VirtualZones.GetBit(bitIndex); // Ommitted
                        bit60 = xzs60VirtualZones.GetBit(bitIndex); // Masked
                        bit70 = xzs70VirtualZones.GetBit(bitIndex); // Fault
                    }
                    else
                    {
                        Logger.Error($"Invalid zone adress: Virtual zone adressed, but virtual RIO deactivated. Zone state skipped: {zone.ThirdPartyId}");
                        continue;
                    }
                }
                else
                {
                    int byteIndex = ((adress - 1) / 8);
                    int bitIndex = ((adress - 1) % 8);

                    Logger.Trace($"Converted Zone adress: {zone.ThirdPartyId} -> {adress}, byte index {byteIndex}, bit index {bitIndex}");

                    if (byteIndex > xzs.Count() - 1)
                    {
                        Logger.Error($"Invalid zone adress, zone state skipped: {zone.ThirdPartyId}");
                        continue;
                    }

                    bit1 = xzs.ElementAt(byteIndex).GetBit(bitIndex);
                    bit10 = xzs10.ElementAt(byteIndex).GetBit(bitIndex); // Alarm
                    bit20 = xzs20.ElementAt(byteIndex).GetBit(bitIndex); // Open
                    bit30 = xzs30.ElementAt(byteIndex).GetBit(bitIndex); // O/C or S/C
                    bit40 = xzs40.ElementAt(byteIndex).GetBit(bitIndex); // Low or High
                    bit50 = xzs50.ElementAt(byteIndex).GetBit(bitIndex); // Ommitted
                    bit60 = xzs60.ElementAt(byteIndex).GetBit(bitIndex); // Masked
                    bit70 = xzs70.ElementAt(byteIndex).GetBit(bitIndex); // Fault
                }

                if (bit1)
                {
                    if (bit20)
                    {
                        result.Add(GalaxyConfig.ZONE_STATE_OPEN);
                    }

                    if (bit30)
                    {
                        result.Add(GalaxyConfig.ZONE_STATE_OCSC);
                    }

                    if (bit60)
                    {
                        result.Add(GalaxyConfig.ZONE_STATE_MASKED);
                    }

                    if (bit70)
                    {
                        result.Add(GalaxyConfig.ZONE_STATE_FAULT);
                    }
                }
                else
                {
                    if (bit40)
                    {
                        result.Add(GalaxyConfig.ZONE_STATE_LOWHIGH);
                    }
                    else
                    {
                        result.Add(GalaxyConfig.ZONE_STATE_CLOSED);
                    }
                }

                // Independent bits
                // Alarm
                if (bit10)
                {
                    result.Add(GalaxyConfig.ZONE_STATE_ALARM);
                }
                else
                {
                    result.Add(GalaxyConfig.ZONE_STATE_NOALARM);
                }

                // Omit
                if (bit50)
                {
                    result.Add(GalaxyConfig.ZONE_STATE_OMIT);
                }
                else
                {
                    result.Add(GalaxyConfig.ZONE_STATE_NOTOMIT);
                }

                foreach (var r in result)
                {
                    Logger.Trace($"{zone.Name} has state {r}");
                }

                int stateId = GetHighestPriorityStateId(result, ItemTypeNames.ITEM_TYPE_TID_ZONE);

                if (stateId != zone.CurrentState && raiseEvent)
                {
                    if (_stateChangedCallback != null)
                    {
                        Logger.Info($"State of item '{zone.Name}' changed.");
                        StateChangedEventArgs evtArgs = new StateChangedEventArgs()
                        {
                            ItemId = zone.Id,
                            NewStateId = stateId,
                            Time = DateTime.Now,
                            Description = "State changed"
                        };

                        _stateChangedCallback(evtArgs);
                    }
                }

                zone.CurrentState = stateId;
            }
        }

        internal static IEnumerable<byte> ExtractZoneBytes(IEnumerable<byte> payload, bool virtualRIO, out byte virtualZonesByte)
        {
            // Two versions of the protocol
            int payloadLength = payload.Count();
            Logger.Trace("Extracting zone state payload bytes.");
            if (payloadLength == 32)
            {
                // Old 32 byte protocol (previous to Galaxy 3 series/Galaxy Dimension)
                Logger.Trace("Using old SIA protocol, no virtual RIO line.");
                virtualZonesByte = 0;
                return payload;
            }
            else if (payloadLength == 35)
            {
                // Modified 34 byte protocol
                Logger.Trace("Using new SIA protocol, virtual RIO line support.");

                if (virtualRIO)
                {
                    Logger.Trace("Virtual RIO activated.");
                    virtualZonesByte = payload.ElementAt(1);

                    var r1 = payload.Take(1);
                    var r2 = payload.Skip(3).Take(31).ToList();
                    IEnumerable<byte> result = r1.Concat(r2).ToList();

                    return result;
                }
                else
                {
                    Logger.Trace("Virtual RIO not activated.");
                    virtualZonesByte = 0;

                    var r1 = payload.Take(2);
                    var r2 = payload.Skip(4).Take(30).ToList();

                    IEnumerable<byte> result = r1.Concat(r2).ToList();
                    return result;
                }
            }
            else
            {
                throw new SeiException($"Unexpected number of state bytes: {payloadLength}, expected 32 or 34 bytes.");
            }

        }

        /// <summary>
        /// Refreshes group states.
        /// </summary>
        /// <param name="raiseEvent">Flag whether state change occured event shall be raised when state changes are detected.</param>
        private void RefreshGroupStates(bool raiseEvent)
        {
            int groupTypeId = _configManager.GetItemTypeId(ItemTypeNames.ITEM_TYPE_TID_GROUP);
            IEnumerable<GalaxySeiItem> groupList = _itemDictionary.Values.Where(i => i.TypeId.Equals(groupTypeId));

            if (groupList.Count() < 1)
            {
                Logger.Debug("No active groups, state refresh skipped.");
                return;
            }

            if (groupList.Count() == 1 && groupList.First().ThirdPartyId.Equals(ItemTypeNames.ITEM_TYPE_TID_GROUP))
            {
                Logger.Debug("State query for master group, state unknown.");
                groupList.First().CurrentState = GalaxyConfig.UNKNOWN_STATE_TYPE_ID;
                return;
            }

            List<SIAMessage> requestList = new List<SIAMessage>()
            {
                new SIAMessage("CSA91"),
                new SIAMessage("CSA92")
            };

            List<SIAMessage> replyList = _commandSocket.SendAllMessages(requestList);

            if (replyList.Count != requestList.Count)
            {
                throw new SeiException("Sending group state request messages failed.");
            }

            string csa91State = GetStateString(replyList[0]);
            string csa92State = GetStateString(replyList[1]);

            foreach (var group in groupList)
            {
                Logger.Trace($"Refreshing state for '{group.Name}'");
                List<string> result = new List<string>();

                // CSA91 state of all groups regarding alarm
                result.AddRange(GetGroupState(csa91State, "CSA91", group.ThirdPartyId));

                // CSA92 state of all groups regarding set
                result.AddRange(GetGroupState(csa92State, "CSA92", group.ThirdPartyId));

                int stateId = GetHighestPriorityStateId(result, ItemTypeNames.ITEM_TYPE_TID_GROUP);

                if (stateId != group.CurrentState && raiseEvent)
                {
                    if (_stateChangedCallback != null)
                    {
                        Logger.Info($"State of item '{group.Name}' changed.");

                        StateChangedEventArgs evtArgs = new StateChangedEventArgs()
                        {
                            ItemId = group.Id,
                            NewStateId = stateId,
                            Time = DateTime.Now,
                            Description = "State changed"
                        };

                        _stateChangedCallback(evtArgs);
                    }
                }

                group.CurrentState = stateId;
            }
        }

        /// <summary>
        /// Initializes all item states.
        /// </summary>
        public void InitItemStates()
        {
            Logger.Trace("Initializing item states...");
            RefreshItemStates(false);
        }

        public IEnumerable<string> GetGroupState(string stateString, string command, string thirdPartyId)
        {
            List<string> result = new List<string>();

            if (int.TryParse(thirdPartyId, out int intId))
            {
                int index = intId - 1;

                if (stateString.Length < intId)
                {
                    Logger.Warn($"Cannot determine state for group Id: {thirdPartyId}");
                    return result;
                }
                string stateName = $"{command}-{stateString[index]}";
                Logger.Trace($"Group '{thirdPartyId}' has state: {stateName}");
                result.Add(stateName);
            }

            return result;
        }

        /// <summary>
        /// Gets the string payload of a state request message.
        /// </summary>
        /// <param name="command">The state request command.</param>
        /// <returns>The state string payload.</returns>
        public string GetStateString(SIAMessage reply)
        {
            if (reply != null)
            {
                string[] split = reply.BlockData.Split('*');
                if (split.Length == 2)
                {
                    return split[1];
                }
            }

            throw new SeiException($"Panel replied with invalid state: {reply.BlockData}");
        }

        /// <summary>
        /// Gets the raw byte payload of state request messages.
        /// </summary>
        /// <param name="command">The state request command.</param>
        /// <param name="numBytes">Number of bytes to take from the payload. If not given or smaller 0, all bytes are taken.</param>
        /// <returns>Payload bytes.</returns>
        public IEnumerable<byte> GetStateBytes(SIAMessage reply, int numBytes = -1)
        {
            if (reply != null)
            {
                int splitIndex = reply.BlockData.IndexOf('*') + 2; // 2 --> 1 Byte Header, 1 Byte BlockCode
                int rawBlockLengt = reply.RawMessage.Length - splitIndex - 1;

                if (rawBlockLengt < 1)
                {
                    throw new SeiException($"Panel replied with invalid state: {reply.BlockData}");
                }

                IEnumerable<byte> payload = reply.RawMessage.Skip(splitIndex + 1);

                if (numBytes >= 0)
                {
                    return payload.Take(numBytes);
                }

                return payload.ToList();
            }

            throw new SeiException($"Panel replied with invalid state: {reply.BlockData}");
        }

        /// <summary>
        /// Extracts a specific output state from the output states vector.
        /// </summary>
        /// <param name="outputState">The output states vector.</param>
        /// <param name="thirdPartyId">Third party ID of the Output to extract.</param>
        /// <returns></returns>
        public List<string> GetOutputState(IEnumerable<byte> outputState, string thirdPartyId)
        {
            List<string> result = new List<string>();

            if (int.TryParse(thirdPartyId, out int intId))
            {
                int byteIndex = ((intId - 1) / 8);

                if (byteIndex > outputState.Count() - 1)
                {
                    Logger.Error($"Invalid output Id, output state skipped: {thirdPartyId}");
                    return result;
                }

                int bitIndex = ((intId - 1) % 8);

                if (outputState.Count() * 8 < intId)
                {
                    Logger.Warn($"Cannot determine state for Output Id: {thirdPartyId}");
                    return result;
                }

                int state = GalaxyHelper.GetBit(outputState.ElementAt(byteIndex), bitIndex) ? 1 : 0;

                string stateName = $"COR-{state}";
                Logger.Trace($"Output '{thirdPartyId}' has state: {stateName}");

                result.Add(stateName);
            }
            else
            {
                throw new ArgumentException($"Invalid third party ID for group: {thirdPartyId}");
            }

            return result;
        }

        public void Dispose()
        {
            _stateRefreshTimer.Stop();
            _stateRefreshTimer.Dispose();
        }
    }
}

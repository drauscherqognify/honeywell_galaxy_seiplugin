﻿using SeeTec.EventInterface;
using SeeTec.HoneywellGalaxy.Configuration.SeiPlugin;
using SeeTec.SEIPluginConfiguration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeTec.HoneywellGalaxy.SEIPlugin.Items
{
    /// <summary>
    /// Represents a SEI Item for Honeywell Galaxy Panels.
    /// </summary>
    internal class GalaxySeiItem : SeiItem
    {
        /// <summary>
        /// Third party ID of the Item.
        /// </summary>
        public string ThirdPartyId { get; private set; }

        /// <summary>
        /// The current state of the item.
        /// </summary>
        public int CurrentState { get; set; }

        public List<GalaxyAction> Actions { get; set; }

        /// <summary>
        /// Creates a new instance of the Galaxy SEI Item.
        /// </summary>
        /// <param name="id">Cayuga item ID</param>
        /// <param name="name">Display name.</param>
        /// <param name="typeId">SEI item type ID.</param>
        /// <param name="thirdPartyId">Third party ID.</param>
        public GalaxySeiItem(int id, string name, int typeId, string thirdPartyId, List<GalaxyAction> actions) : base(id, name, typeId)
        {
            ThirdPartyId = thirdPartyId;
            Actions = actions;
            CurrentState = GalaxyConfig.UNKNOWN_STATE_TYPE_ID;
        }

        /// <summary>
        /// Returns a string representation of the item.
        /// </summary>
        /// <returns>String representation of the item.</returns>
        public override string ToString()
        {
            return $"({Id}, {ThirdPartyId}) [{Name}]";
        }
    }
}

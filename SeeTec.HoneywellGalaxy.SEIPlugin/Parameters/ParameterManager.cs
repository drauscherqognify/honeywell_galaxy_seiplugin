﻿using SeeTec.EventInterface;
using SeeTec.Helpers.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SeeTec.HoneywellGalaxy.SEIPlugin.Parameters
{
    /// <summary>
    /// Parameter types. Expand this list if necessary, but don't forget to add handling in the SeiPluginParameterManager below! ;-) 
    /// </summary>
    public enum SeiPluginParameterType
    {
        /// <summary>
        /// Integer values.
        /// </summary>
        INT,

        /// <summary>
        /// String values.
        /// </summary>
        STRING,

        /// <summary>
        /// Comma-separated list of strings. 
        /// Warning: Very simple implementation, cannot (yet) handle strings containing commas!
        /// </summary>
        STRING_LIST,

        /// <summary>
        /// Comma-separated positive integer list with support for dash-noted value ranges. 
        /// Example:
        /// "5, 7, 10-18, 21" --> 5,7,10,11,12,13,14,15,16,17,18,21
        /// </summary>
        INT_LIST_RANGE,

        /// <summary>
        /// Boolean value. Possible values to represent booleans are (case insensitive):
        /// "t", "true", "1" --> true
        /// "f", "false", "0" --> false
        /// </summary>
        BOOL
    }

    /// <summary>
    /// Base class for parameter manager. 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class SeiPluginParameterManager<T>
    {
        /// <summary>
        /// Retrieves list of all available optional parameters
        /// </summary>
        public static List<SeiParameter> AvailableOptionalParameters;

        /// <summary>
        /// List of all available required parameters.
        /// </summary>
        public static List<SeiParameter> AvailableRequiredParameters;

        public bool AreParametersValid { get; private set; }
        
        /// <summary>
        /// Static initialization of the class.
        /// </summary>
        static SeiPluginParameterManager()
        {
            AvailableOptionalParameters = new List<SeiParameter>();
            AvailableRequiredParameters = new List<SeiParameter>();

            System.Reflection.PropertyInfo[] propertyInfo = typeof(T).GetProperties();

            foreach (var prop in propertyInfo)
            {
                SeiPluginParameter param = (SeiPluginParameter)Attribute.GetCustomAttribute(prop, typeof(SeiPluginParameter));

                if (param == null)
                {
                    Logger.Trace($"Property '{prop.Name}' is not annotated, skipping.");
                    continue;
                }

                if (param.IsRequired)
                {
                    AvailableRequiredParameters.Add(new SeiParameter(param.Name, param.DefaultValue, param.IsPassword));
                }
                else
                {
                    AvailableOptionalParameters.Add(new SeiParameter(param.Name, param.DefaultValue, param.IsPassword));
                }
            }
        }

        /// <summary>
        /// Creates a new instance of the parameter manager and initializes with the given required and optional Parameters
        /// passed down from Cayuga.
        /// </summary>
        /// <param name="requiredParameters">The required parameters.</param>
        /// <param name="optionalParameters">The optional parameters.</param>
        public SeiPluginParameterManager(List<SeiParameter> requiredParameters, List<SeiParameter> optionalParameters)
        {
            foreach (var prop in typeof(T).GetProperties())
            {
                SeiPluginParameter paramAttribute = (SeiPluginParameter)Attribute.GetCustomAttribute(prop, typeof(SeiPluginParameter));

                if (paramAttribute == null)
                {
                    continue;
                }

                string value = GetParameterValue(requiredParameters, optionalParameters, paramAttribute);

                prop.SetValue(this, ParseValue(value, paramAttribute.ParameterType));
            }

            AreParametersValid = CheckParameters();
        }

        /// <summary>
        /// Parse the value from the given string according to the given parameter type.
        /// </summary>
        /// <param name="value">The string to parse from.</param>
        /// <param name="type">The parameter type.</param>
        /// <returns>The parsed parameter with the correct type.</returns>
        private object ParseValue(string value, SeiPluginParameterType type)
        {
            switch (type)
            {
                case SeiPluginParameterType.BOOL:
                    return ParseBoolParameter(value);
                case SeiPluginParameterType.INT:
                    return ParseIntParameter(value);
                case SeiPluginParameterType.STRING:
                    return ParseStringParameter(value);
                case SeiPluginParameterType.STRING_LIST:
                    return ParseStringListParameter(value);
                case SeiPluginParameterType.INT_LIST_RANGE:
                    return ParseIntListWithRangeParameter(value);
                default:
                    throw new ArgumentException($"Unsupported plugin parameter type: {type}");
            }
        }

        /// <summary>
        /// Parses a comma-separated string list parameter.
        /// </summary>
        /// <param name="value">Comma-separated value of the parameter.</param>
        /// <returns>List of strings in comma-separated list string.</returns>
        internal static List<string> ParseStringListParameter(string value)
        {
            HashSet<string> result = new HashSet<string>();

            var listItems = value.Split(',');

            foreach (var item in listItems)
            {
                if (!string.IsNullOrWhiteSpace(item))
                {
                    result.Add(item.Trim());
                }
            }

            return result.ToList();
        }

        /// <summary>
        /// Parses a simple string parameter.
        /// </summary>
        /// <param name="value">String value.</param>
        /// <returns>String value.</returns>
        internal static string ParseStringParameter(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return "";
            }

            return value;
        }

        /// <summary>
        /// Parses a boolean parameter. Possible values to represent booleans are (case insensitive):
        /// "t", "true", "1" --> true
        /// "f", "false", "0" --> false
        /// </summary>
        /// <param name="value">String value.</param>
        /// <returns>Boolean value.</returns>
        internal static bool ParseBoolParameter(string value)
        {
            switch (value.ToUpper())
            {
                case "T":
                case "TRUE":
                case "1":
                case "ON":
                    return true;
                case "F":
                case "FALSE":
                case "0":
                case "OFF":
                    return false;
                default:
                    throw new ArgumentException($"Not a boolean value: {value}");
            }
        }

        /// <summary>
        /// Parses a comma-separated positive integer list with support for dash-noted value ranges. 
        /// Example:
        /// "5, 7, 10-18, 21" --> 5,7,10,11,12,13,14,15,16,17,18,21
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        internal static List<int> ParseIntListWithRangeParameter(string value)
        {
            HashSet<int> result = new HashSet<int>();

            if (string.IsNullOrWhiteSpace(value))
            {
                return new List<int>();
            }

            var listItems = value.Split(',');

            foreach (var item in listItems)
            {
                if (string.IsNullOrWhiteSpace(item))
                {
                    continue;
                }

                var trimmedItem = item.Trim();

                // check if item represents a range
                var rangeSplit = trimmedItem.Split('-');

                if (rangeSplit.Length == 1)
                {
                    if (int.TryParse(rangeSplit[0], out int val))
                    {
                        result.Add(val);
                    }
                }
                else if (int.TryParse(rangeSplit[0], out int rangeStart))
                {
                    if (int.TryParse(rangeSplit[rangeSplit.Length - 1], out int rangeEnd))
                    {
                        if (rangeStart > rangeEnd)
                        {
                            int temp = rangeStart;
                            rangeStart = rangeEnd;
                            rangeEnd = temp;
                        }

                        for (int i = rangeStart; i <= rangeEnd; i++)
                        {
                            result.Add(i);
                        }
                    }
                    else
                    {
                    }
                }
            }
            return result.ToList();
        }

        /// <summary>
        /// Parses an int parameter from the given string.
        /// </summary>
        /// <param name="value">String value.</param>
        /// <returns>Integer value.</returns>
        internal static int ParseIntParameter(string value)
        {
            if (int.TryParse(value, out int result))
            {
                return result;
            }
            else
            {
                throw new ArgumentException($"Not an integer value: {value}.");
            }
        }

        /// <summary>
        /// Gets the actual SeiParameter for a given ParameterAttribute.
        /// </summary>
        /// <param name="requiredParameters">The required parameters, potentially containing the requested parameter.</param>
        /// <param name="optionalParameters">The optional parameters, potentially containing the requested parameter.</param>
        /// <param name="paramAttribute">The Parameter to look for.</param>
        /// <returns>The string value of the parameter if found, else the default value of the Parameter.</returns>
        internal static string GetParameterValue(List<SeiParameter> requiredParameters, List<SeiParameter> optionalParameters, SeiPluginParameter paramAttribute)
        {
            string value = null;

            if (paramAttribute.IsRequired)
            {
                Logger.Trace($"Looking for required parameter '{paramAttribute.Name}'");
                // Handle required paramters 
                SeiParameter seiParameter = requiredParameters.Find(p => p.Name.Equals(paramAttribute.Name));

                if (null == seiParameter)
                {
                    // Missing a required parameter is fatal
                    throw new SeiException($"Missing required parameter: {paramAttribute.Name}");
                }

                Logger.Trace($"Value for parameter '{seiParameter.Name}' found.");
                value = seiParameter.Value;
            }
            else
            {
                Logger.Trace($"Looking for optional parameter '{paramAttribute.Name}'");
                SeiParameter seiParameter = optionalParameters.Find(p => p.Name.Equals(paramAttribute.Name));

                if (null != seiParameter)
                {
                    value = seiParameter.Value;
                    Logger.Trace($"Optional parameter '{paramAttribute.Name}' found.");
                }

                if (string.IsNullOrWhiteSpace(value))
                {
                    value = paramAttribute.DefaultValue;
                    Logger.Trace($"Optional parameter '{paramAttribute.Name}' has no value, fall back to default value: {paramAttribute.DefaultValue}");
                }
            }
            return value;
        }

        /// <summary>
        /// Checks if all parameters are valid (number ranges etc.). 
        /// Implement at own convenience.
        /// </summary>
        /// <returns>True if all parameters are valid, false else.</returns>
        internal abstract bool CheckParameters();
    }
}
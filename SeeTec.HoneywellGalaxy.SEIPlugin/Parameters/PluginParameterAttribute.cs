﻿using System;

namespace SeeTec.HoneywellGalaxy.SEIPlugin.Parameters
{
    /// <summary>
    /// Attribute for annotating SEI Plugin Parameters with additional information.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    internal class SeiPluginParameter : Attribute
    {
        /// <summary>
        /// Cayuga display name of the parameter.
        /// </summary>
        public string Name { get; }
        public SeiPluginParameterType ParameterType { get; }

        /// <summary>
        /// Type of the parameter, used to parse the content.
        /// </summary>
        public Type Type { get; }

        /// <summary>
        /// Optional default value for the parameter.
        /// </summary>
        public string DefaultValue { get; }

        /// <summary>
        /// Flag whether it is a required parameter. Else it's an optional parameter.
        /// </summary>
        public bool IsRequired { get; }

        /// <summary>
        /// Flag whether the parameter is a password (defaults to false).
        /// </summary>
        public bool IsPassword { get; }

        /// <summary>
        /// Creates a new Plugin Parameter Attribute.
        /// </summary>
        /// <param name="name">Cayuga display name of the parameter.</param>
        /// <param name="defaultValue">Optional default value for the parameter.</param>
        /// <param name="isRequired">Flag whether it is a required parameter. Else it's an optional parameter.</param>
        /// <param name="isPassword">Flag whether the parameter is a password (defaults to false).</param>
        internal SeiPluginParameter(string name, SeiPluginParameterType parameterType, string defaultValue = "", bool isRequired = false, bool isPassword = false)
        {
            Name = name;
            ParameterType = parameterType;
            DefaultValue = defaultValue;
            IsRequired = isRequired;
            IsPassword = isPassword;
        }
    }
}
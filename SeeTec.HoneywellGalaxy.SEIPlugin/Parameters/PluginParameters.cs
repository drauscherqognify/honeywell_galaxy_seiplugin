﻿using SeeTec.EventInterface;
using SeeTec.Helpers.Logging;
using SeeTec.HoneywellGalaxy.Configuration;
using SeeTec.HoneywellGalaxy.Configuration.SeiPlugin;
using SeeTec.HoneywellGalaxy.SEIPlugin.Helper;
using SeeTec.HoneywellGalaxy.SIAConnector.SIA;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SeeTec.HoneywellGalaxy.SEIPlugin.Parameters
{
    /// <summary>
    /// Manages the Plugin Parameters for the SEI Plugin.
    /// </summary>
    internal class PluginParameters : SeiPluginParameterManager<PluginParameters>
    {
        /// <summary>
        /// PIN to use for authenticating at the Galaxy panel.
        /// </summary>
        [SeiPluginParameter("PIN", SeiPluginParameterType.STRING, "543210", true, true)]
        public string Pin { get; private set; }

        /// <summary>
        /// Host/IP of the Galaxy Panel.
        /// </summary>
        [SeiPluginParameter("Host", SeiPluginParameterType.STRING, isRequired: true)]
        public string Host { get; private set; }

        /// <summary>
        /// Port to listen on for Galaxy SIA events.
        /// </summary>
        [SeiPluginParameter("Event Port", SeiPluginParameterType.INT, "10002")]
        public int EventPort { get; private set; }

        /// <summary>
        /// Whitelist of remote hosts which are allowed to connect to the plugin.
        /// If empty, all hosts are allowed.
        /// </summary>
        [SeiPluginParameter("Allowed Hosts", SeiPluginParameterType.STRING_LIST)]
        public List<string> AllowedHostList { get; private set; }

        /// <summary>
        /// Amount of seconds after which item states are refreshed.
        /// </summary>
        [SeiPluginParameter("State refresh interval (secs)", SeiPluginParameterType.INT, "15")]
        public int StateRefreshIntervalSecs { get; private set; }

        /// <summary>
        /// Log level for the logger.
        /// </summary>
        [SeiPluginParameter("Log Level", SeiPluginParameterType.STRING, "INFO")]
        public string LogLevel { get; private set; }

        /// <summary>
        /// Zone IDs which are exposed to Cayuga. If left blank,
        /// only a single zone item is produced. The actual module ID is then found in the event description.
        /// </summary>
        [SeiPluginParameter("Zone IDs", SeiPluginParameterType.INT_LIST_RANGE)]
        public List<int> ZoneIdList { get; private set; }

        /// <summary>
        /// Group IDs which are exposed to Cayuga. If left blank,
        /// all possible groups are exposed (00-32)
        /// </summary>
        [SeiPluginParameter("Group IDs", SeiPluginParameterType.INT_LIST_RANGE)]
        public List<int> GroupIdList { get; private set; }

        /// <summary>
        /// Module IDs which are exposed to Cayuga. If left blank,
        /// only a single module item is produced. The actual module ID is then found in the event description.
        /// </summary>
        [SeiPluginParameter("Module IDs", SeiPluginParameterType.INT_LIST_RANGE)]
        public List<int> ModuleIdList { get; private set; }

        /// <summary>
        /// User IDs which are exposed to Cayuga. If left blank,
        /// only a single user item is produced. The actual ID is then found in the event description.
        /// </summary>
        [SeiPluginParameter("User IDs", SeiPluginParameterType.INT_LIST_RANGE)]
        public List<int> UserIdList { get; private set; }

        /// <summary>
        /// Output IDs which are exposed to Cayuga. If left blank,
        /// no Output user item is produced.
        /// </summary>
        [SeiPluginParameter("Output IDs", SeiPluginParameterType.INT_LIST_RANGE)]
        public List<int> OutputIdList { get; private set; }

        /// <summary>
        /// Flag whether virtual RIO is enabled (DIP switch 8 = ON).
        /// This changes zone adressing.
        /// </summary>
        [SeiPluginParameter("Enable Virtual RIO", SeiPluginParameterType.BOOL, "false")]
        public bool VirtualRIO { get; private set; }

        /// <summary>
        /// Flag whether User Id List is set in optional parameters.
        /// </summary>
        public bool HasUserIdList => UserIdList != null && UserIdList.Count > 0;

        /// <summary>
        /// Flag whether Zone Id List is set in optional parameters.
        /// </summary>
        public bool HasZoneIdList => ZoneIdList != null && ZoneIdList.Count > 0;

        /// <summary>
        /// Flag whether Group Id List is set in optional parameters.
        /// </summary>
        public bool HasGroupIdList => GroupIdList != null && GroupIdList.Count > 0;

        /// <summary>
        /// Flag whether Output Id List is set in optional parameters.
        /// </summary>
        public bool HasOutputIdList => OutputIdList != null && OutputIdList.Count > 0;

        /// <summary>
        /// Flag whether Module Id List is set in optional parameters.
        /// </summary>
        public bool HasModuleIdList => ModuleIdList != null && ModuleIdList.Count > 0;


        /// <summary>
        /// Creates a new instance of the parameter manager and initializes with the given required and optional Parameters
        /// passed down from Cayuga.
        /// </summary>
        /// <param name="requiredParameters">The required parameters.</param>
        /// <param name="optionalParameters">The optional parameters.</param>
        internal PluginParameters(List<SeiParameter> requiredParameters, List<SeiParameter> optionalParameters) : base(requiredParameters, optionalParameters)
        {
        }

        /// <summary>
        /// Returns the corresponding item id list based on the third party ID of the item type.
        /// </summary>
        /// <param name="thirdPartyId">The third party type id.</param>
        /// <returns>The item ID list.</returns>
        internal List<int> GetSeiItemIdList(string thirdPartyId)
        {
            List<int> idList;
            switch (thirdPartyId)
            {
                case ItemTypeNames.ITEM_TYPE_TID_OUTPUT:
                    idList = OutputIdList;
                    break;
                case ItemTypeNames.ITEM_TYPE_TID_MOD:
                    idList = ModuleIdList;
                    break;
                case ItemTypeNames.ITEM_TYPE_TID_USER:
                    idList = UserIdList;
                    break;
                case ItemTypeNames.ITEM_TYPE_TID_ZONE:
                    idList = ZoneIdList;
                    break;
                case ItemTypeNames.ITEM_TYPE_TID_GROUP:
                    idList = GroupIdList;
                    break;
                case ItemTypeNames.ITEM_TYPE_TID_EVENT:
                default:
                    idList = new List<int>();
                    break;
            }

            return idList;
        }

        /// <summary>
        /// Checks if all parameters are valid. 
        /// </summary>
        /// <returns>True if all parameters are valid, false else.</returns>
        internal override bool CheckParameters()
        {
            bool result = true;

            List<int> newZoneIds = new List<int>();
            foreach(int zId in ZoneIdList)
            {
                if (!GalaxyHelper.IsValidZoneAdress(zId))
                {
                    Logger.Warn($"Removing invalid Zone Adress: {zId}");
                    Logger.Warn($"Valid 3 digit Zone Adresses are 1-512");
                    Logger.Warn($"Valid 4 digit Zone Adresses are <line><rio><zone> with line=1-4, rio=01-15, zone=1-8.");
                    result = false;
                }
                else
                {
                    newZoneIds.Add(zId);
                }
            }
            ZoneIdList = newZoneIds;

            List<int> newGroupIds = new List<int>();
            foreach (int gId in GroupIdList)
            {
                if (gId < 1 || gId > 32)
                {
                    Logger.Warn($"Removing invalid Group ID: {gId}");
                    Logger.Warn($"Valid Group IDs are 1-32.");
                    result = false;
                }
                else
                {
                    newGroupIds.Add(gId);
                }
            }
            GroupIdList = newGroupIds;

            List<int> newOutputIds = new List<int>();
            foreach (int oid in OutputIdList)
            {
                if (oid < 1 || oid > 256)
                {
                    Logger.Warn($"Removing invalid Output ID: {oid}");
                    Logger.Warn($"Valid Output IDs are 1-256.");
                    result = false;
                }
                else
                {
                    newOutputIds.Add(oid);
                }
            }
            OutputIdList = newOutputIds;

            if (StateRefreshIntervalSecs < 0)
            {
                Logger.Warn($"Invalid value for state refresh interval: {StateRefreshIntervalSecs}, value must be positive. Falling back to default value (15 sec).");
                StateRefreshIntervalSecs = 15;
            }

            return result;
        }
    }
}

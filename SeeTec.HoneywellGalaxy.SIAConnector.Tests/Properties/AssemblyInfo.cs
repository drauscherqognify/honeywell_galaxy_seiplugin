using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SeeTec.HoneywellGalaxy.SIAConnector.Tests")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("UnitTests")]
[assembly: AssemblyCopyright("Copyright ©  2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("69bba244-4088-44bc-baa0-6a056ba9d28a")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.131.7125")]
[assembly: AssemblyInformationalVersion("1.0.0")]

// Make internal classes/methods visible to unit test project
[assembly: InternalsVisibleTo("SeeTec.HoneywellGalaxy.UnitTests")]
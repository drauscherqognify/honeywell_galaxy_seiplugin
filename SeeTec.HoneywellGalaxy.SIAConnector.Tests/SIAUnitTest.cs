﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeeTec.HoneywellGalaxy.SIAConnector;

namespace SeeTec.HoneywellGalaxy.UnitTests
{
    /// <summary>
    /// Unit Tests for SeeTec.HoneywellGalaxy.SAIConnector.
    /// </summary>
    [TestClass]
    public class SIAUnitTest
    {
        [TestMethod]
        public void TestSIAMessageFromRaw()
        {
            byte[] rMsg = new byte[] { 0x46, 0x23, 0x30, 0x30, 0x30, 0x30, 0x30, 0x31, 0x9b };

            SIAMessage siaMessage = new SIAMessage(rMsg);

            Assert.AreEqual(rMsg, siaMessage.RawMessage);
            Assert.AreEqual('#', siaMessage.BlockCode);
            Assert.AreEqual("000001", siaMessage.BlockData);
            Assert.AreEqual(6, siaMessage.BlockLength);
            Assert.AreEqual(0x9b, siaMessage.Checksum);
            Assert.IsTrue(siaMessage.Valid);
            Assert.IsTrue(siaMessage.AckRequest);
        }

        [TestMethod]
        public void TestSIAMessageFromString()
        {
            string blockData = "000001";
            char blockCode = '#';
            byte[] expectedRawMessage = new byte[] { 0x46, 0x23, 0x30, 0x30, 0x30, 0x30, 0x30, 0x31, 0x9b };

            SIAMessage siaMessage = new SIAMessage(blockCode, blockData, true);

            Assert.AreEqual('#', siaMessage.BlockCode);
            Assert.AreEqual("000001", siaMessage.BlockData);
            Assert.AreEqual(6, siaMessage.BlockLength);
            Assert.AreEqual(0x9b, siaMessage.Checksum);
            Assert.IsTrue(siaMessage.Valid);
            Assert.IsTrue(siaMessage.AckRequest);

            Assert.AreEqual(expectedRawMessage.Length, siaMessage.RawMessage.Length);
            for (int i = 0; i < expectedRawMessage.Length; i++)
            {
                Assert.AreEqual(expectedRawMessage[i], siaMessage.RawMessage[i]);
            }

            blockData = "ZS102";
            blockCode = 'X';
            
            expectedRawMessage = new byte[] { 0x45, 0x58, 0x5A, 0x53, 0x31, 0x30, 0x32, 0xd8 };
            siaMessage = new SIAMessage(blockCode, blockData, true);
            Assert.AreEqual('X', siaMessage.BlockCode);
            Assert.AreEqual("ZS102", siaMessage.BlockData);
            Assert.AreEqual(5, siaMessage.BlockLength);
            Assert.AreEqual(0xd8, siaMessage.Checksum);
            Assert.IsTrue(siaMessage.Valid);
            Assert.IsTrue(siaMessage.AckRequest);

            Assert.AreEqual(expectedRawMessage.Length, siaMessage.RawMessage.Length);
            for (int i = 0; i < expectedRawMessage.Length; i++)
            {
                Assert.AreEqual(expectedRawMessage[i], siaMessage.RawMessage[i]);
            }
        }

        [TestMethod]
        public void TestSiaMessageEmpty()
        {
            SIAMessage msg = new SIAMessage('8');
            string strMsg = SIACoder.GetRawMessageString(msg.RawMessage);
            byte[] expectedRawMessage = new byte[] { 0x0, 0x38, 0xc7};

            Assert.IsTrue(msg.Valid);
            Assert.AreEqual("", msg.BlockData);
            Assert.AreEqual(0, msg.BlockLength);
            Assert.AreEqual('8', msg.BlockCode);
            Assert.AreEqual(0xc7, msg.Checksum);
            Assert.AreEqual(expectedRawMessage.Length, msg.RawMessage.Length);
            for (int i = 0; i < expectedRawMessage.Length; i++)
            {
                Assert.AreEqual(expectedRawMessage[i], msg.RawMessage[i]);
            }
        }

        [TestMethod]
        public void TestCalculateChecksum()
        {
            byte[] rMsg = new byte[] { 0x46, 0x23, 0x30, 0x30, 0x30, 0x30, 0x30, 0x31, 0x9b };
            byte[] sMsg = new byte[rMsg.Length - 1];
            Array.Copy(rMsg, sMsg, sMsg.Length);

            byte calcChecksum = SIACoder.CalculateChecksum(sMsg);
            byte checksum = rMsg.Last();
            
            Assert.AreEqual(calcChecksum, checksum);

            byte[] rMsgInvalid = new byte[] { 0x46, 0x23, 0x31, 0x30, 0x30, 0x30, 0x30, 0x31, 0x9b };
            byte[] sMsgInvalid = new byte[rMsgInvalid.Length - 1];
            Array.Copy(rMsgInvalid, sMsgInvalid, sMsgInvalid.Length);

            byte calcChecksumInvalid = SIACoder.CalculateChecksum(sMsgInvalid);
            byte checksumInvalid = rMsgInvalid.Last();

            Assert.AreNotEqual(calcChecksumInvalid, checksumInvalid);
        }

        [TestMethod]
        public void TestAckRequestBit()
        {
            byte header = 0x45;
            header = SIACoder.SetAckRequestBit(header, false);

            Assert.AreEqual(0x5, header);

            header = SIACoder.SetAckRequestBit(header, true);
            Assert.AreEqual(0x45, header);
        }

        [TestMethod]
        public void TestEncodeHeader()
        {
            string blockData = "543210";
            byte expectedHeader = 0x46;
            byte header = SIACoder.EncodeHeader(blockData.Length, true);
            Assert.AreEqual(expectedHeader, header);

            blockData = "ZS102";
            expectedHeader = 0x45;
            header = SIACoder.EncodeHeader(blockData.Length, true);
            Assert.AreEqual(expectedHeader, header);

            expectedHeader = 0x5;
            header = SIACoder.EncodeHeader(blockData.Length, false);
            Assert.AreEqual(expectedHeader, header);
        }

        [TestMethod]
        public void TestSIAEventZoneWithGroup()
        {
            string blockData = "ti12:34/ri00/AT1005";
            string asciiBlock = "eeeeeeeeesiiiiiiiidddddddddddddddd";
            SIAEvent ev = new SIAEvent('N', blockData, asciiBlock);

            Assert.AreEqual("AT", ev.EventCode);
            Assert.IsTrue(ev.IsValidEventCode);
            Assert.AreEqual("12:34", ev.Time);
            Assert.IsTrue(ev.HasGroupId);
            Assert.AreEqual("00", ev.Group);
            Assert.IsTrue(ev.HasZoneId);
            Assert.AreEqual("1005", ev.ZoneId);
            Assert.AreEqual(SIAEventType.ZONE, ev.EventType);
            Assert.IsFalse(ev.HasPeripheryId);
            Assert.IsFalse(ev.HasUserId);
        }

        [TestMethod]
        public void TestSIAEventZoneWithoutGroup()
        {
            string blockData = "ti12:34/AT1005";
            string asciiBlock = "eeeeeeeeesiiiiiiiidddddddddddddddd";
            SIAEvent ev = new SIAEvent('N', blockData, asciiBlock);

            Assert.AreEqual("AT", ev.EventCode);
            Assert.IsTrue(ev.IsValidEventCode);
            Assert.AreEqual("12:34", ev.Time);
            Assert.IsFalse(ev.HasGroupId);
            Assert.IsTrue(ev.HasZoneId);
            Assert.AreEqual("1005", ev.ZoneId);
            Assert.AreEqual(SIAEventType.ZONE, ev.EventType);
            Assert.IsFalse(ev.HasPeripheryId);
            Assert.IsFalse(ev.HasUserId);
        }

        [TestMethod]
        public void TestSIAEventUserWithGroup()
        {
            string blockData = "ti12:34/ri01/id321/pi012/CE";
            string asciiBlock = "eeeeeeeeesiiiiiiiidddddd";
            SIAEvent ev = new SIAEvent('N', blockData, asciiBlock);

            Assert.IsTrue(ev.HasGroupId);
            Assert.IsFalse(ev.HasZoneId);
            Assert.IsTrue(ev.HasPeripheryId);
            Assert.IsTrue(ev.HasUserId);

            Assert.AreEqual("CE", ev.EventCode);
            Assert.IsTrue(ev.IsValidEventCode);

            Assert.AreEqual("12:34", ev.Time);
            Assert.AreEqual("01", ev.Group);
            Assert.AreEqual("012", ev.PeripheryId);

            Assert.AreEqual(SIAEventType.USER, ev.EventType);
        }

        [TestMethod]
        public void TestSIAEventUserWithoutGroup()
        {
            string blockData = "ti12:34/id321/pi012/CE";
            string asciiBlock = "eeeeeeeeesiiiiiiiidddddd";
            SIAEvent ev = new SIAEvent('N', blockData, asciiBlock);

            Assert.IsFalse(ev.HasGroupId);
            Assert.IsFalse(ev.HasZoneId);
            Assert.IsTrue(ev.HasPeripheryId);
            Assert.IsTrue(ev.HasUserId);

            Assert.AreEqual("CE", ev.EventCode);
            Assert.IsTrue(ev.IsValidEventCode);

            Assert.AreEqual("12:34", ev.Time);
            Assert.AreEqual("012", ev.PeripheryId);

            Assert.AreEqual(SIAEventType.USER, ev.EventType);
        }

        [TestMethod]
        public void TestSIAEventModuleWithGroup()
        {
            string blockData = "ti09:12/ri03/pi003/TA";
            string asciiBlock = "eeeeeeeeesiiiiiiiiddd";

            SIAEvent ev = new SIAEvent('N', blockData, asciiBlock);

            Assert.IsTrue(ev.HasGroupId);
            Assert.IsFalse(ev.HasZoneId);
            Assert.IsTrue(ev.HasPeripheryId);
            Assert.IsFalse(ev.HasUserId);

            Assert.AreEqual("TA", ev.EventCode);
            Assert.IsTrue(ev.IsValidEventCode);

            Assert.AreEqual("09:12", ev.Time);
            Assert.AreEqual("03", ev.Group);
            Assert.AreEqual("003", ev.PeripheryId);

            Assert.AreEqual(SIAEventType.MODULE, ev.EventType);
        }

        [TestMethod]
        public void TestSIAEventModuleWithoutGroup()
        {
            string blockData = "ti09:12/pi003/TA";
            string asciiBlock = "eeeeeeeeesiiiiiiiiddd";

            SIAEvent ev = new SIAEvent('N', blockData, asciiBlock);

            Assert.IsFalse(ev.HasGroupId);
            Assert.IsFalse(ev.HasZoneId);
            Assert.IsTrue(ev.HasPeripheryId);
            Assert.IsFalse(ev.HasUserId);

            Assert.AreEqual("TA", ev.EventCode);
            Assert.IsTrue(ev.IsValidEventCode);

            Assert.AreEqual("09:12", ev.Time);
            Assert.AreEqual("003", ev.PeripheryId);

            Assert.AreEqual(SIAEventType.MODULE, ev.EventType);
        }

        [TestMethod]
        public void TestSIAEventEventWithGroup()
        {
            string blockData = "ti11:22/ri05/CG";
            string asciiBlock = "eeeeeeeeesiiiiiiii";

            SIAEvent ev = new SIAEvent('N', blockData, asciiBlock);

            Assert.IsTrue(ev.HasGroupId);
            Assert.IsFalse(ev.HasZoneId);
            Assert.IsFalse(ev.HasPeripheryId);
            Assert.IsFalse(ev.HasUserId);

            Assert.AreEqual("CG", ev.EventCode);
            Assert.IsTrue(ev.IsValidEventCode);

            Assert.AreEqual("11:22", ev.Time);
            Assert.AreEqual("05", ev.Group);

            Assert.AreEqual(SIAEventType.EVENT, ev.EventType);
        }

        [TestMethod]
        public void TestSIAEventEventWithoutGroup()
        {
            string blockData = "ti21:32/CG";
            string asciiBlock = "eeeeeeeeesiiiiiiii";

            SIAEvent ev = new SIAEvent('N', blockData, asciiBlock);

            Assert.IsFalse(ev.HasGroupId);
            Assert.IsFalse(ev.HasZoneId);
            Assert.IsFalse(ev.HasPeripheryId);
            Assert.IsFalse(ev.HasUserId);

            Assert.AreEqual("CG", ev.EventCode);
            Assert.IsTrue(ev.IsValidEventCode);

            Assert.AreEqual("21:32", ev.Time);

            Assert.AreEqual(SIAEventType.EVENT, ev.EventType);
        }

        [TestMethod]
        public void TestSIAAsciiBlock()
        {
            string zoneAB = "eeeeeeeeesiiiiiiiidddddddddddddddd";
            SIAAsciiBlock ab = new SIAAsciiBlock(zoneAB);
            Assert.AreEqual("eeeeeeeee", ab.LogEvent);
            Assert.AreEqual("s", ab.EventState);
            Assert.AreEqual("iiiiiiii", ab.SiteIdentifier);
            Assert.AreEqual("dddddddddddddddd", ab.Descriptor);

            string userAB = "eeeeeeeeesiiiiiiiidddddd";
            ab = new SIAAsciiBlock(userAB);
            Assert.AreEqual("eeeeeeeee", ab.LogEvent);
            Assert.AreEqual("s", ab.EventState);
            Assert.AreEqual("iiiiiiii", ab.SiteIdentifier);
            Assert.AreEqual("dddddd", ab.Descriptor);

            string moduleAB = "eeeeeeeeesiiiiiiiiddd";
            ab = new SIAAsciiBlock(moduleAB);
            Assert.AreEqual("eeeeeeeee", ab.LogEvent);
            Assert.AreEqual("s", ab.EventState);
            Assert.AreEqual("iiiiiiii", ab.SiteIdentifier);
            Assert.AreEqual("ddd", ab.Descriptor);

            string eventAB = "eeeeeeeeesiiiiiiii";
            ab = new SIAAsciiBlock(eventAB);
            Assert.AreEqual("eeeeeeeee", ab.LogEvent);
            Assert.AreEqual("s", ab.EventState);
            Assert.AreEqual("iiiiiiii", ab.SiteIdentifier);
            Assert.AreEqual("", ab.Descriptor);
        }
    }
}

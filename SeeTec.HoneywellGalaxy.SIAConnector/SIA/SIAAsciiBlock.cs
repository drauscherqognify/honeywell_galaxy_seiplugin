﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SeeTec.HoneywellGalaxy.SIAConnector
{
    /// <summary>
    ///  Represents an ASCII block sent alongside SIA events.
    /// </summary>
    public class SIAAsciiBlock
    {
        /// <summary>
        /// Version of the ASCII Block - this is needed due to discrepancies between the documentation
        /// and the actual ASCII Blocks received from the hardware during development.
        /// </summary>
        public enum BlockVersion
        {
            /// <summary>
            /// ASCII Block corresponds to the pattern as documented in "Galaxy SIA Interface Specification Rev 1.05".
            /// </summary>
            DOCUMENTATION,

            /// <summary>
            /// ASCII Block corresponds to the pattern as observed from hardware.
            /// </summary>
            HARDWARE
        }

        /// <summary>
        /// The ASCII block pattern as documented.
        /// </summary>
        private const string BLOCK_PATTERN_FROM_DOCUMENTATION = "eeeeeeeeesiiiiiiiidddddddddddddddd";

        /// <summary>
        /// The ASCII block pattern as observed from actual hardware during development.
        /// </summary>
        private const string BLOCK_PATTERN_FROM_HARDWARE = "seeeeeeeeeiiiiiiiidddddddddddddddd";

        /// <summary>
        /// The log event contained in the ASCII block (pattern 'e').
        /// </summary>
        public string LogEvent { get; private set; }

        /// <summary>
        /// The event state contained in the ASCII block (pattern 's').
        /// </summary>
        public string EventState { get; private set; }

        /// <summary>
        /// The site identifier contained in the ASCII block (pattern 'i').
        /// </summary>
        public string SiteIdentifier { get; private set; }

        /// <summary>
        /// The descriptor contained in the ASCII block (pattern 'd').
        /// </summary>
        public string Descriptor { get; private set; }

        /// <summary>
        /// The version of this instance.
        /// </summary>
        public BlockVersion Version;

        /// <summary>
        /// Creates a new instance of ASCII block from the given raw ASCII block string.
        /// </summary>
        /// <param name="rawAsciiBlock">The raw ASCII block.</param>
        public SIAAsciiBlock(string rawAsciiBlock)
        {
            if (string.IsNullOrEmpty(rawAsciiBlock))
            {
                throw new ArgumentException("Empty/Null ASCII block passed.");
            }
            InitFromRawAsciiBlock(rawAsciiBlock);
        }

        /// <summary>
        /// Helper method to parse an ASCII block.
        /// </summary>
        /// <param name="block">The raw ASCII block to parse.</param>
        /// <param name="version"></param>
        /// <returns></returns>
        private Dictionary<char, string> ParseAsciiBlock(string block, BlockVersion version = BlockVersion.DOCUMENTATION)
        {
            Dictionary<char, string> result = new Dictionary<char, string>();

            string pattern = "";

            switch (version)
            {
                case BlockVersion.HARDWARE:
                    pattern = BLOCK_PATTERN_FROM_HARDWARE;
                    break;
                default:
                case BlockVersion.DOCUMENTATION:
                    pattern = BLOCK_PATTERN_FROM_DOCUMENTATION;
                    break;
            }

            foreach (char c in pattern.Distinct())
            {
                result.Add(c, "");
            }

            for (int i = 0; i < pattern.Length; i++)
            {
                char pChar = pattern[i];

                if (i > block.Length - 1)
                {
                    break;
                }

                char bChar = block[i];

                result[pChar] += bChar;
            }

            return result;
        }

        /// <summary>
        /// Helper method to parse the given raw ASCII block into this instance.
        /// </summary>
        /// <param name="rawAsciiBlock">The raw ASCII block.</param>
        private void InitFromRawAsciiBlock(string rawAsciiBlock)
        {
            Version = GuessVersion(rawAsciiBlock);

            var blockDict = ParseAsciiBlock(rawAsciiBlock, Version);

            LogEvent = blockDict['e'].Trim();
            EventState = blockDict['s'];
            SiteIdentifier = blockDict['i'];
            Descriptor = blockDict['d'];
        }

        /// <summary>
        /// Helper method to attempt to guess the version of the ASCII block.
        /// </summary>
        /// <param name="rawAsciiBlock">The ASCII block.</param>
        /// <returns>The guessed version of the ASCII block.</returns>
        private BlockVersion GuessVersion(string rawAsciiBlock)
        {
            List<char> possibleStates = new List<char>() { ' ', '+', '-' };
            char firstChar = rawAsciiBlock[0];

            if (possibleStates.Contains(firstChar))
            {
                return BlockVersion.HARDWARE;
            }

            return BlockVersion.DOCUMENTATION;
        }

        /// <summary>
        /// Returns a human readable representation of the ASCII block object.
        /// </summary>
        /// <returns>A human readable representation of the ASCII block object.</returns>
        public override string ToString()
        {
            return $"Version=[{Version}], LogEvent=[{LogEvent}], EventState=[{EventState}], SiteIdentifier=[{SiteIdentifier}], Descriptor=[{Descriptor}]";
        }
    }
}

﻿namespace SeeTec.HoneywellGalaxy.SIAConnector.SIA
{
    /// <summary>
    /// Constants for the SIA Block Codes.
    /// </summary>
    internal static class SIABlockCode
    {
        /// <summary>
        /// End Data (Inform receiver, telecom mod will hang up)
        /// </summary>
        public const char END_DATA = '0';

        /// <summary>
        /// Positive Ack (Galaxy control command successful or event successfully received) 
        /// </summary>
        public const char POS_ACK = '8';

        /// <summary>
        /// Negative Ack (Galaxy PIN/control command unsuccessful or event reception failed)
        /// </summary>
        public const char REJECT = '9';
        
        /// <summary>
        /// Control commands, reply to control command
        /// </summary>
        public const char CONTROL = 'C';
        
        /// <summary>
        /// New Event
        /// </summary>
        public const char NEW_EVENT = 'N';
        
        /// <summary>
        /// Old Event
        /// </summary>
        public const char OLD_EVENT = 'O';

        /// <summary>
        /// Config, PIN was successful
        /// </summary>
        public const char CONFIG = '@';
        
        /// <summary>
        /// Used to send site identification from panel
        /// </summary>
        public const char ID = '#';
        
        /// <summary>
        /// ASCII (Additional Information, sent with 'N' and 'O' blocks
        /// </summary>
        public const char ASCII = 'A';
        
        /// <summary>
        /// PIN
        /// </summary>
        public const char PIN = '?';

        /// <summary>
        /// Extended control command
        /// </summary>
        public const char EXTEND = 'X';
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeTec.HoneywellGalaxy.SIAConnector
{
    /// <summary>
    /// Methods to help with encoding and decoding SIA messages.
    /// </summary>
    public class SIACoder
    {
        /// <summary>
        /// Character encoding for the SIA message stream.
        /// </summary>
        private const string CHARACTER_ENCODING = "ibm850";

        /// <summary>
        /// Extracts the block length from a SIA header.
        /// </summary>
        /// <param name="header">The SIA header.</param>
        /// <returns>The block length.</returns>
        public static int GetBlockLength(byte header)
        {
            var bits = new BitArray(header);
            string bitString = bits.ToString();
            var result = (header >> 0) & 63;
            return result;
        }

        /// <summary>
        /// Extracts the Acknowledgement Request bit from a SIA header.
        /// </summary>
        /// <param name="header">The header.</param>
        /// <returns>True if the Acknowledgement Request bit is set, false else.</returns>
        public static bool GetAckRequestBit(byte header)
        {
            return (header & (1 << 6)) != 0;
        }

        /// <summary>
        /// Validates a raw SIA byte message using the given checksum.
        /// </summary>
        /// <param name="rawMessage">The SIA byte message.</param>
        /// <param name="checksum">The checksum.</param>
        /// <returns>True if the message is valid against the checksum, false else.</returns>
        public static bool Validate(byte[] rawMessage, byte checksum)
        {
            byte calculatedChecksum = CalculateChecksum(rawMessage);

            var result = checksum == calculatedChecksum;

            return result;
        }

        /// <summary>
        /// Calculates the checksum for a raw SIA byte message.
        /// </summary>
        /// <param name="message">The SIA byte message.</param>
        /// <returns>The checksum.</returns>
        public static byte CalculateChecksum(byte[] message)
        {
            byte calculatedChecksum = 0xFF;

            foreach (byte c in message)
            {
                calculatedChecksum = (byte) (calculatedChecksum ^ c);
            }

            return calculatedChecksum;
        }

        /// <summary>
        /// Calculates the checksum for a raw SIA byte message.
        /// </summary>
        /// <param name="message">The SIA byte message.</param>
        /// <returns>The checksum.</returns>
        public static byte CalculateChecksum(string message)
        {
            byte[] bMsg = GetBytes(message);
            return CalculateChecksum(bMsg);
        }

        /// <summary>
        /// Converts the given string to raw bytes.
        /// </summary>
        /// <param name="message">The message to convert.</param>
        /// <returns>Byte array containing raw bytes of the message.</returns>
        public static byte[] GetBytes(string message)
        {
            var enc = Encoding.GetEncoding(CHARACTER_ENCODING);
            return enc.GetBytes(message);
        }

        /// <summary>
        /// Converts the raw message to a string (not including checksum byte).
        /// </summary>
        /// <param name="rawMessage">The raw message bytes.</param>
        /// <returns>The message as string.</returns>
        public static string GetMessageString(byte[] rawMessage)
        {
            if (rawMessage == null || rawMessage.Length < 2)
            {
                return "INVALID MESSAGE";
            }

            var enc = Encoding.GetEncoding(CHARACTER_ENCODING);

            StringBuilder sb = new StringBuilder();

            sb.Append(enc.GetString(
                rawMessage, 0, 2 + SIACoder.GetBlockLength(rawMessage.First())));

            return sb.ToString();
        }

        /// <summary>
        /// Converts the raw message to a string (including checksum byte).
        /// </summary>
        /// <param name="rawMessage">The raw message bytes.</param>
        /// <returns>The message as string.</returns>
        public static string GetRawMessageString(byte[] rawMessage)
        {
            var enc = Encoding.GetEncoding(CHARACTER_ENCODING);

            StringBuilder sb = new StringBuilder();

            sb.Append(enc.GetString(
                rawMessage, 0, rawMessage.Length));

            return sb.ToString();
        }

        /// <summary>
        /// Extracts the SIA messages contained in the given buffer into the given 
        /// result list. The remaining buffer may contain incomplete messages and is
        /// returned.
        /// </summary>
        /// <param name="buf">The buffer to get the messages from.</param>
        /// <param name="messageList">The result list to add the decoded messages to.</param>
        /// <returns>The remaining buffer.</returns>
        internal static byte[] GetAllMessages(byte[] buf, List<SIAMessage> messageList)
        {
            List<byte> allBytes = new List<byte>(buf);

            // get first byte (assume it's a SIA header), and calculate total message size
            while (allBytes.Count > 0)
            {
                byte header = allBytes[0];
                int messageLength = SIACoder.GetBlockLength(header) + 3; // 1 byte header, 1 byte block code, <blockLength> bytes data, 1 byte checksum

                // check if (another) complete message is in buffer
                if (allBytes.Count >= messageLength)
                {
                    // pop the raw message 
                    byte[] rawMessage = allBytes.GetRange(0, messageLength).ToArray();
                    allBytes.RemoveRange(0, messageLength);

                    // Add SIA message to output list
                    messageList.Add(new SIAMessage(rawMessage));
                }
                else
                {
                    break;
                }
            }

            // return remaining bytes
            return allBytes.ToArray();
        }

        /// <summary>
        /// Encodes the SIA message header byte from given block length and acknowledgement request bit.
        /// </summary>
        /// <param name="blockLength">The block lenght.</param>
        /// <param name="ackRequest">Flag whether acknowledgement request is set.</param>
        /// <returns>The header byte.</returns>
        public static byte EncodeHeader(int blockLength, bool ackRequest)
        {
            byte result = 0;
            result = SetAckRequestBit(result, ackRequest);
            result = (byte) (result | blockLength);
            return result;
        }

        /// <summary>
        /// Sets the acknowledgement reqeust bit in a header byte.
        /// </summary>
        /// <param name="ackRequest">Flag whether acknowledgement request is set.</param>
        /// <param name="header"></param>
        /// <returns></returns>
        public static byte SetAckRequestBit(byte header, bool ackRequest)
        {
            if (ackRequest)
            {
                header = (byte)(header | (1 << 6));
            }
            else
            {
                header = (byte)(header & ~(1 << 6));
            }
            return header;
        }

        /// <summary>
        /// Method to extract the actual message from the given raw message (without or without the checksum byte).
        /// </summary>
        /// <param name="rawMessage">The raw message bytes.</param>
        /// <returns>The message bytes without checksum byte.</returns>
        public static byte[] GetMessageWithoutParity(byte[] rawMessage)
        {
            byte[] result = new byte[2 + GetBlockLength(rawMessage.First())];
            Array.Copy(rawMessage, result, result.Length);
            return result;
        }
    }
}

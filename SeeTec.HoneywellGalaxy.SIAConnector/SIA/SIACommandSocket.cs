﻿using SeeTec.EventInterface;
using SeeTec.Helpers.Logging;
using SeeTec.HoneywellGalaxy.Configuration.SeiPlugin;
using SeeTec.HoneywellGalaxy.SEIPlugin.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace SeeTec.HoneywellGalaxy.SIAConnector.SIA
{
    /// <summary>
    /// Represents a socket over which commands and status requests can be sent to 
    /// the Galaxy panel.
    /// </summary>
    public class SIACommandSocket
    {
        /// <summary>
        /// Receiving buffer size for the socket.
        /// </summary>
        private const int BUFFER_SIZE = 1024;

        /// <summary>
        /// Default timeout for command/status replies.
        /// </summary>
        private const int DEFAULT_RECEIVE_TIMEOUT_MS = 30000;

        /// <summary>
        /// Command port at the panel.
        /// </summary>
        private const int COMMAND_PORT = 10005;

        /// <summary>
        /// Reply from the panel if a login is succesful.
        /// </summary>
        private const string LOGIN_SUCCESSFUL_REPLY = "AL4B15";

        /// <summary>
        /// Host/IP of the Galaxy panel.
        /// </summary>
        private readonly string _host;

        /// <summary>
        /// User PIN to use for login at the panel.
        /// </summary>
        private readonly string _pin;

        /// <summary>
        /// Creates a new instance of the SIA Command Socket.
        /// </summary>
        /// <param name="host">Host/IP of the panel.</param>
        /// <param name="port">Command port of the panel.</param>
        /// <param name="pin">PIN to use for authentication.</param>
        public SIACommandSocket(string host, string pin)
        {
            _host = host;
            _pin = pin;
        }

        /// <summary>
        /// Logs in at the panel.
        /// </summary>
        /// <param name="sock">Socket to send the log in request at.</param>
        /// <param name="pin">PIN to use for authentication.</param>
        /// <returns>True if login was successful, false else.</returns>
        private bool Login(Socket sock, string pin, bool manyCommands = false)
        {
            SIAMessage loginMessage;
            if (manyCommands)
            {
                loginMessage = new SIAMessage(SIABlockCode.PIN, $"{pin}*1");
            }
            else
            {
                loginMessage = new SIAMessage(SIABlockCode.PIN, pin);
            }

            var reply = SendSingleMessage(sock, loginMessage.RawMessage);

            if (null != reply && reply.BlockData.Contains(LOGIN_SUCCESSFUL_REPLY))
            {
                return true;
            }

            if (null != reply && reply.BlockData.Contains($"{SIABlockCode.REJECT}"))
            {
                Logger.Error("Invalid PIN.");
            }

            return false;
        }

        /// <summary>
        /// Sends a list of requests and returns a list of replies.
        /// </summary>
        /// <param name="messageList">The requests.</param>
        /// <returns>Replies in same order as requests.</returns>
        public List<SIAMessage> SendAllMessages(List<SIAMessage> messageList)
        {
            List<SIAMessage> result = new List<SIAMessage>();

            Socket sock = ConnectSocket();

            // Login for multiple commands
            if (true == Login(sock, _pin, true))
            {
                Logger.Trace("Login successful, sending commands.");
                
                // send all commands
                foreach(SIAMessage message in messageList)
                {
                    SIAMessage reply = SendSingleMessage(sock, message.RawMessage);
                    // collect replies
                    result.Add(reply);
                }
            }
            else
            {
                throw new SeiException("Unable to log in at the panel.");
            }

            sock.Disconnect(true);
            return result;
        }

        /// <summary>
        /// Sends a message with automatic login. 
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <returns></returns>
        public SIAMessage SendMessage(byte[] message)
        {
            Socket sock = ConnectSocket();

            SIAMessage reply = null;

            if (true == Login(sock, _pin))
            {
                Logger.Trace("Login successful, sending command.");
                reply = SendSingleMessage(sock, message);
            }
            else
            {
                throw new SeiException("Unable to log in at the panel.");
            }

            sock.Disconnect(true);
            return reply;
        }

        /// <summary>
        /// Helper method to connect the command socket.
        /// </summary>
        /// <returns>The connected command socket.</returns>
        private Socket ConnectSocket()
        {
            Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                Logger.Trace("Trying to connect to panel...");
                sock.Connect(_host, COMMAND_PORT);

                if (!sock.Connected)
                {
                    throw new SeiException("Connection to remote host timed out.");
                }
            }
            catch (SocketException se)
            {
                if (se.SocketErrorCode == SocketError.ConnectionRefused)
                {
                    throw new SeiException($"The panel refused the connection. Check panel configuration (SIA command host).");
                }

                throw new SeiException($"Unable to connect to panel: {se.Message}");
            }

            Logger.Trace($"Connection to panel established at {_host}:{COMMAND_PORT}");
            return sock;
        }

        /// <summary>
        /// Sends a single message (no login).
        /// </summary>
        /// <param name="sock">Socket to use.</param>
        /// <param name="message">Message to send.</param>
        /// <param name="closeAfterReply"></param>
        /// <returns></returns>
        private SIAMessage SendSingleMessage(Socket sock, byte[] message, bool closeAfterReply = true)
        {
            // Data buffer for incoming data.  
            byte[] bytes = new byte[BUFFER_SIZE];

            try
            {
                sock.Send(message);

                Logger.Trace("Waiting for reply...");

                sock.ReceiveTimeout = DEFAULT_RECEIVE_TIMEOUT_MS;
                int bytesRec = sock.Receive(bytes);

                Logger.Trace($"Received {bytesRec} bytes.");
                if (bytesRec > 0)
                {
                    byte[] rawMessage = new byte[bytesRec];
                    Array.Copy(bytes, rawMessage, bytesRec);

                    string strMessage = Encoding.ASCII.GetString(rawMessage);
                    Logger.Trace($"Message: [{strMessage}]");

                    SIAMessage reply = new SIAMessage(rawMessage);

                    return reply;
                }
                else
                {
                    throw new SeiException($"Panel did not reply to command within {DEFAULT_RECEIVE_TIMEOUT_MS / 1000} seconds.");
                }
            }
            catch (SocketException se)
            {
                switch (se.SocketErrorCode)
                {
                    case SocketError.TimedOut:
                        throw new SeiException($"The connection timed out after {DEFAULT_RECEIVE_TIMEOUT_MS / 1000} seconds.");
                    case SocketError.ConnectionRefused:
                        throw new SeiException($"The connection was refused at {_host}:{COMMAND_PORT}.");
                    default:
                        throw new SeiException($"Error connecting to {_host}:{COMMAND_PORT}: {se.Message}", se);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Gets the version of the panel.
        /// </summary>
        /// <returns>The version of the panel.</returns>
        public SIAPanelVersion GetPanelVersion()
        {
            Logger.Trace("Querying Panel version...");

            SIAPanelVersion result;
            var version = SendMessage(new SIAMessage(SIABlockCode.EXTEND, "VN").RawMessage);

            Logger.Debug($"Panel version reply: {version}");

            if (version.BlockData.Contains("XVN"))
            {
                Logger.Info($"Panel version XVN ({version})");
                result = SIAPanelVersion.XVN;
            }
            else if (version.BlockData.Contains("CVN"))
            {
                Logger.Info($"Panel version CVN ({version})");
                result = SIAPanelVersion.CVN;
            }
            else
            {
                Logger.Warn($"Panel did not provide version, assuming CVN.");
                result = SIAPanelVersion.CVN;
            }

            return result;
        }

        /// <summary>
        /// Executes an action for an item.
        /// </summary>
        /// <param name="action">Action to execute.</param>
        /// <param name="itemTID">Third party item ID to execute the action for.</param>
        /// <returns>True if the command gets a positive ack reply, false else.</returns>
        public bool ExecuteAction(GalaxyAction action, string itemTID)
        {
            switch (action.ItemTypeID)
            {
                case ItemTypeNames.ITEM_TYPE_TID_ZONE:
                case ItemTypeNames.ITEM_TYPE_TID_GROUP:
                    string siaCommand = $"{action.Command}{itemTID}*{action.Value}";
                    Logger.Debug($"Sending SIA command: {siaCommand}");

                    SIAMessage msg = new SIAMessage(siaCommand, true);
                    SIAMessage reply = SendMessage(msg.RawMessage);

                    Logger.Debug($"Panel command reply: {SIACoder.GetMessageString(reply?.RawMessage)}");

                    if (null != reply && reply.BlockCode.Equals(SIABlockCode.POS_ACK))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                case ItemTypeNames.ITEM_TYPE_TID_OUTPUT:
                    // Special treatment for OUTPUTs 
                    byte[] bytes = new byte[32];

                    // try to get group id as int
                    if (int.TryParse(itemTID, out int intId))
                    {
                        if (intId > 256)
                        {
                            Logger.Error($"Invalid Output ID '{itemTID}' - no command sent.");
                            return false;
                        }

                        string cmdSuffix = "1000";
                        
                        // per byte 4 bits mask, 4 bits op
                        if (intId > 128)
                        {
                            // Roll over
                            intId = intId - 128;
                            cmdSuffix = "1001";
                        }

                        int byteIndex = ((intId - 1) / 4);

                        if (byteIndex > (bytes.Length) - 1)
                        {
                            // byte index outside array, error
                        }

                        int opBitIndex = ((intId - 1) % 4);
                        int maskBitIndex = ((intId - 1) % 4) + 4;

                        bool value = action.Value == 1;
                        Logger.Trace($"{itemTID} -> byteIndex {byteIndex}, maskBit {maskBitIndex}, opBit {opBitIndex}, value {value}");


                        // set 1 bit in appropriate byte (mask and op)
                        bytes[byteIndex] = bytes[byteIndex].SetBit(opBitIndex, value);
                        bytes[byteIndex] = bytes[byteIndex].SetBit(maskBitIndex, true);

                        IEnumerable<byte> byteMsg = new byte[0];

                        string cmdStr = $"{action.Command}{cmdSuffix}*";
                        IEnumerable<byte> commandBytes = SIACoder.GetBytes(cmdStr);
                        IEnumerable<byte> blockData = commandBytes.Concat(bytes);
                        IEnumerable<byte> header = new byte[1] { SIACoder.EncodeHeader(blockData.Count() - 1, true) };
                        byteMsg = header.Concat(blockData);
                        IEnumerable<byte> checksum = new byte[1] { SIACoder.CalculateChecksum(byteMsg.ToArray()) };

                        byteMsg = byteMsg.Concat(checksum);

                        SIAMessage request = new SIAMessage(byteMsg.ToArray());
                        SIAMessage outputReply = SendMessage(request.RawMessage);

                        Logger.Debug($"Panel command reply: {SIACoder.GetMessageString(outputReply?.RawMessage)}");

                        if (null != outputReply && outputReply.BlockCode.Equals(SIABlockCode.POS_ACK))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    break;
                default:
                    throw new SeiException($"Invalid action, item type {action.ItemTypeID} does not support any actions.");
            }
            return false;
        }
    }
}

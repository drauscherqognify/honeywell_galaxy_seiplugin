﻿using SeeTec.Helpers.Logging;
using System.Collections.Generic;
using System.Linq;

namespace SeeTec.HoneywellGalaxy.SIAConnector
{
    /// <summary>
    /// Represents a SIA Event, including associated ASCII block.
    /// </summary>
    public class SIAEvent
    {
        /// <summary>
        /// List of valid event codes. Any event codes outside this list are ignored.
        /// </summary>
        private static string[] VALID_EVENT_CODES = new string[]
            {   "AR", "AC", "AT", "BA", "BB", "BC", "BJ", "BR", "BT", "BU", "BV", "BX", "CA", "CE", "CG", "CI", "CJ",
                "CL", "CP", "CR", "CT", "DD", "DF", "DG", "DK", "DT", "ET", "FA", "FB", "FJ", "FR", "SA", "FT", "FU",
                "FX", "HA", "HB", "HJ", "HR", "HT", "HU", "JA", "JL", "JT", "LB", "LE", "LF", "LR", "LT", "LX", "MA",
                "MR", "OG", "OK", "OP", "OR", "PA", "PB", "PJ", "PR", "PT", "PU", "RC", "RD", "RO", "RP", "RR", "RS",
                "RX", "TA", "TE", "TR", "TS", "XH", "XQ", "XR", "XT", "YC", "YK", "YR", "YT"
            };

        /// <summary>
        /// Event type of the event.
        /// </summary>
        public SIAEventType EventType { get; private set; }

        /// <summary>
        /// Time as received from the Galaxy panel in hh:mm format. 
        /// This is the time the event occured in Panel time, not the time of reception.
        /// </summary>
        public string Time { get; private set; }

        /// <summary>
        /// Group Id for the event, if any.
        /// </summary>
        public string Group { get; private set; }

        /// <summary>
        /// User Id for the event, if any.
        /// </summary>
        public string UserId { get; private set; }

        /// <summary>
        /// Periphery (module) Id for the event, if any.
        /// </summary>
        public string PeripheryId { get; private set; }

        /// <summary>
        /// The ASCII block received together with the event (if any).
        /// </summary>
        public SIAAsciiBlock AsciiBlock { get; private set; }

        /// <summary>
        /// Zone Id for the event, if any.
        /// </summary>
        public string ZoneId { get; private set; }

        /// <summary>
        /// SIA event code of the event.
        /// </summary>
        public string EventCode { get; private set; }

        /// <summary>
        /// Flag whether the event code is an expected event code.
        /// </summary>
        public bool IsValidEventCode { get => VALID_EVENT_CODES.Contains(EventCode); }

        /// <summary>
        /// Flag whether the event has a group id.
        /// </summary>
        public bool HasGroupId { get => Group != null; }

        /// <summary>
        /// Flag whether the event has a zone id.
        /// </summary>
        public bool HasZoneId { get => ZoneId != null; }

        /// <summary>
        /// Flag whether the event has a user id.
        /// </summary>
        public bool HasUserId { get => UserId != null; }

        /// <summary>
        /// Flag whether the event has a periphery id.
        /// </summary>
        public bool HasPeripheryId { get => PeripheryId != null; }

        /// <summary>
        /// Creates a new instance of the SIA Event.
        /// </summary>
        /// <param name="blockCode">Block code of the event ('N' for new event, 'O' for old event).</param>
        /// <param name="blockData">Raw string block data of the event.</param>
        /// <param name="rawAsciiBlock">Raw string ASCII block associated with the event.</param>
        public SIAEvent(char blockCode, string blockData, string rawAsciiBlock = "")
        {
            if (blockCode != 'N' && blockCode != 'O')
            {
                Logger.Error($"Passed block data is not an event: {blockData}");
                throw new SIAEventException($"Passed block data is not an event: {blockData}");
            }

            AsciiBlock = new SIAAsciiBlock(rawAsciiBlock);
            ParseBlockData(blockData);
            EventType = DetermineEventType(this);
        }

        /// <summary>
        ///  Tries to determine the event type of the given event. This is done according to the SIA 
        ///  message structure as per documentation "Galaxy SIA Interface Specification Rev 1.05".
        /// </summary>
        /// <param name="siaEvent">The event to determine type for.</param>
        /// <returns>The type of the event.</returns>
        private SIAEventType DetermineEventType(SIAEvent siaEvent)
        {
            if (siaEvent.ZoneId != null)
            {
                return SIAEventType.ZONE;
            }

            if (siaEvent.UserId != null)
            {
                return SIAEventType.USER;
            }

            if (siaEvent.PeripheryId != null)
            {
                return SIAEventType.MODULE;
            }

            if (siaEvent.IsValidEventCode == true)
            {
                return SIAEventType.EVENT;
            }

            return SIAEventType.UNKNOWN;
        }

        /// <summary>
        /// Parses the block data into all possible content blocks.
        /// </summary>
        /// <param name="blockData">The block data to parse.</param>
        private void ParseBlockData(string blockData)
        {
            Dictionary<string, string> blocks = ExtractBlocks(blockData);

            foreach (var key in blocks.Keys)
            {
                var value = blocks[key];

                switch (key)
                {
                    case "ti":
                        if (value == null)
                        {
                            string msg = $"Invalid event: ti block without content.";
                            throw new SIAEventException(msg);
                        }
                        Time = value;
                        break;
                    case "ri":
                        if (value == null)
                        {
                            string msg = $"Invalid event: ri block without content.";
                            throw new SIAEventException(msg);
                        }
                        Group = value;
                        break;
                    case "pi":
                        if (value == null)
                        {
                            string msg = $"Invalid event: pi block without content.";
                            throw new SIAEventException(msg);
                        }
                        PeripheryId = value;
                        break;
                    case "id":
                        if (value == null)
                        {
                            string msg = $"Invalid event: id block without content.";
                            throw new SIAEventException(msg);
                        }
                        UserId = value;
                        break;
                    default:
                        // check if key is a valid event code
                        if (!VALID_EVENT_CODES.Contains(key))
                        {
                            Logger.Warn($"Unknown block in event content: {key}{value}");
                            continue;
                        }
                        EventCode = key;
                        ZoneId = value;
                        break;
                }
            }
        }

        /// <summary>
        /// Extracts the blocks contained in the given block data into a convenient dictionary.
        /// </summary>
        /// <param name="blockData">The block data to parse.</param>
        /// <returns>Dictionary of contained blocks.</returns>
        private Dictionary<string, string> ExtractBlocks(string blockData)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            string eventData = blockData;

            // Blocks are separated by '/'
            var rawBlocks = eventData.Split('/');

            if (rawBlocks.Length < 2)
            {
                // SIA Level 3/4 events have at least two blocks (time and some id), whereas SIA 0/1 blocks have only one block
                throw new SIAEventException($"Event block data is not SIA Level 3/4 compliant: {blockData}");
            }

            foreach (var rb in rawBlocks)
            {
                if (rb == null || rb.Length < 2)
                {
                    Logger.Error($"Skipping block, invalid block in SIA event block message: {rb}");
                    continue;
                }

                // first two characters of raw block decide what type of block it is
                var blockType = rb.Substring(0, 2);

                string blockContent = null;

                // An event block might or might not have a zone id, all other blocks should have content
                if (rb.Length > 2)
                {
                    blockContent = rb.Substring(2);
                }

                result.Add(blockType, blockContent);
            }

            return result;
        }

        /// <summary>
        /// String representation of the event.
        /// </summary>
        /// <returns>String representation of the event.</returns>
        public override string ToString()
        {
            return $"Type=[{EventType}], EV=[{EventCode}], ti=[{Time}], ri=[{Group}], pi=[{PeripheryId}], id=[{UserId}], zone=[{ZoneId}], ASCII=[{AsciiBlock}]";
        }
    }
}

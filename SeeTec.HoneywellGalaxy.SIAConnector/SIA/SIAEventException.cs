﻿using System;
using System.Runtime.Serialization;

namespace SeeTec.HoneywellGalaxy.SIAConnector
{
    /// <summary>
    /// Represents an exception thrown while handling SIA events.
    /// </summary>
    [Serializable]
    internal class SIAEventException : Exception
    {
        public SIAEventException()
        {
        }

        public SIAEventException(string message) : base(message)
        {
        }

        public SIAEventException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected SIAEventException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
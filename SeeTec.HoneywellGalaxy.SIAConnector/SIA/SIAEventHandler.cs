﻿using SeeTec.EventInterface;
using SeeTec.Helpers.Logging;
using SeeTec.HoneywellGalaxy.SIAConnector.SIA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SeeTec.HoneywellGalaxy.SIAConnector
{
    /// <summary>
    /// Static class to help with SIA message handling.
    /// </summary>
    public static class SIAEventHandler
    {
        /// <summary>
        /// Handles a timed out SIA event message (received SIA event, but no ASCII block).
        /// </summary>
        /// <param name="state">The state object managing the stream state.</param>
        internal static void HandleEventTimeout(SIASocketState state)
        {
            Logger.Trace($"Handling timed out event (no ASCII block received).");
            SIAEvent evt = new SIAEvent(state.LastEventMessage.BlockCode, state.LastEventMessage.BlockData);
            state.NoEventReceived();
        }

        /// <summary>
        /// Handles a set of SIA messages.
        /// </summary>
        /// <param name="socket">The socket to use for handling the messages (e.g. to send replies).</param>
        /// <param name="messageList">The messages to handle.</param>
        /// <param name="state">The state object managing the stream state.</param>
        internal static void HandleMessages(Socket socket, List<SIAMessage> messageList, SIASocketState state, Action<SIAEvent> eventOccuredCallback)
        {
            foreach (var msg in messageList)
            {
                AcknowledgeMessage(socket, msg);

                switch (msg.BlockCode)
                {
                    case SIABlockCode.NEW_EVENT:
                        // New Event
                        Logger.Trace("Received new event message.");
                        state.ReceivedEvent(msg);
                        break;
                    case SIABlockCode.OLD_EVENT:
                        // Old Event
                        Logger.Trace("Received old event message.");
                        state.ReceivedEvent(msg);
                        break;
                    case SIABlockCode.ASCII:
                        // ASCII (Additional Information, sent with 'N' and 'O' blocks
                        Logger.Trace("Received ASCII (additional information) message.");
                        if (true == state.ExpectAsciiBlock)
                        {
                            // We have a complete SIA event now (Event Message + ASCII block)
                            SIAEvent evt = new SIAEvent(state.LastEventMessage.BlockCode, state.LastEventMessage.BlockData, msg.BlockData);
                            eventOccuredCallback(evt);
                            Logger.Debug($"Received complete SIA event: {evt.ToString()}");
                        }
                        else
                        {
                            Logger.Warn("Received ASCII block, but no ASCII block was expected, message is ignored.");
                        }
                        state.NoEventReceived();
                        break;
                    case SIABlockCode.ID:
                        // '#' -> ID, Used to send site identification from panel
                        Logger.Trace($"Site identified with '{msg.BlockData}'.");
                        break;
                    case SIABlockCode.END_DATA:
                        // End Data (Inform receiver, telecom mod will hang up)
                    case SIABlockCode.CONFIG:
                        // Config, PIN was successful, 
                    case SIABlockCode.POS_ACK:
                        // Positive Ack (Galaxy control command successful)
                    case SIABlockCode.REJECT:
                        // Negative Ack (Galaxy PIN/control command unsuccessful)
                    case SIABlockCode.CONTROL:
                        // Galaxy reply to control command
                    case SIABlockCode.PIN:
                        // PIN (should never be sent by Galaxy)
                    case SIABlockCode.EXTEND:
                        // Extended control command (should never be sent by Galaxy)
                    default:
                        Logger.Warn($"Unexpected Block Code '{msg.BlockCode}' on event socket, skipping message.");
                        state.NoEventReceived();
                        break;
                }
            }
        }

        /// <summary>
        /// Acknowledges the given message if necessary (i.e. if the "Acknowledgement Request Bit" is set).
        /// </summary>
        /// <param name="sock">The socket to send the acknowledgement on.</param>
        /// <param name="msg">The message to acknowledge.</param>
        private static void AcknowledgeMessage(Socket sock, SIAMessage msg)
        {
            // Check if this message needs to be acknowledged
            if (true == msg.AckRequest)
            {
                if (true == msg.Valid)
                {
                    // Ack valid message with '8' block
                    Logger.Trace($"Sending positive ack.");
                    var ackMsg = new SIAMessage('8');
                    Send(sock, ackMsg.RawMessage);
                }
                else
                {
                    // Ack invalid message with '9' block
                    Logger.Trace($"Sending negative ack.");
                    var ackMsg = new SIAMessage('9');
                    Send(sock, ackMsg.RawMessage);
                }
            }
            else
            {
                Logger.Trace("No ack needed.");
            }
        }

        /// <summary>
        /// Sends the given raw bytes on the given socket.
        /// </summary>
        /// <param name="socket">The socket.</param>
        /// <param name="byteData">The data.</param>
        private static void Send(Socket socket, byte[] byteData)
        {
            // Send data synchronuosly 
            socket.Send(byteData, byteData.Length, 0);
        }
    }
}

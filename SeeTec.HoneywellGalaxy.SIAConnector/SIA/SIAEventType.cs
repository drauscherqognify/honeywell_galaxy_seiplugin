﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeTec.HoneywellGalaxy.SIAConnector
{
    /// <summary>
    /// Enumeration for SIA Event Type
    /// </summary>
    [Flags]
    public enum SIAEventType : short
    {
        ZONE = 1,
        USER = 2,
        MODULE = 4,
        EVENT = 8,
        UNKNOWN = short.MaxValue
    }
}

﻿using SeeTec.EventInterface;
using SeeTec.Helpers.Logging;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace SeeTec.HoneywellGalaxy.SIAConnector
{
    /// <summary>
    /// Wraps a socket listening for and handling SIA messages. 
    /// </summary>
    public class SIAListenerSocket : IDisposable
    {
        /// <summary>
        /// Backlock for the socket (maximum number of connections).
        /// </summary>
        private const int SOCKET_BACKLOG = 10;

        /// <summary>
        /// Default timeout when listening for any SIA messages
        /// </summary>
        private const int DEFAULT_RECEIVE_TIMEOUT_MS = 0; // 0 = infinite

        /// <summary>
        /// Constant for time to wait for ASCII block after receiving an event message.
        /// If no ASCII block is received after this time, the event is fired without ASCII block.
        /// </summary>
        private const int EXPECTING_ASCII_BLOCK_RECEIVE_TIMEOUT_MS = 2000;

        /// <summary>
        /// Thread signal while waiting for incoming connections.
        /// </summary>
        public static ManualResetEvent allDone = new ManualResetEvent(false);

        /// <summary>
        /// Port this listener shall listen on.
        /// </summary>
        private readonly int _port;

        /// <summary>
        /// List of hosts to accept messages from (null/empty = any)
        /// </summary>
        private List<string> _allowedHosts;
        
        /// <summary>
        /// Callback for events.
        /// </summary>
        private Action<SIAEvent> _eventOccuredCallback;
        private Socket _listener;
        private bool _dispose = false;

        /// <summary>
        /// Creates a new instance of the SIA Event Listener. 
        /// </summary>
        /// <param name="port">Port to listen on.</param>
        /// <param name="host">Remote host allowed to connect to this listener. Defaults to null (= accept all hosts).</param>
        public SIAListenerSocket(Action<SIAEvent> raiseEventOccured, int port, List<string> allowedHosts)
        {
            _port = port;
            _allowedHosts = allowedHosts;
            _eventOccuredCallback = raiseEventOccured;
        }

        /// <summary>
        /// Signals the listener to start accepting incoming connections.
        /// </summary>
        public void StartListening()
        {
            // Establish the local endpoint for the socket.  
            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, _port);

            // Create a TCP/IP socket.  
            _listener = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            LingerOption lo = new LingerOption(false, 0);
            _listener.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Linger, lo);

            // Bind the socket to the local endpoint and listen for incoming connections.  
            try
            {
                _listener.Bind(localEndPoint);
                _listener.Listen(SOCKET_BACKLOG);

                string hostList = string.Join(", ", _allowedHosts);
                if (string.IsNullOrWhiteSpace(hostList)) hostList = "any remote host.";

                Logger.Info($"Now accepting connections from {hostList}");

                while (true)
                {
                    // Set the event to nonsignaled state.  
                    allDone.Reset();

                    // Start an asynchronous socket to listen for connections.  
                    _listener.BeginAccept(
                        new AsyncCallback(AcceptCallback),
                        _listener);

                    // Wait until a connection is made before continuing.  
                    allDone.WaitOne();

                    if (_dispose)
                    {
                        Logger.Trace("Listener socket disposed.");
                        return;
                    }
                }

            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                Logger.Error("Opening event listening socket failed. Retrying in 10 seconds.");
                Thread.Sleep(10000);
                StartListening();
            }
        }

        /// <summary>
        /// Called once a remote host connection is established.
        /// Checks if the remote host is allowed to connect, refuses the connection otherwise.
        /// </summary>
        /// <param name="ar">Asynchronous state object.</param>
        private void AcceptCallback(IAsyncResult ar)
        {
            // Signal the main thread to continue.  
            allDone.Set();

            // Get the socket that handles the client request.  
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);

            if (_allowedHosts != null && _allowedHosts.Count > 0 && !_allowedHosts.Contains(((IPEndPoint) handler.RemoteEndPoint).Address.ToString()))
            {
                Logger.Warn($"Connection from {handler.RemoteEndPoint} refused, only accepting connections from {string.Join(", ",_allowedHosts)}. Event not handled.");

                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
                return;
            }

            Logger.Debug($"Connection accepted from {handler.RemoteEndPoint}");


            // Create the state object.  
            SIASocketState state = new SIASocketState();
            state.workSocket = handler;

            SynchronousReceive(state, handler);
        }

        /// <summary>
        /// Synchronuosly receives bytes on the given given socket, using the given state object.
        /// </summary>
        /// <param name="state">Object to manage the state of the message stream.</param>
        /// <param name="socket">The socket to receive from.</param>
        private void SynchronousReceive(SIASocketState state, Socket socket)
        {
            int receiveTimeoutMs = DEFAULT_RECEIVE_TIMEOUT_MS;
            while (true)
            {
                socket.ReceiveTimeout = receiveTimeoutMs;

                Logger.Trace($"Waiting for data (timeout {receiveTimeoutMs}ms)...");
                int bytesRead = socket.Receive(state.buffer);

                if (bytesRead > 0)
                {
                    // Extract actually received bytes from buffer
                    byte[] receivedBytes = new byte[bytesRead];
                    Array.Copy(state.buffer, receivedBytes, bytesRead);

                    Logger.Trace($"Received: '{SIACoder.GetMessageString(receivedBytes)}'");

                    // Put all bytes in internal buffer for partial data
                    state.receivedBytes.AddRange(receivedBytes);
                    var buf = state.receivedBytes.ToArray();

                    // get first byte (assume it's a SIA header), and calculate total message size
                    byte header = state.receivedBytes[0];

                    // 1 byte header, 1 byte block code, <blockLength> bytes data, 1 byte checksum --> at least 3 bytes
                    int messageLength = SIACoder.GetBlockLength(header) + 3;

                    // check if the whole message is in the buffer
                    if (state.receivedBytes.Count < messageLength)
                    {
                        Logger.Warn($"Message incomplete. Expected {messageLength} bytes, got {bytesRead} bytes. Waiting for more data.");
                    }
                    else
                    {
                        // Get all messages contained in the buffer
                        List<SIAMessage> messageList = new List<SIAMessage>();
                        byte[] remainingBuffer = SIACoder.GetAllMessages(buf, messageList);

                        // the remaining buffer may contain more messages - store it in the state object
                        state.receivedBytes.Clear();
                        state.receivedBytes.AddRange(remainingBuffer);

                        Logger.Trace($"Extracted {messageList.Count} message(s) from buffer, {remainingBuffer.Length} bytes remain in buffer.");

                        // Handle the received messages
                        SIAEventHandler.HandleMessages(socket, messageList, state, _eventOccuredCallback);

                        // If, after handling all messages in the buffer, an ASCII block is expected, we set a short timeout
                        if (true == state.ExpectAsciiBlock)
                        {
                            Logger.Trace("Expecting ASCII block, short timeout.");
                            receiveTimeoutMs = EXPECTING_ASCII_BLOCK_RECEIVE_TIMEOUT_MS;
                        }
                        else
                        {
                            Logger.Trace("Not expecting ASCII block, long timeout.");
                            receiveTimeoutMs = DEFAULT_RECEIVE_TIMEOUT_MS;
                        }
                    }
                }
                else if (true == state.ExpectAsciiBlock)
                {
                    // Connection was closed or read times out, but we expected an ASCII block - handle the event without ASCII block
                    SIAEventHandler.HandleEventTimeout(state);
                }
                else
                {
                    Logger.Trace("Connection closed by remote peer.");
                    break;
                }
            }
        }

        public void Dispose()
        {
            _dispose = true;
            _listener.Shutdown(SocketShutdown.Both);
            _listener.Disconnect(true);
            _listener.Close();
            _listener.Dispose();
        }

    }
}
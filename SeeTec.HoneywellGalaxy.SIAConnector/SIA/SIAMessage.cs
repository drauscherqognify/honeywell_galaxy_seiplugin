﻿using System;
using System.Linq;
using System.Text;

namespace SeeTec.HoneywellGalaxy.SIAConnector
{
    /// <summary>
    /// Represents a SIA message received from the Galaxy panel.
    /// </summary>
    public class SIAMessage
    {
        /// <summary>
        /// The original raw message the SIA Message was parsed from.
        /// </summary>
        public byte[] RawMessage { get; }

        /// <summary>
        /// Header byte of the message.
        /// </summary>
        private byte Header { get; }

        /// <summary>
        /// Block length of the SIA message, extracted from header.
        /// </summary>
        public int BlockLength { get; }

        /// <summary>
        /// Block code of the SIA message.
        /// </summary>
        public char BlockCode { get; }

        /// <summary>
        /// Block data of the SIA message.
        /// </summary>
        public string BlockData { get; }

        /// <summary>
        /// Checksum of the SIA message.
        /// </summary>
        public byte Checksum { get; }

        /// <summary>
        /// Flag whether the Acknowledgement Request bit is set in the SIA message.
        /// </summary>
        public bool AckRequest { get; }

        /// <summary>
        /// Boolean wether the SIA message is valid according to the parity checksum.
        /// </summary>
        public bool Valid { get; } = false;

        /// <summary>
        /// Constructs a new SIA message from a given raw SIA byte message.
        /// </summary>
        /// <param name="rawMessage">The raw message to decode the SIA message from.</param>
        public SIAMessage(byte[] rawMessage)
        {
            RawMessage = rawMessage;
            string stringMessage = string.Empty;

            if (RawMessage.Length > 1)
            {
                stringMessage = SIACoder.GetMessageString(RawMessage);
                Header = RawMessage.First();
                BlockLength = SIACoder.GetBlockLength(Header);
                AckRequest = SIACoder.GetAckRequestBit(Header);
                BlockCode = stringMessage[1];
            }
            else
            {
                throw new ArgumentException($"Invalid SIA Message: Empty message");
            }

            if (stringMessage.Length == BlockLength + 2)
            {
                BlockData = stringMessage.Substring(2, BlockLength);
                Checksum = RawMessage.Last();
            }
            else
            {
                throw new ArgumentException($"Invalid SIA Message: Unexpected message length - expected {BlockLength + 3}, got {stringMessage.Length}");
            }

            byte[] messageWithoutParity = SIACoder.GetMessageWithoutParity(RawMessage);
            Valid = SIACoder.Validate(messageWithoutParity, Checksum);
        }

        /// <summary>
        /// Creates a SIA message from the given parameters, generates header and checksum bytes.
        /// </summary>
        /// <param name="blockCode">Block Code.</param>
        /// <param name="blockData">Block data (may be blank) not including block code.</param>
        /// <param name="ackRequest">Flag whether the message requests an ack from the panel.</param>
        public SIAMessage(char blockCode, string blockData = "", bool ackRequest = false)
        {
            AckRequest = ackRequest;
            BlockData = blockData;
            BlockCode = blockCode;
            BlockLength = BlockData.Length;
            Header = SIACoder.EncodeHeader(BlockLength, AckRequest);

            string completeMessage = $"{((char)Header)}{BlockCode}{BlockData}";
            Checksum = SIACoder.CalculateChecksum(completeMessage);
            byte[] messageWithoutParity = SIACoder.GetBytes(completeMessage);
            Valid = SIACoder.Validate(messageWithoutParity, Checksum);

            RawMessage = new byte[messageWithoutParity.Length + 1];
            Array.Copy(messageWithoutParity, 0, RawMessage, 0, messageWithoutParity.Length);
            RawMessage[RawMessage.Length - 1] = Checksum;
        }

        /// <summary>
        /// Creates a SIA message from the given string.
        /// </summary>
        /// <param name="stringMessage">The string message including block code, block data (no header, no checksum!).</param>
        /// <param name="ackRequest">Flag whether the message requests an ack from the panel.</param>
        public SIAMessage(string stringMessage, bool ackRequest = false) : this(stringMessage[0], stringMessage.Substring(1), ackRequest)
        {
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeTec.HoneywellGalaxy.SIAConnector.SIA
{
    /// <summary>
    /// Enum the SIA Panel version.
    /// 
    /// E080-04 (Ethernet module connection) – reply is CVN
    /// E080-2 (Ethernet module connection) – reply is XVN
    /// A083 (Ethernet module connection) – reply is XVN
    /// Galaxy Dimension(RS232 connection) - reply is CVN
    /// Flex 3 (USB connection) – reply is XVN
    /// </summary>
    public enum SIAPanelVersion
    {
        CVN,
        XVN
    }
}

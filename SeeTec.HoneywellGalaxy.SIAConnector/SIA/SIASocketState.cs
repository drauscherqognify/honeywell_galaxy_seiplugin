﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SeeTec.HoneywellGalaxy.SIAConnector
{
    /// <summary>
    /// State object for managing the SIA socket stream.
    /// </summary>
    public class SIASocketState
    {
        /// <summary>
        /// Size of receive buffer.
        /// </summary>   
        public const int BUFFER_SIZE = 1024;

        /// <summary>
        /// Client  socket.  
        /// </summary>
        public Socket workSocket = null;

        /// <summary>
        /// Receive buffer.  
        /// </summary>
        public byte[] buffer = new byte[BUFFER_SIZE];

        /// <summary>
        /// Buffer for partially received data.
        /// </summary>
        public List<byte> receivedBytes = new List<byte>();

        /// <summary>
        /// True if we received a partial message and are waiting for more data. 
        /// </summary>
        public bool FragmentedMessage = false;

        /// <summary>
        /// True if we expect an ASCII Block (after receiving an N or O block).
        /// </summary>
        public bool ExpectAsciiBlock = false;

        /// <summary>
        /// The last event message which was received. Used to remember the event message
        /// until the corresponding ASCII block is received (if any).
        /// </summary>
        public SIAMessage LastEventMessage { get; internal set; }

        /// <summary>
        /// Time of reception for the last message.
        /// Used to assign the original reception time of an event after the corresponding
        /// ASCII block was received.
        /// </summary>
        public DateTime LastEventReceptionTime { get; internal set; }

        /// <summary>
        /// Handles the state when a message is received.
        /// </summary>
        /// <param name="msg">The received message.</param>
        internal void ReceivedEvent(SIAMessage msg)
        {
            LastEventMessage = msg;
            LastEventReceptionTime = DateTime.Now;
            ExpectAsciiBlock = true;
        }

        /// <summary>
        /// Handles the state when any message except an event is received.
        /// </summary>
        internal void NoEventReceived()
        {
            ExpectAsciiBlock = false;
            LastEventMessage = null;
        }
    }
}

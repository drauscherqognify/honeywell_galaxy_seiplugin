﻿using System;
using System.Runtime.Serialization;

namespace SeeTec.HoneywellGalaxy.SIAConnector.SIA
{
    [Serializable]
    internal class SIAStateException : Exception
    {
        public SIAStateException()
        {
        }

        public SIAStateException(string message) : base(message)
        {
        }

        public SIAStateException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected SIAStateException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
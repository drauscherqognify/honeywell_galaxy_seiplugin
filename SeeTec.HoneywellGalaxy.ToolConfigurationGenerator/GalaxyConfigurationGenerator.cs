﻿using SeeTec.Helpers.Logging;
using SeeTec.HoneywellGalaxy.Configuration.ICT;
using SeeTec.SEIToolConfiguration;

namespace SeeTec.HoneywellGalaxy.ToolConfigurationGenerator
{
    /// <summary>
    /// Console application to generate the ICT configuration for the Honeywell Galaxy SEI plugin.
    /// </summary>
    internal class GalaxyConfigurationGenerator
    {
        /// <summary>
        /// Gets the version of this plugin. 
        /// </summary>
        public static string Version => System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

        /// <summary>
        /// Main method.
        /// </summary>
        /// <param name="args">Arguments.</param>
        private static void Main(string[] args)
        {
            // TODO: Refactor this to command line parameters? Right now this tool is only used during develpment...
            const string csvPath = @"c:\temp\galaxyEvents_cleaned.csv";
            const string filename = @"C:\dev\SeeTec\Honeywell Galaxy\ICT\SeeTec.SEIConfigurationTool.xml";
            Logger.SetLogLevel(Logger.LogLevel.DBG);
            Logger.Info($"Honeywell Galaxy Configuration Generatog Version {Version}");

            ToolConfig conf = GalaxyToolConfigFactory.GenerateToolConfigFromCSV(csvPath);
            Logger.Info($"Writing config to file {filename}");

            ToolConfigurationSerializer.WriteToolConfiguration(conf, filename);
            Logger.Info("\nFinished. Have a nice day!");
        }
    }
}

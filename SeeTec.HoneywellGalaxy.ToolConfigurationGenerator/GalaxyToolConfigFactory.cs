﻿using SeeTec.Helpers.Logging;
using SeeTec.HoneywellGalaxy.Configuration.ICT;
using SeeTec.HoneywellGalaxy.Configuration.SeiPlugin;
using SeeTec.SEIToolConfiguration;
using System;
using System.Collections.Generic;

namespace SeeTec.HoneywellGalaxy.ToolConfigurationGenerator
{
    /// <summary>
    /// Factory for creating a SEI ICT configuration.
    /// </summary>
    public static class GalaxyToolConfigFactory
    {
        /// <summary>
        /// List of supported item type names for checking imported configuration.
        /// </summary>
        private static readonly List<string> _supportedItemTypeNames = new List<string>()
        {
            ItemTypeNames.ITEM_TYPE_TID_GROUP,
            ItemTypeNames.ITEM_TYPE_TID_ZONE,
            ItemTypeNames.ITEM_TYPE_TID_USER,
            ItemTypeNames.ITEM_TYPE_TID_MOD,
            ItemTypeNames.ITEM_TYPE_TID_TEST,
            ItemTypeNames.ITEM_TYPE_TID_EVENT
        };

        /// <summary>
        /// Lookup dictionary for human readable item types names.
        /// </summary>
        private static readonly Dictionary<string, string> ItemTypeDisplayNameDictionary = new Dictionary<string, string>()
        {
            { ItemTypeNames.ITEM_TYPE_TID_GROUP, ItemTypeNames.ITEM_TYPE_DISPLAY_NAME_GROUP },
            { ItemTypeNames.ITEM_TYPE_TID_ZONE, ItemTypeNames.ITEM_TYPE_DISPLAY_NAME_ZONE },
            { ItemTypeNames.ITEM_TYPE_TID_USER, ItemTypeNames.ITEM_TYPE_DISPLAY_NAME_USER },
            { ItemTypeNames.ITEM_TYPE_TID_MOD, ItemTypeNames.ITEM_TYPE_DISPLAY_NAME_MOD },
            { ItemTypeNames.ITEM_TYPE_TID_TEST, ItemTypeNames.ITEM_TYPE_DISPLAY_NAME_TEST },
            { ItemTypeNames.ITEM_TYPE_TID_EVENT, ItemTypeNames.ITEM_TYPE_DISPLAY_NAME_EVENT },
            { ItemTypeNames.ITEM_TYPE_TID_OUTPUT, ItemTypeNames.ITEM_TYPE_DISPLAY_NAME_OUTPUT }
        };

        /// <summary>
        /// SIA Level 2-4 identification for the group id block in the SIA message.
        /// </summary>
        private const string SIA_MESSAGE_GROUP_BLOCK = "/ri";

        /// <summary>
        /// Mapping of item type names to their (generated) id.
        /// </summary>
        private static Dictionary<string, string> _itemTypeNameIdDict;

        /// <summary>
        /// Static method to generate a tool configuration from a given CSV file.
        /// </summary>
        /// <param name="csvPath">File name of the CSV file to generate the tool configuration from, including full path.</param>
        /// <returns>The <see cref="ToolConfig">tool configuration</see>.</returns>
        public static ToolConfig GenerateToolConfigFromCSV(string csvPath)
        {
            var eventDefinitions = EventDefinitionCSVReader.ReadEventDefinition(csvPath);

            ToolConfig conf = new ToolConfig()
            {
                PluginName = "Honeywell Galaxy SEI Plugin",
                ProtocolVersion = 1,
                ThirdPartyId = "SIA Level 4"
            };

            Logger.Info("Building Item Type tree.");
            conf.ItemTypes = GetItemTypes(eventDefinitions);

            _itemTypeNameIdDict = GetItemTypeIdDict(conf.ItemTypes);

            Logger.Info("Building Event Type tree.");
            conf.NativeEventTypes = GetNativeEventTypes(eventDefinitions);

            return conf;
        }

        /// <summary>
        /// Generates the mapping between item type names and their id.
        /// </summary>
        /// <param name="itemTypes">Item types to get mapping from.</param>
        /// <returns>The mapping.</returns>
        private static Dictionary<string, string> GetItemTypeIdDict(List<ToolConfigItemType> itemTypes)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            foreach (var it in itemTypes)
            {
                result.Add(it.ThirdPartyId, it.Id);
            }

            return result;
        }

        /// <summary>
        /// Extracts the native event types from the given list of <see cref="SIAEventDefinition">event definitions</see>.
        /// </summary>
        /// <param name="eventDefinitions">The event definitions to get the native event types from.</param>
        /// <returns>The native event types.</returns>
        private static List<ToolConfigNativeEventType> GetNativeEventTypes(List<SIAEventDefinition> eventDefinitions)
        {
            // Categories are root event types
            var nativeEventTypes = GetCategoryEventTypes(eventDefinitions);

            foreach (var category in nativeEventTypes)
            {
                Logger.Info($"==== {category.Name}");
                // subtypes of categories are event descriptions
                var eventDescriptionEventTypes = GetEventDescriptionEventTypes(category.Name, eventDefinitions);

                foreach (var eventDescriptionET in eventDescriptionEventTypes)
                {
                    Logger.Info($"\t[{eventDescriptionET.ThirdPartyId}] {eventDescriptionET.Name} ({eventDescriptionET.Id})");

                    // subtypes of event descriptions are log events
                    var logEventEventTypes = GetLogEventEventTypes(eventDescriptionET.ThirdPartyId, eventDefinitions);

                    foreach (var le in logEventEventTypes)
                    {
                        Logger.Info($"\t\t[{le.ThirdPartyId}] {le.Name} ({le.Id})");
                    }

                    // event description types are applicable to any item type any subtype is applicable to
                    eventDescriptionET.ApplicableItemTypeIdList = GetApplicableItemTypes(logEventEventTypes); ;
                    eventDescriptionET.SubTypes = logEventEventTypes;
                }

                // category types are applicable to any item type any subtype is applicable to
                category.ApplicableItemTypeIdList = GetApplicableItemTypes(eventDescriptionEventTypes);
                category.SubTypes = eventDescriptionEventTypes;
            }

            return nativeEventTypes;
        }

        /// <summary>
        /// Looks through all given native event types and returns a list of distinct applicable item types any of the 
        /// given event types is applicable to.
        /// </summary>
        /// <param name="nativeEventTypes">The event types to scan.</param>
        /// <returns>The list of distinct item type ids.</returns>
        private static List<string> GetApplicableItemTypes(List<ToolConfigNativeEventType> nativeEventTypes)
        {
            List<string> result = new List<string>();

            foreach (var ne in nativeEventTypes)
            {
                foreach (var ae in ne.ApplicableItemTypeIdList)
                {
                    if (!result.Contains(ae))
                    {
                        result.Add(ae);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Gets all "Log Event" event types for a given event code. These are used as sub-types of the event codes.
        /// </summary>
        /// <param name="eventCode">Event code to filter by.</param>
        /// <param name="eventDefinitions">All event definitions.</param>
        /// <returns>Event types corresponding to event code.</returns>
        private static List<ToolConfigNativeEventType> GetLogEventEventTypes(string eventCode, List<SIAEventDefinition> eventDefinitions)
        {
            List<ToolConfigNativeEventType> logEventEventTypes = new List<ToolConfigNativeEventType>();

            List<SIAEventDefinition> filteredEd = eventDefinitions.FindAll(e => e.EventCode.Equals(eventCode));

            foreach (var ed in filteredEd)
            {
                ToolConfigNativeEventType it = new ToolConfigNativeEventType()
                {
                    Id = GenerateGuid(),
                    Name = $"[{ed.LogEvent}] {ed.LogEventDescription}",
                    ThirdPartyId = ed.LogEvent
                };

                it.AddProperty("parentEventCode", eventCode);

                // Get applicable item type id 
                var applicableItemTypeId = _itemTypeNameIdDict[ed.ItemType];

                if (applicableItemTypeId != null)
                {
                    it.ApplicableItemTypeIdList.Add(applicableItemTypeId);
                }
                else
                {
                    Logger.Error($"Event definition '{ed.LogEvent}' has unknown item type '{ed.ItemType}'.");
                }

                // does it potentially concern a group?
                if (ConcernsGroup(ed))
                {
                    var groupItemTypeId = _itemTypeNameIdDict[ItemTypeNames.ITEM_TYPE_TID_GROUP];

                    if (groupItemTypeId != null)
                    {
                        it.ApplicableItemTypeIdList.Add(groupItemTypeId);
                    }
                    else
                    {
                        Logger.Error($"Group item type is undefined.");
                    }
                }

                logEventEventTypes.Add(it);
            }

            return logEventEventTypes;
        }

        /// <summary>
        /// Helper method to determine if an event definition may concern a group.
        /// </summary>
        /// <param name="ed">The event definition.</param>
        /// <returns>True if it may concern a group, false else.</returns>
        private static bool ConcernsGroup(SIAEventDefinition ed)
        {
            return ed.SIA24 != null && ed.SIA24.Contains(SIA_MESSAGE_GROUP_BLOCK);
        }

        /// <summary>
        /// Extracts the distinct names of all categories from the given event definitions.
        /// </summary>
        /// <param name="eventDefinitions">The event definitions.</param>
        /// <returns>List of distinct category names.</returns>
        private static List<string> GetCategories(List<SIAEventDefinition> eventDefinitions)
        {
            Logger.Trace("======= Categories =======");
            List<string> result = new List<string>();

            foreach (var ed in eventDefinitions)
            {
                string itName = ed.Category;

                if (!result.Contains(itName))
                {
                    Logger.Trace($"Adding category: {itName}");
                    result.Add(itName);
                }
            }
            Logger.Trace($"Got {result.Count} categories.");
            return result;
        }

        /// <summary>
        /// Generates all category event types from the given event definitions.
        /// </summary>
        /// <param name="eventDefinitions">The event definitions.</param>
        /// <returns>List of category event types.</returns>
        private static List<ToolConfigNativeEventType> GetCategoryEventTypes(List<SIAEventDefinition> eventDefinitions)
        {
            List<string> categoryList = GetCategories(eventDefinitions);
            List<ToolConfigNativeEventType> categoryEventTypes = new List<ToolConfigNativeEventType>();

            foreach (var name in categoryList)
            {
                ToolConfigNativeEventType it = new ToolConfigNativeEventType()
                {
                    Id = GenerateGuid(),
                    Name = name,
                    ThirdPartyId = name,
                };
                categoryEventTypes.Add(it);
            }
            return categoryEventTypes;
        }

        /// <summary>
        /// Filters all "event description" event definitions for a given category from the given event definitions.
        /// </summary>
        /// <param name="categoryName">The category name to filter by.</param>
        /// <param name="eventDefinitions">The event definitions to filter.</param>
        /// <returns></returns>
        private static List<SIAEventDefinition> GetEventDescriptions(string categoryName, List<SIAEventDefinition> eventDefinitions)
        {
            List<string> uniqueList = new List<string>();
            List<SIAEventDefinition> result = new List<SIAEventDefinition>();

            var filteredEd = eventDefinitions.FindAll(e => e.Category.Equals(categoryName));

            foreach (var ed in filteredEd)
            {
                string itName = ed.EventDescription;

                if (!uniqueList.Contains(itName))
                {
                    uniqueList.Add(itName);
                    result.Add(ed);
                }
            }
            return result;
        }

        /// <summary>
        /// Generates Tool Config Event Types for a given category from the given event definitions.
        /// </summary>
        /// <param name="categoryName">The category.</param>
        /// <param name="eventDefinitions">The event definitions.</param>
        /// <returns>The Tool Config Event Types.</returns>
        private static List<ToolConfigNativeEventType> GetEventDescriptionEventTypes(string categoryName, List<SIAEventDefinition> eventDefinitions)
        {
            List<SIAEventDefinition> eventDescriptionList = GetEventDescriptions(categoryName, eventDefinitions);
            List<ToolConfigNativeEventType> eventDescriptionEventTypes = new List<ToolConfigNativeEventType>();
            List<SIAEventDefinition> filteredEd = eventDescriptionList.FindAll(e => e.Category.Equals(categoryName));

            foreach (var ed in filteredEd)
            {
                ToolConfigNativeEventType it = new ToolConfigNativeEventType()
                {
                    Id = GenerateGuid(),
                    Name = $"[{ed.EventCode}] {ed.EventDescription}",
                    ThirdPartyId = ed.EventCode,
                };
                eventDescriptionEventTypes.Add(it);
            }
            return eventDescriptionEventTypes;
        }

        /// <summary>
        /// Generates all Item Types from the given event definitions.
        /// </summary>
        /// <param name="eventDefinitions">The event definitions.</param>
        /// <returns>The item types.</returns>
        private static List<ToolConfigItemType> GetItemTypes(List<SIAEventDefinition> eventDefinitions)
        {
            List<string> result = new List<string>();
            List<ToolConfigItemType> itList = new List<ToolConfigItemType>();

            foreach (var ed in eventDefinitions)
            {
                string itName = ed.ItemType;

                if (!result.Contains(itName))
                {
                    Logger.Info($"Adding item type: {itName}");
                    result.Add(itName);

                    var newIt = new ToolConfigItemType()
                    {
                        Active = true,
                        Id = GenerateGuid(),
                        Name = ItemTypeDisplayNameDictionary[itName],
                        ThirdPartyId = itName,
                        StateTypes = GetStateTypes(itName)
                    };
                    itList.Add(newIt);
                }
            }

            itList.Add(GetCustomItemType(ItemTypeNames.ITEM_TYPE_TID_GROUP));
            itList.Add(GetCustomItemType(ItemTypeNames.ITEM_TYPE_TID_OUTPUT));

            Logger.Info($"Got {itList.Count} item types.");

            return itList;
        }

        private static ToolConfigItemType GetCustomItemType(string itemTypeName)
        {
            Logger.Info($"Adding custom item type: {itemTypeName}");

            return new ToolConfigItemType()
            {
                Active = true,
                Id = GenerateGuid(),
                Name = ItemTypeDisplayNameDictionary[itemTypeName],
                ThirdPartyId = itemTypeName,
                StateTypes = GetStateTypes(itemTypeName)
            };
        }

        /// <summary>
        /// Helper method to generate a GUID.
        /// </summary>
        /// <returns>Generated GUID.</returns>
        private static string GenerateGuid()
        {
            return "" + Guid.NewGuid().GetHashCode();
        }

        /// <summary>
        /// Gets all state types for a given Item Type name.
        /// </summary>
        /// <param name="itemTypeName">The Item Type name</param>
        /// <returns>List of all State Types.</returns>
        private static List<ToolConfigStateType> GetStateTypes(string itemTypeName)
        {
            List<ToolConfigStateType> result = new List<ToolConfigStateType>();

            switch (itemTypeName)
            {
                case ItemTypeNames.ITEM_TYPE_TID_GROUP:
                    result = GetGroupStateTypes();
                    break;
                case ItemTypeNames.ITEM_TYPE_TID_ZONE:
                    result = GetZoneStateTypes();
                    break;
                case ItemTypeNames.ITEM_TYPE_TID_OUTPUT:
                    result = GetOutputStateTypes();
                    break;
                case ItemTypeNames.ITEM_TYPE_TID_USER:
                case ItemTypeNames.ITEM_TYPE_TID_MOD:
                case ItemTypeNames.ITEM_TYPE_TID_TEST:
                case ItemTypeNames.ITEM_TYPE_TID_EVENT:
                    Logger.Info($"\tNo state types.");
                    break;
                default:
                    Logger.Warn($"Unknown item type '{itemTypeName}', no state types available.");
                    break;
            }

            foreach (var stateType in result)
            {
                Logger.Info($"\t{stateType.Name} ({stateType.Id})");
            }

            return result;
        }

        /// <summary>
        /// Gets the State Types for the custom item type "Output".
        /// </summary>
        /// <returns>List of state types for "Output".</returns>
        private static List<ToolConfigStateType> GetOutputStateTypes()
        {
            return new List<ToolConfigStateType>
            {
                NewState(0, "Off", "COR-0"),
                NewState(1, "On", "COR-1"),
            };
        }

        /// <summary>
        /// Gets the State Types for the custom item type "Group".
        /// </summary>
        /// <returns>List of state types for "Group".</returns>
        private static List<ToolConfigStateType> GetGroupStateTypes()
        {
            return new List<ToolConfigStateType>
            {
                NewState(0, "Normal", "CSA91-0"),
                NewState(1, "Alarm", "CSA91-1"),
                NewState(2, "Reset required", "CSA91-2"),

                NewState(3, "Unset", "CSA92-0"),
                NewState(4, "Set", "CSA92-1"),
                NewState(5, "Part set", "CSA92-2"),
                NewState(6, "Ready to set", "CSA92-3"),
                NewState(7, "Time locked", "CSA92-4"),
            };
        }

        /// <summary>
        /// Gets the State Types for the item type "Zone".
        /// </summary>
        /// <returns>List of state types for "Zone".</returns>
        private static List<ToolConfigStateType> GetZoneStateTypes()
        {
            return new List<ToolConfigStateType>
            {
                // CLosed
                NewState(0, "Closed", GalaxyConfig.ZONE_STATE_CLOSED),
                
                // Alarm
                NewState(1, "No Alarm", GalaxyConfig.ZONE_STATE_NOALARM),
                NewState(2, "Alarm", GalaxyConfig.ZONE_STATE_ALARM),
                
                // Open
                NewState(3, "Open", GalaxyConfig.ZONE_STATE_OPEN),

                // O/C or S/C
                NewState(4, "Tamper O/C or S/C", GalaxyConfig.ZONE_STATE_OCSC),

                // Low or High
                NewState(5, "Low/High resistance", GalaxyConfig.ZONE_STATE_LOWHIGH),
                
                // Masked
                NewState(6, "Masked", GalaxyConfig.ZONE_STATE_MASKED),

                // Fault
                NewState(7, "Fault", GalaxyConfig.ZONE_STATE_FAULT),

                // Ommitted
                NewState(8, "Not omitted", GalaxyConfig.ZONE_STATE_NOTOMIT),
                NewState(9, "Omitted", GalaxyConfig.ZONE_STATE_OMIT)
            };
        }

        /// <summary>
        /// Helper method to conveniently create a ToolConfigStateType.
        /// </summary>
        /// <param name="priority">Priority of the state type.</param>
        /// <param name="name">Name of the state type.</param>
        /// <param name="thirdPartyId">Third party ID  of the state type.</param>
        /// <returns></returns>
        private static ToolConfigStateType NewState(int priority, string name, string thirdPartyId)
        {
            return new ToolConfigStateType()
            {
                Active = true,
                Id = GenerateGuid(),
                Name = name,
                Priority = priority,
                ThirdPartyId = thirdPartyId
            };
        }
    }
}
